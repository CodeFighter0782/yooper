﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.UI.Pages;
using Yooper.UOW;

namespace Yooper.UI
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        private IUnitofWork _uow;
        public Dashboard()
        {
            InitializeComponent();
            MainSection.Content = new DashboardContent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
        }

        private void btnChannel_Click(object sender, RoutedEventArgs e)
        {
            if (Helper.CheckRestart(SessionItem.UserId))
            {
                new MainWindow().Show();
                this.Close();
            }
            MainSection.Content = new Channel();
        }

        private void btnNumber_Click(object sender, RoutedEventArgs e)
        {
            if (Helper.CheckRestart(SessionItem.UserId))
            {
                new MainWindow().Show();
                this.Close();
            }
            MainSection.Content = new NumberContent();
        }

        private void btnMessage_Click(object sender, RoutedEventArgs e)
        {
            if (Helper.CheckRestart(SessionItem.UserId))
            {
                new MainWindow().Show();
                this.Close();
            }
            MainSection.Content = new MessageContent();
        }

        private void btn_group_Click(object sender, RoutedEventArgs e)
        {
            //START CHECKING IF ADMIN HAS RESTARTED THIS APPLICATION OR NOT.
            
            if (Helper.CheckRestart(SessionItem.UserId))
            {
                new MainWindow().Show();
                this.Close();
            }
            //END CHECKING IF ADMIN HAS RESTARTED THIS APPLICATION OR NOT.

            MainSection.Content = new Group();
        }

        private void grid_mainHeader_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void btn_shutdown_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_uow.Account.CheckChannelStatus())
                {
                    SessionItem._whatsapp.EndSession();
                    _uow.Account.MakeChangeInChannelStatus(false);
                }
                new MainWindow().Show();
                this.Close();
                
            }
            catch 
            {
             
            }
        }

        private void btn_dashboard_Click(object sender, RoutedEventArgs e)
        {

            if (Helper.CheckRestart(SessionItem.UserId))
            {
                new MainWindow().Show();
                this.Close();
            }
            MainSection.Content = new DashboardContent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //SessionItem._whatsapp.Minimize();
                this.WindowState = WindowState.Minimized;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Connect your whatsapp.");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                //SessionItem._whatsapp.Maximize();
                this.WindowState = WindowState.Normal;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Connect your whatsapp.");
            }
        }

        private void btn_whatsapp_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_uow.Account.CheckChannelStatus())
                {
                    SessionItem._whatsapp.EndSession();
                    _uow.Account.MakeChangeInChannelStatus(false);
                }

                new MainWindow().Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
