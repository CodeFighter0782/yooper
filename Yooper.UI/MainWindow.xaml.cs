﻿    using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IUnitofWork _uow;
        public MainWindow()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
        }


        private void CheckRestartBP(long userId)
        {
            while (true)
            {
                if (_uow.Account.CheckIfApplicationRestart(userId))
                {
                    _uow.Account.EnableRestart(userId);
                }
                Thread.Sleep(5000);
            }
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                login.IsEnabled = false;
                login.Content = "Loading...";
                var thread = new Thread(x => { LoginWrapper(); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            catch (YooperException ex)
            {
                MessageBox.Show(ex.ErrorMessage);
            }
        }

        private void txt_key_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                login.IsEnabled = false;
                login.Content = "Loading...";
                var thread = new Thread(x => { LoginWrapper(); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }


        private void LoginWrapper()
        {
            try
            {
                string username = string.Empty, password = string.Empty, key = string.Empty;
                this.Dispatcher.Invoke(() =>
                {
                    username = txt_username.Text.ToString();
                    password = txt_password.Password.ToString();
                    key = txt_key.Text.ToString();
                });
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(key))
                {
                    if (string.IsNullOrEmpty(username))
                    {
                        MessageBox.Show("Username is required");
                    }
                    else if (string.IsNullOrEmpty(password))
                    {
                        MessageBox.Show("Password is required");
                    }
                    else if (string.IsNullOrEmpty(key))
                    {
                        MessageBox.Show("License key is missing");
                    }
                }
                else
                {
                    var user = _uow.Account.Login(username, password, key);
                    if (user != null)
                    {

                        _uow.Account.AddUser(new DTO.SqliteCustomUsers()
                        {
                            LicenseKey = user.SecretKey,
                            CreatedBy = user.Username,
                            CreatedDate = DateTime.UtcNow.ToString(),
                            Enable = 1,
                            IsLogin = 1,
                            Restart = 0,
                            TwoWayAuthentication = 0,
                            Username = user.Username,
                            UserId = (int)user.Id,
                            UpdatedBy=string.Empty,
                            UpdatedDate= string.Empty
                        });
                        //MessageBox.Show("Login successful");

                        SessionItem.Username = user.Username;
                        SessionItem.UserId = user.Id;

                        this.Dispatcher.Invoke(() =>
                        {
                            new Dashboard().Show();
                            this.Close();
                        });

                        var thread = new Thread(x => { CheckRestartBP(user.Id); }) { IsBackground = true };
                        thread.SetApartmentState(ApartmentState.STA);
                        thread.Start();
                        _uow.Account.MakeUserLogin(true);
                    }
                    else
                    {
                        _uow.Account.MakeUserLogin(false);
                        MessageBox.Show("username/password is invalid");
                    }
                    this.Dispatcher.Invoke(() =>
                    {
                        txt_username.Text = "";
                        txt_password.Password = "";
                        txt_key.Text = "";
                    });

                }
                this.Dispatcher.Invoke(() =>
                {
                    login.IsEnabled = true;
                    login.Content = "PROCEED";
                });

            }
            catch (YooperException ex)
            {
                MessageBox.Show(ex.ErrorMessage);
                this.Dispatcher.Invoke(() =>
                {
                    login.IsEnabled = true;
                    login.Content = "PROCEED";
                });
            }
        }

        private void CheckIfAlreadyLogin()
        {
            var obj = _uow.Account.GetCustomUser().FirstOrDefault();
            if (obj != null)
            {
                SessionItem.Username = obj.Username;
                SessionItem.UserId = obj.UserId;
                new Dashboard().Show();
                this.Close();
                var thread = new Thread(x => { CheckRestartBP(obj.UserId); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }
    }
}
