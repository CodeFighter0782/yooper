﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using Yooper.UI;

namespace Whatsapp
{
    public class WhatsappWrapper
    {
        IWebDriver driver;
        public string windowId = string.Empty;
        public IReadOnlyList<Cookie> cookie = new List<Cookie>();
        private string WhatsAppGroupUrl = "https://chat.whatsapp.com/";

        public bool CheckLoggedIn()
        {
            try
            {
                //return driver.FindElement(By.ClassName("_2Uo0Z")).Displayed;
                return driver.FindElement(By.ClassName("_1WliW")).Displayed;
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
                return false;
            }
        }

        public void AutoReply(string message)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                var new_message = driver.FindElements(By.ClassName("OUeyt"));
                foreach (var item in new_message)
                {
                    var reply_obj = item.FindElement(By.XPath("ancestor::div[6]"));
                    reply_obj.Click();
                    var inputField = driver.FindElement(By.CssSelector("._2S1VP.copyable-text.selectable-text"));
                    inputField.SendKeys(message);
                    var temp = driver.FindElement(By.CssSelector("._35EW6"));
                        temp.Click();
                }
            }
            catch(Exception ex)
            {
                Debug.Write(ex.Message);
                throw ex;
            }
        }

        public void sendMessageToWhatsAppGroup(string WhatsAppGroupId,string message, bool isAttachment = false)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
                driver.Navigate().GoToUrl(WhatsAppGroupUrl + WhatsAppGroupId);
                while (true)
                {
                    try
                    {
                        driver.FindElement(By.Id("action-button")).Click();
                        break;
                    }
                    catch
                    {
                    }
                }
                if (!isAttachment)
                {
                    while (true)
                    {
                        driver.FindElement(By.ClassName("_2S1VP")).SendKeys(message);
                        var temp = driver.FindElement(By.ClassName("_35EW6"));
                            temp.Click();
                        break;
                    }
                }
                else
                {
                    while (true)
                    {
                        try
                        {
                            var _selectors = driver.FindElements(By.XPath("//div[@class='_3Kxus']/div[@class='rAUz7']"));
                            _selectors[1].Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    var inner_selector = driver.FindElements(By.XPath("//div[@class='_3ZRwN']/div/ul/li"));
                    var input = inner_selector[2].FindElement(By.XPath("//button/input"));
                    input.SendKeys(message);
                    Thread.Sleep(1000);
                    driver.FindElement(By.CssSelector("._3hV1n.yavlE")).Click();
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void CreateGroups(List<string> numbers, string message, out string getGroupId)
        {
            string _getGroupId = string.Empty;
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                Console.WriteLine("Please wait...!");
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);//Wait for maximun of 10 seconds if any element is not found
                //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                //sendButton.Click();
                //Click SEND Buton
                var dropdown = driver.FindElements(By.ClassName("rAUz7"));
                dropdown[2].Click();
                var lis = driver.FindElements(By.ClassName("_3L0q3"));
                lis[0].Click();
                var inputField = driver.FindElement(By.ClassName("_16RnB"));
                foreach (var item in numbers)
                {
                    inputField.Clear();
                    inputField.SendKeys(item);
                    Thread.Sleep(2000);
                    var block = driver.FindElements(By.ClassName("RLfQR"));
                    if (block.Count > 0)
                    {
                        block[0].Click();
                    }
                }
                var button = driver.FindElement(By.ClassName("_3hV1n"));
                button.Click();
                Thread.Sleep(2000);
                var groupNameInput = driver.FindElement(By.ClassName("_2S1VP"));
                int rand = new Random().Next(1000, 9999);
                groupNameInput.SendKeys("test" + rand.ToString());
                driver.FindElement(By.ClassName("_3hV1n")).Click();
                Thread.Sleep(5000);
                var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                groupTag.Click();

                var invite = driver.FindElements(By.ClassName("_2EXPL"));
                invite[1].Click();

                var groupIdAnchor = driver.FindElement(By.Id("group-invite-link-anchor"));
                string groupId = groupIdAnchor.Text;
                Thread.Sleep(1000);
                string actualId = groupId.Substring(groupId.LastIndexOf('/')).Replace('/', ' ').TrimStart();
                Console.WriteLine(actualId);


                driver.Navigate().GoToUrl(WhatsAppGroupUrl + actualId);
                driver.FindElement(By.Id("action-button")).Click();
                Console.WriteLine("Done...........");

                var input1 = driver.FindElement(By.ClassName("_2S1VP"));
                input1.Clear();
                input1.SendKeys(message);
                driver.FindElement(By.ClassName("_35EW6")).Click();

                _getGroupId = actualId;
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception :" + ex.Message);
            }
            getGroupId = _getGroupId;
        }

        public void AddNewGroup(string GroupName,string Number, string GroupId,out string WhatsAppGroupId)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                if (string.IsNullOrEmpty(GroupId))
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);//Wait for maximun of 10 seconds if any element is not found
                                                                                       //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                                                                                       //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                                                                                       //sendButton.Click();
                                                                                       //Click SEND Buton
                    driver.Navigate().Refresh();
                    while (true)
                    {
                        try
                        {
                            var dropdown = driver.FindElements(By.ClassName("rAUz7"));
                            dropdown[2].Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    Thread.Sleep(2000);
                    var lis = driver.FindElements(By.ClassName("_3L0q3"));
                    lis[0].Click();
                    var inputField = driver.FindElement(By.ClassName("_16RnB"));

                    inputField.Clear();
                    inputField.SendKeys(Number);
                    Thread.Sleep(2000);
                    var block = driver.FindElements(By.ClassName("RLfQR"));
                    if (block.Count > 0)
                    {
                        block[0].Click();
                    }
                    Thread.Sleep(2000);
                    var button = driver.FindElement(By.ClassName("_3hV1n"));
                    button.Click();
                    var groupNameInput = driver.FindElement(By.ClassName("_2S1VP"));
                    groupNameInput.SendKeys(GroupName);
                    var temp = driver.FindElement(By.ClassName("_3hV1n"));
                    temp.Click();
                    while (true)
                    {
                        try
                        {
                            var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                            groupTag.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }

                    while (true)
                    {
                        try
                        {
                            var invite = driver.FindElements(By.ClassName("_2EXPL"));
                            invite[1].Click();
                            break;
                        }
                        catch
                        {
                        }
                    }

                    var groupIdAnchor = driver.FindElement(By.Id("group-invite-link-anchor"));
                    string groupId = groupIdAnchor.Text;
                    Thread.Sleep(1000);
                    string actualId = groupId.Substring(groupId.LastIndexOf('/')).Replace('/', ' ').TrimStart();
                    Console.WriteLine(actualId);


                    //driver.Navigate().GoToUrl(WhatsAppGroupUrl + actualId);
                    //driver.FindElement(By.Id("action-button")).Click();
                    Console.WriteLine("Done...........");

                    //var input1 = driver.FindElement(By.ClassName("_2S1VP"));
                    //input1.Clear();
                    //input1.SendKeys(Message);
                    //driver.FindElement(By.ClassName("_35EW6")).Click();

                    WhatsAppGroupId = actualId;
                }
                else
                {
                    WhatsAppGroupId = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        

        public void OneToOneGroup(string GroupName, string Number,string Message , out string WhatsAppGroupId,bool isFirst = true)
        {
            try
            {
                if (!string.IsNullOrEmpty(Number))
                {
                    if (isFirst)
                    {
                        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);//Wait for maximun of 10 seconds if any element is not found
                                                                                           //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                                                                                           //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                                                                                           //sendButton.Click();
                                                                                           //Click SEND Buton
                        driver.Navigate().Refresh();
                        while (true)
                        {
                            try
                            {
                                var dropdown = driver.FindElements(By.ClassName("rAUz7"));
                                dropdown[2].Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        Thread.Sleep(2000);
                        var lis = driver.FindElements(By.ClassName("_3L0q3"));
                        lis[0].Click();
                        var inputField = driver.FindElement(By.ClassName("_16RnB"));

                        inputField.Clear();
                        inputField.SendKeys(Number);
                        Thread.Sleep(2000);
                        var block = driver.FindElements(By.ClassName("RLfQR"));
                        if (block.Count > 0)
                        {
                            block[0].Click();
                        }
                        Thread.Sleep(2000);
                        var button = driver.FindElement(By.ClassName("_3hV1n"));
                        button.Click();
                        var groupNameInput = driver.FindElement(By.ClassName("_2S1VP"));
                        groupNameInput.SendKeys(GroupName);
                        
                        while (true)
                        {
                            try
                            {
                                var send = driver.FindElement(By.ClassName("_3hV1n"));
                                send.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        while (true)
                        {
                            try
                            {
                                var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                                groupTag.Click();
                                break;
                            }
                            catch
                            {
                            }
                        }

                        while (true)
                        {
                            try
                            {
                                var invite = driver.FindElements(By.ClassName("_2EXPL"));
                                invite[1].Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        var groupIdAnchor = driver.FindElement(By.Id("group-invite-link-anchor"));
                        string groupId = groupIdAnchor.Text;
                        Thread.Sleep(1000);
                        string actualId = groupId.Substring(groupId.LastIndexOf('/')).Replace('/', ' ').TrimStart();
                        Console.WriteLine(actualId);

                        WhatsAppGroupId = actualId;

                        while (true)
                        {
                            try
                            {
                                var backButton = driver.FindElement(By.ClassName("_1aTxu"));
                                backButton.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            } 
                        }
                        

                        while (true)
                        {
                            try
                            {
                                var closeButton = driver.FindElement(By.ClassName("_1aTxu"));
                                closeButton.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        Thread.Sleep(2000);
                        while (true)
                        {
                            try
                            {
                                var input1 = driver.FindElement(By.XPath("//div[@class='_1Plpp']"));
                                input1.SendKeys(Message);
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }

                        while (true)
                        {
                            try
                            {
                                var send = driver.FindElement(By.ClassName("_35EW6"));
                                send.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        while (true)
                        {
                            try
                            {
                                var temp1 = driver.FindElement(By.ClassName("_5SiUq"));
                                temp1.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }



                        while (true)
                        {
                            try
                            {
                                var temp10 = driver.FindElements(By.XPath("//div[@class='_1ZiJ3']"));
                                temp10[1].Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        while (true)
                        {
                            try
                            {
                                var input = driver.FindElement(By.ClassName("jN-F5"));
                                input.SendKeys(Number);
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                        Thread.Sleep(2000);
                        while (true)
                        {
                            try
                            {
                                var temp11 = driver.FindElements(By.ClassName("ZR5SB"))[0];
                                temp11.Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        Thread.Sleep(1000);
                        while (true)
                        {
                            try
                            {
                                var temp12 = driver.FindElements(By.ClassName("_3L0q3"))[1];
                                temp12.Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        Thread.Sleep(3000);
                        while (true)
                        {
                            try
                            {
                                var close = driver.FindElement(By.ClassName("_1aTxu"));
                                close.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                        while (true)
                        {
                            try
                            {
                                var close1 = driver.FindElement(By.ClassName("_1aTxu"));
                                close1.Click();
                                break;
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                    else
                    {
                        while (true)
                        {
                            try
                            {
                                var temp1 = driver.FindElement(By.ClassName("_5SiUq"));
                                temp1.Click();
                                break;
                            }
                            catch
                            {
                                try
                                {
                                    var close1 = driver.FindElement(By.ClassName("_1aTxu"));
                                    close1.Click();
                                    break;
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }

                        while (true)
                        {
                            try
                            {
                                var temp2 = driver.FindElements(By.ClassName("_2EXPL"));
                                temp2[0].Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }

                        while (true)
                        {
                            try
                            {
                                var temp3 = driver.FindElement(By.ClassName("jN-F5"));
                                temp3.SendKeys(Number);
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }



                        while (true)
                        {
                            try
                            {
                                var temp4 = driver.FindElement(By.ClassName("_3j7s9"));
                                temp4.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        while (true)
                        {
                            try
                            {
                                var temp5 = driver.FindElement(By.ClassName("_3hV1n"));
                                temp5.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        while (true)
                        {
                            try
                            {
                                var temp6 = driver.FindElements(By.ClassName("_1WZqU"));
                                temp6[1].Click();
                                break;
                            }
                            catch (Exception)
                            {
                            }
                        }

                        Thread.Sleep(2000);

                        while (true)
                        {
                            try
                            {
                                var temp7 = driver.FindElement(By.ClassName("_1aTxu"));
                                temp7.Click();
                                break;
                            }
                            catch (Exception)
                            {
                            } 
                        }


                        while (true)
                        {
                            try
                            {
                                var input1 = driver.FindElement(By.XPath("//div[@class='_1Plpp']"));
                                input1.SendKeys(Message);
                                break;

                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        while (true)
                        {
                            try
                            {
                                var send = driver.FindElement(By.ClassName("_35EW6"));
                                send.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }


                        WhatsAppGroupId = "Exsist";


                        while (true)
                        {
                            try
                            {
                                var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                                groupTag.Click();
                                break;
                            }
                            catch
                            {

                            }
                        }
                        Thread.Sleep(2000);
                        while (true)
                        {
                            try
                            {
                                var temp10 = driver.FindElements(By.XPath("//div[@class='_1ZiJ3']"));
                                temp10[1].Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        var input = driver.FindElement(By.ClassName("jN-F5"));
                        input.SendKeys(Number);
                        Thread.Sleep(2000);
                        while (true)
                        {
                            try
                            {
                                var temp11 = driver.FindElements(By.ClassName("ZR5SB"))[0];
                                temp11.Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        Thread.Sleep(1000);
                        while (true)
                        {
                            try
                            {
                                var temp12 = driver.FindElements(By.ClassName("_3L0q3"))[1];
                                temp12.Click();
                                break;
                            }
                            catch
                            {
                            }
                        }
                        Thread.Sleep(3000);
                        while (true)
                        {
                            try
                            {
                                var close = driver.FindElement(By.ClassName("_1aTxu"));
                                close.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                        while (true)
                        {
                            try
                            {
                                var close1 = driver.FindElement(By.ClassName("_1aTxu"));
                                close1.Click();
                                break;
                            }
                            catch (Exception)
                            {
                                
                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("Number can't be null or empty.");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }




        public void DeleteNumbersFromWhatsAppGroup(string WhatsAppGroupId, List<string> numbers)
        {
            try
            {
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                driver.Navigate().GoToUrl(WhatsAppGroupUrl + WhatsAppGroupId);

                while (true)
                {
                    try
                    {
                        var button = driver.FindElement(By.Id("action-button"));
                        button.Click();
                        break;
                    }
                    catch (Exception)
                    {
                    }
                }

                while (true)
                {
                    try
                    {
                        var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                        groupTag.Click();
                        break;
                    }
                    catch
                    {

                    }
                }
                Thread.Sleep(2000);
                while (true)
                {
                    try
                    {
                        var temp10 = driver.FindElements(By.XPath("//div[@class='_1ZiJ3']"));
                        temp10[1].Click();
                        break;
                    }
                    catch
                    {
                    }
                }
                var input = driver.FindElement(By.ClassName("jN-F5"));
                foreach (var item in numbers)
                {
                    input.Clear();
                    input.SendKeys(item);
                    Thread.Sleep(2000);
                    while (true)
                    {
                        try
                        {
                            var temp11 = driver.FindElements(By.ClassName("ZR5SB"))[0];
                            temp11.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    Thread.Sleep(1000);
                    while (true)
                    {
                        try
                        {
                            var temp12 = driver.FindElements(By.ClassName("_3L0q3"))[1];
                            temp12.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                }
                
                Thread.Sleep(3000);
                while (true)
                {
                    try
                    {
                        var close = driver.FindElement(By.ClassName("_1aTxu"));
                        close.Click();
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }
                while (true)
                {
                    try
                    {
                        var close1 = driver.FindElement(By.ClassName("_1aTxu"));
                        close1.Click();
                        break;
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public void AddNumberInNewGroup(string Number, string GroupId)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);//Wait for maximun of 10 seconds if any element is not found
                                                                                       //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                                                                                       //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                                                                                       //sendButton.Click();
                                                                                       //Click SEND Buton

                
                driver.Navigate().GoToUrl(WhatsAppGroupUrl + GroupId);
                while (true)
                {
                    try
                    {
                        var temp5 = driver.FindElement(By.Id("action-button"));
                        temp5.Click();
                        break;
                    }
                    catch
                    {
                    }
                }

                //Thread.Sleep(10000);
                while (true)
                {
                    try
                    {
                        var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                        groupTag.Click();
                        break;
                    }
                    catch
                    {
                        
                    }
                }

                while (true)
                {
                    try
                    {
                        var invite = driver.FindElements(By.ClassName("_2EXPL"));
                        invite[0].Click();
                        break;
                    }
                    catch
                    {
                        
                    }
                }

                var groupIdAnchor = driver.FindElement(By.ClassName("jN-F5"));
                groupIdAnchor.Clear();
                groupIdAnchor.SendKeys(Number.ToString());
                Thread.Sleep(2000);
                while (true)
                {
                    try
                    {
                        var elem = driver.FindElements(By.ClassName("_2EXPL"));
                        elem[0].Click();
                        break;
                    }
                    catch 
                    {
                    }
                }
                while (true)
                {
                    try
                    {
                        var temp1 = driver.FindElement(By.ClassName("eTCKi"));
                        temp1.Click();
                        break;
                    }
                    catch 
                    {
                    }
                }
                while (true)
                {
                    var temp2 = driver.FindElements(By.ClassName("_1WZqU"))[1];
                        temp2.Click();
                    break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void RemoveNumberFromGroup(string Number, string GroupId)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                if (!string.IsNullOrEmpty(GroupId))
                {
                    driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.Navigate().GoToUrl(WhatsAppGroupUrl + GroupId);
                    while (true)
                    {
                        try
                        {
                            var actionButton =  driver.FindElement(By.Id("action-button"));
                            actionButton.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    //Thread.Sleep(10000);
                    while (true)
                    {
                        try
                        {
                            var groupTag = driver.FindElement(By.ClassName("_5SiUq"));
                            groupTag.Click();
                            break;
                        }
                        catch
                        {

                        } 
                    }
                    while (true)
                    {
                        try
                        {
                            var temp3 = driver.FindElement(By.ClassName("_1ZiJ3"));
                            temp3.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    var input = driver.FindElement(By.ClassName("jN-F5"));
                    input.SendKeys(Number);
                    Thread.Sleep(2000);
                    while (true)
                    {
                        try
                        {
                            var temp1 = driver.FindElements(By.ClassName("ZR5SB"))[0];
                            temp1.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }
                    Thread.Sleep(1000);
                    while (true)
                    {
                        try
                        {
                            var temp2 = driver.FindElements(By.ClassName("_3L0q3 "))[1];
                            temp2.Click();
                            break;
                        }
                        catch
                        {
                        }
                    }

                }
                else
                {
                    throw new Exception("somthing went wrong.");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public void SendMessage(string number, string message)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5); //Wait for maximun of 10 seconds if any element is not found
                                                                                    //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                                                                                    //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                                                                                    //sendButton.Click();// Click SEND Buton

                //var searchInput = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                //searchInput.SendKeys(number);
                //var checkBox = driver.FindElement(By.CssSelector("._2EXPL.aZ91u"));
                //checkBox.Click();
                //var tempSendButton = driver.FindElement(By.CssSelector("._3hV1n.yavlE"));//Click SEND Arrow Button
                //tempSendButton.Click();
                //driver.FindElement(By.CssSelector("._35EW6")).Click();
                //var selectors = driver.FindElements(By.ClassName("rAUz7"));
                //if (selectors.Count > 0)
                //{
                //    selectors[1].Click();

                //}
                string url = string.Format("https://web.whatsapp.com/send/?phone={0}&text={1}", number, message);

                driver.Navigate().GoToUrl(url);
                //var newChatSearch = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                ////newChatSearch.SendKeys("");
                //newChatSearch.Clear();
                //newChatSearch.SendKeys(number);
                //Thread.Sleep(10000);
                //var temp = driver.FindElement(By.CssSelector("._1AKfk"));
                //if (temp.Text == "CHATS" || temp.Text == "CONTACTS")
                //{
                //var nameWrapper = driver.FindElement(By.CssSelector("._2EXPL"));
                //nameWrapper.Click();
                //Thread.Sleep(2000);

                //var inputField = driver.FindElement(By.CssSelector("._2S1VP.copyable-text.selectable-text"));
                //inputField.SendKeys(message);
                //Thread.Sleep(1000);
                bool invalid = false;
                while (true)
                {
                    try
                    {
                        
                        var sendButton = driver.FindElement(By.ClassName("_35EW6"));
                        sendButton.Click();
                        break;
                    }
                    catch
                    {
                        try
                        {
                            var invalidDiv = driver.FindElement(By.ClassName("_3lLzD"));
                            string text = invalidDiv.Text;
                            if (text.Contains("Phone number shared via url is invalid."))
                            {
                                invalid = true;
                                break;
                            }
                        }
                        catch
                        {

                        }
                    }
                }
                if (invalid)
                {
                    throw new Exception("not exist in whatsapp");

                }
                
                //}
                //else
                //{
                //    Console.WriteLine("Record not found");
                //}

                //var searchButton = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                //searchButton.SendKeys(number);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void SendMessage(string number, string message, bool isAttachment = true)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                Console.WriteLine("Please wait...!");
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5); //Wait for maximun of 10 seconds if any element is not found
                //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                //sendButton.Click();// Click SEND Buton

                //var searchInput = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                //searchInput.SendKeys(number);
                //var checkBox = driver.FindElement(By.CssSelector("._2EXPL.aZ91u"));
                //checkBox.Click();
                //var tempSendButton = driver.FindElement(By.CssSelector("._3hV1n.yavlE"));//Click SEND Arrow Button
                //tempSendButton.Click();
                //driver.FindElement(By.CssSelector("._35EW6")).Click();

                string url = string.Format("https://web.whatsapp.com/send/?phone={0}", number);

                driver.Navigate().GoToUrl(url);
                //var selectors = driver.FindElements(By.ClassName("rAUz7"));
                //if (selectors.Count > 0)
                //{
                //    selectors[1].Click();

                //}

                //var newChatSearch = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                ////newChatSearch.SendKeys("");
                //newChatSearch.Clear();
                //newChatSearch.SendKeys(number);
                //Thread.Sleep(5000);
                //var temp = driver.FindElement(By.CssSelector("._1AKfk"));

                //var nameWrapper = driver.FindElement(By.CssSelector("._2EXPL"));
                //nameWrapper.Click();
                while (true)
                {
                    try
                    {
                        var _selectors = driver.FindElements(By.XPath("//div[@class='_3Kxus']/div[@class='rAUz7']"));
                        _selectors[1].Click();
                        break;
                    }
                    catch
                    {
                    }
                }
                var inner_selector = driver.FindElements(By.XPath("//div[@class='_3ZRwN']/div/ul/li"));
                //////
                ///TESTING FOR UNDERSTANDING
                ///
                //var text_box = driver.FindElement(By.ClassName("klasdj"));
                //text_box.SendKeys("")

                var input = inner_selector[2].FindElement(By.XPath("//button/input"));
                input.SendKeys(message);
                Thread.Sleep(1000);

                var sendButton = driver.FindElement(By.ClassName("_3hV1n"));
                sendButton.Click();

                //var searchButton = driver.FindElement(By.CssSelector(".jN-F5.copyable-text.selectable-text"));
                //searchButton.SendKeys(number);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void SendCampaign(string number, string[] messages, string[] attachments)
        {
            try
            {
                while (!Helper.CheckForInternetConnection())
                {

                }
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5); //Wait for maximun of 10 seconds if any element is not found
                                                                                   //driver.Navigate().GoToUrl("https://api.whatsapp.com/send?phone=" + number + "&text=" + Uri.EscapeDataString(message));
                                                                                   //var sendButton = driver.FindElement(By.CssSelector("a.button.button--simple.button--primary"));
                                                                                   //sendButton.Click();// Click SEND Buton
                string url = string.Format("https://web.whatsapp.com/send/?phone={0}", number);
                driver.Navigate().GoToUrl(url);
                bool invalid = false;
                while (true)
                {
                    try
                    {
                        if (messages.Length>0)
                        {
                            foreach (var item in messages)
                            {
                                var textBox = driver.FindElement(By.XPath("//div[@class='_1Plpp']"));
                                //textBox.Clear();
                                textBox.SendKeys(item);
                                Thread.Sleep(1000);
                                var sendButton = driver.FindElement(By.ClassName("_35EW6"));
                                sendButton.Click();
                                Thread.Sleep(2000);
                            }
                        }
                        if (attachments.Length > 0)
                        {
                            foreach (var item in attachments)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    while (true)
                                    {
                                        try
                                        {
                                            var _selectors = driver.FindElements(By.XPath("//div[@class='_3Kxus']/div[@class='rAUz7']"));
                                            _selectors[1].Click();
                                            break;
                                        }
                                        catch
                                        {
                                        }
                                    }
                                    var inner_selector = driver.FindElements(By.XPath("//div[@class='_3ZRwN']/div/ul/li"));
                                    var input = inner_selector[2].FindElement(By.XPath("//button/input"));
                                    input.SendKeys(item);
                                    Thread.Sleep(1000);
                                    var sendButton = driver.FindElement(By.ClassName("_3hV1n"));
                                    sendButton.Click();
                                    Thread.Sleep(2000); 
                                }
                            }
                        }
                        
                        break;
                    }
                    catch(Exception ex)
                    {
                        try
                        {
                            var invalidDiv = driver.FindElement(By.ClassName("_3lLzD"));
                            string text = invalidDiv.Text;
                            if (text.Contains("Phone number shared via url is invalid."))
                            {
                                invalid = true;
                                break;
                            }
                        }
                        catch
                        {

                        }
                    }
                }
                if (invalid)
                {
                    throw new Exception("not exist in whatsapp");

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Minimize()
        {
            while (!Helper.CheckForInternetConnection())
            {

            }
            this.driver.Manage().Window.Minimize();
        }

        public void Maximize()
        {
            while (!Helper.CheckForInternetConnection())
            {

            }
            this.driver.Manage().Window.Maximize();
        }

        public void run()
        {
            try
            {
                ChromeDriverService service = ChromeDriverService.CreateDefaultService(); 

                service.HideCommandPromptWindow = true;
                //ChromeOptions options = new ChromeOptions();
                //options.AddArgument("user-data-dir=C:\\Users\\Username\\AppData\\Local\\Google\\Chrome\\User Data");


                driver = new ChromeDriver(service);

                
                this.windowId = driver.CurrentWindowHandle;
                if (!CheckLoggedIn())
                {
                    foreach (var item in cookie)
                    {
                        driver.Manage().Cookies.AddCookie(item);
                    }
                    driver.Navigate().GoToUrl("https://web.whatsapp.com");

                    while (true)
                    {
                        if (CheckLoggedIn())
                        {
                            cookie = driver.Manage().Cookies.AllCookies;
                            break;
                        }

                    }
                }
                else
                {
                    driver = driver.SwitchTo().Window(this.windowId);
                }
            }
            catch (Exception ex)
            {

                //throw ex;
                Debug.Write(ex.Message);
            }

        }

        public void run(string phoneNumber, string message)
        {
            if (string.IsNullOrEmpty(this.windowId))
            {
                driver = new ChromeDriver();
                this.windowId = driver.CurrentWindowHandle;
                if (!CheckLoggedIn())
                {
                    driver.Navigate().GoToUrl("https://web.whatsapp.com");

                    while (true)
                    {
                        //Console.WriteLine("Login to WhatsApp Web and Press Enter");
                        //Console.ReadLine();
                        if (CheckLoggedIn())
                            break;
                    }
                }
                else
                {
                    driver = driver.SwitchTo().Window(this.windowId);
                }
            }

            SendMessage(phoneNumber, message); //*** Replace here ***//
        }


        public void EndSession()
        {
            this.driver.Quit();
        }
    }
}
