﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for ManageAutoReply.xaml
    /// </summary>
    public partial class ManageAutoReply : Window
    {
        private ObservableCollection<AutoReplyVM> autoReply = new ObservableCollection<AutoReplyVM>();
        private IUnitofWork _uow;
        public ManageAutoReply()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            this.PopulateGrid();
        }

        private void PopulateGrid()
        {
            try
            {
                dataGrid.ItemsSource = _uow.Account.GetAutoReplyMessages().Select(x => new AutoReplyVM()
                {
                    Id = x.Id,
                    Message = x.Message,
                    CreatedDate = x.CreatedDate,
                    SetAsDefault = x.SetAsReply == 1 ? "TRUE" : "FALSE"
                }).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }


        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var obj = dataGrid.SelectedItem as AutoReplyVM;
            if (obj!=null)
            {
                textBox.Text = obj.Id.ToString() ;
                textBox1.Text = obj.Message;
                button.IsEnabled = false;
                button1.IsEnabled = true;
                button2.IsEnabled = true;
                button3.IsEnabled = true;
            }
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    throw new Exception("Message field is requried.");
                }
                _uow.Account.SaveAutoReply(textBox1.Text);
                this.Clear();
                this.PopulateGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(textBox1.Text))
                {
                    throw new Exception("Message field is requried.");
                }
                _uow.Account.SaveAutoReply(Convert.ToInt32(textBox.Text), textBox1.Text, true);
                this.PopulateGrid();
                Clear();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _uow.Account.DeleteAutoReply(Convert.ToInt32(textBox.Text));
                this.PopulateGrid();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _uow.Account.SetAsAutoReplyMessage(Convert.ToInt32(textBox.Text));
                this.PopulateGrid();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            this.Clear();
        }

        private void Clear()
        {
            textBox.Text = "";
            textBox1.Text = "";
            button1.IsEnabled = false;
            button2.IsEnabled = false;
            button3.IsEnabled = false;
            button.IsEnabled = true;
            dataGrid.UnselectAll();
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _uow.Account.UnableAllReplyMessages();
                MessageBox.Show("Reply feature is disabled successfully.");
                this.PopulateGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Exception");
            }
        }
    }
}
