﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.UOW;
using Ninject;
using System.Threading;
using System.Collections.ObjectModel;
using Yooper.DTO;
using Yopper.Common;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for SendAllWin.xaml
    /// </summary>
    public partial class SendAllWin : Window
    {
        private ObservableCollection<NumberStatusVM> changeset = new ObservableCollection<NumberStatusVM>();
        private IUnitofWork _uow;
        public SendAllWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            AddMessageToTextBox();
            AddAllNumbers();
            UpdateMessageSent();
        }

        //Update Sending Status////////////////////////////////////////////////
        private void UpdateMessageSent()
        {
            dataGrid.ItemsSource = changeset;
        }

        //////////////////////Adding message to textbox/////////////////////////////////////////
        private void AddMessageToTextBox()
        {
            string message = SessionItem.item["message"].ToString();
            SessionItem.item.Remove("message");
            textBox1.Text = message;
        }


        /////////////////////Adding all numbers to textbox with comma seperation////////////////////////////
        private void AddAllNumbers()
        {
            var numbers= _uow.Account.GetNumbers();
            string numberString = string.Empty;
            foreach (var item in numbers)
            {
                numberString += item.PhoneNumber+",";
            }
            textBox.Text = numberString;
        }
        
        //Update Status Function/////////////////////////////////////////////////////
        private void AddStatus(string number, string Status = "Successful")
        {
            this.Dispatcher.Invoke(() => { this.changeset.Add(new NumberStatusVM { Number = number, Status = Status, Time = DateTime.Now.ToString() }); });
        }

        //Custom Timer Method///////////////////////////////////////////////////////
        private void Timer(int seconds)
        {
            for (int i = seconds; i > 0; i--)
            {
                this.Dispatcher.Invoke(() => {
                    label2.Content = "Time Left: " + i + "second";
                });
                Thread.Sleep(1000);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (!_uow.Account.CheckChannelStatus())
            {
                MessageBox.Show("Please connect to WhatsApp through channel.");
            }
            else
            {
                ButtonOperation(true, false, true);
                button.Content = "SENDING..."; button.IsEnabled = false;
                var thread = new Thread(() => { this.SendGroupMessage(); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }
        
        //Sending message to all numbers in the textbox//////////////////////////////////////////////////////////
        private void SendGroupMessage()
        {
            try
            {
                string[] numbers = null;
                string message = string.Empty;
                this.Dispatcher.Invoke(() =>
                {
                    numbers = textBox.Text.Split(',');
                    message = textBox1.Text;
                });

                if (numbers != null)
                {
                    double totalRecord = numbers.Length, count = 1, percentage = 0;
                    foreach (var item in numbers)
                    {
                        while (_uow.Setting.Get().FirstOrDefault().Pause == 1)
                        {

                        }
                        if (_uow.Setting.Get().FirstOrDefault().Stop==1)
                        {
                            break;
                        }
                        if (!string.IsNullOrEmpty(item))
                        {
                            var numberObj = _uow.Account.GetNumbers(item).FirstOrDefault();
                            if (Convert.ToBoolean(SessionItem.item["IsText"]))
                            {
                                try
                                {
                                    SessionItem._whatsapp.SendMessage(item, message);
                                    this.AddStatus(item);
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, true);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 1, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                        _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                        {
                                            Message = message,
                                            Number = item,
                                            MessageType = EMessageType.GROUP.ToString()
                                        });
                                    }
                                }
                                catch
                                {
                                    this.AddStatus(item, "Number is invalid");
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, false);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    SessionItem._whatsapp.SendMessage(item, message, true);
                                    this.AddStatus(item);
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, true);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 1, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                        _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                        {
                                            Message = message,
                                            Number = item,
                                            MessageType = EMessageType.GROUP.ToString()
                                        });
                                    }
                                }
                                catch
                                {
                                    this.AddStatus(item, "Number is invalid");
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, false);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                    }
                                }
                            }

                        }
                        var _thread = new Thread(() => { this.Timer(SessionItem.Delay); }) { IsBackground = true };
                        _thread.SetApartmentState(ApartmentState.STA);
                        _thread.Start();
                        Thread.Sleep(1000 * SessionItem.Delay);
                        percentage = (count / totalRecord) * 100;
                        this.Dispatcher.Invoke(() =>
                        {
                            label3.Content = percentage.ToString("0.00") + "% Completed";
                        });
                        count++;
                        
                    }
                }
                this.Dispatcher.Invoke(() =>
                {
                    button.IsEnabled = false;
                    button.Content = "SEND";
                });
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            ButtonOperation(false, true, false);
            var settingObj = _uow.Setting.Get().FirstOrDefault();
            settingObj.Pause = 1;
            settingObj.Stop = 0;
            _uow.Setting.Update(settingObj.Id, settingObj);
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            ButtonOperation(true, false, true);
            var settingObj = _uow.Setting.Get().FirstOrDefault();
            settingObj.Pause = 0;
            settingObj.Stop = 0;
            _uow.Setting.Update(settingObj.Id, settingObj);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            ButtonOperation(false, false, false);
            var settingObj = _uow.Setting.Get().FirstOrDefault();
            settingObj.Pause = 0;
            settingObj.Stop = 1;
            _uow.Setting.Update(settingObj.Id, settingObj);
        }

        private void ButtonOperation(bool Pause, bool Resume, bool Stop)
        {
            button2.IsEnabled = Pause;
            button3.IsEnabled = Resume;
            button1.IsEnabled = Stop;
        }
    }
}
