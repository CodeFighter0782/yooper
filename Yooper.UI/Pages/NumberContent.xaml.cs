﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for NumberContent.xaml
    /// </summary>

    public partial class NumberContent : Page
    {
        public List<string> number_List = new List<string>();

        IUnitofWork _uow;
        public NumberContent()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            this.UpdateDataGrid();
        }

        private void UpdateDataGrid()
        {
            var model = _uow.Account.GetNumbers().AsEnumerable().Select(x => new NumberVM()
            {
                ID = x.Id,
                Name = x.Name,
                Number = x.PhoneNumber,
                CreatedDate = x.CreatedDate

            }).ToList();

            int count = 1;
            foreach (var item in model)
            {
                item.ID = count;
                count++;
            }
            number_grid.ItemsSource = model;
        }

        private void add_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //if (name_txtbx.Text == "")
                //{
                //    MessageBox.Show("Name Field Empty");
                //}
                if (number_txtbx.Text == "")
                {
                    MessageBox.Show("Number Field Empty");
                }
                else
                {
                    var numberListTemp = _uow.Account.GetNumbers().ToList();
                    string number = number_txtbx.Text.ToString().Trim().Replace(" ", "");
                    if (numberListTemp.Count != 0)
                    {
                        for (int z = 0; z < numberListTemp.Count; z++)
                        {
                            if (numberListTemp[z].PhoneNumber.Equals(number))
                            {
                                MessageBox.Show("This Number is already exist in database");
                                break;
                            }
                            else if (z == (numberListTemp.Count - 1))
                            {
                                _uow.Account.AddNumber(name_txtbx.Text, number);
                                this.UpdateDataGrid();
                                this.ResetAll();
                            }
                        }
                    }
                    else {
                        _uow.Account.AddNumber(name_txtbx.Text, number);
                        this.UpdateDataGrid();
                        this.ResetAll();
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void update_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (name_txtbx.Text == "")
                {
                    MessageBox.Show("Name Field Empty");
                }
                else if (number_txtbx.Text == "")
                {
                    MessageBox.Show("Number Field Empty");
                }
                else
                {
                    _uow.Account.AddNumber(Convert.ToInt64(id_lbl.Content), name_txtbx.Text, number_txtbx.Text);
                    this.UpdateDataGrid();
                    this.ResetAll();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
        }

        private void delete_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    _uow.Account.DeleteNumber(Convert.ToInt32(id_lbl.Content));
                    this.UpdateDataGrid();
                    this.ResetAll();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void ResetAll()
        {
            id_lbl.Content = "";
            name_txtbx.Text = "";
            number_txtbx.Text = "";
            add_btn.IsEnabled = true;
            update_btn.IsEnabled = false;
            delete_btn.IsEnabled = false;
            number_grid.UnselectAll();
        }

        private void reset_btn_Click(object sender, RoutedEventArgs e)
        {
            this.ResetAll();
        }

        private void number_grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }

        private void number_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var item = number_grid.SelectedItem;
                if (number_grid.SelectedItem != null)
                {
                    var obj = (NumberVM)number_grid.SelectedItem;
                    if (obj != null)
                    {
                        id_lbl.Content = obj.ID;
                        name_txtbx.Text = obj.Name;
                        number_txtbx.Text = obj.Number;
                        add_btn.IsEnabled = false;
                        update_btn.IsEnabled = true;
                        delete_btn.IsEnabled = true;
                    }
                }
            }
            catch
            {
            }
            
        }
        
        private void Choose_file_btn_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new Microsoft.Win32.OpenFileDialog(); // open file dialog
            var result = ofd.ShowDialog();
            if (result == false) return;
            TextReader tr = new StreamReader(ofd.FileName); // read file
             
            string row_Val;
            while ((row_Val = tr.ReadLine()) != null) {

                var numberListTemp = _uow.Account.GetNumbers().ToList();

                if (numberListTemp.Count != 0)
                {
                    for (int z = 0; z < numberListTemp.Count; z++)
                    {
                        string row_value = row_Val != null ? row_Val.ToString().TrimEnd() : "";
                        if (!row_value.Contains(","))
                        {
                            if (numberListTemp[z].PhoneNumber.Equals(row_value))
                            {
                                //MessageBox.Show("This Number is already exist in database");
                                break;
                            }
                            else if (z == (numberListTemp.Count - 1))
                            {
                                add_Number("", row_Val.Trim().Replace(" ",""));
                            }
                        }
                        else
                        {
                            string[] temp = row_value.Split(',');
                            if (numberListTemp[z].PhoneNumber.Equals(temp[0].Trim().Replace(" ","")))
                            {
                                //MessageBox.Show("This Number is already exist in database");
                                break;
                            }
                            else if (z == (numberListTemp.Count - 1))
                            {
                                add_Number(temp[1], temp[0].Trim().Replace(" ",""));
                            }
                        }
                    }
                }
                else {
                    string row_value = row_Val != null ? row_Val.ToString().TrimEnd() : "";
                    string[] temp = row_value.Split(',');
                    if (row_Val.Contains(","))
                    {
                        add_Number(temp[1], temp[0].Trim().Replace(" ",""));
                    }
                    else
                    {
                        add_Number("", row_value.Trim().Replace(" ",""));
                    }
                }
            }
            UpdateDataGrid();
        }

        private void add_Number(string name, string number) {
            try
            {
                _uow.Account.AddNumber(name, number);
                //this.UpdateDataGrid();
                //this.ResetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
        }

        private void Referesh_list_btn_Click(object sender, RoutedEventArgs e)
        {
        // refere
            this.UpdateDataGrid();
            this.ResetAll();
        }

        private void Search_txtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            number_grid.ItemsSource = _uow.Account.GetNumbers(search_txtbx.Text.ToString()).AsEnumerable().Select(x => new NumberVM()
            {
                ID = x.Id,
                Name = x.Name,
                Number = x.PhoneNumber,
                CreatedDate = x.CreatedDate
            }).ToList();
        }

        private void Number_txtbx_TextChanged(object sender, TextChangedEventArgs e)
        {
            string val= number_txtbx.Text;
            bool isIntString = val.All(char.IsDigit);
            if (!isIntString)
            {
                number_txtbx.Text = "";
            }
        }

        private void delete_all_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    _uow.Account.DeleteAllNumbers();
                    UpdateDataGrid();
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
