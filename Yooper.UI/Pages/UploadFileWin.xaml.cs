﻿using Microsoft.Win32;
using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for UploadFileWin.xaml
    /// </summary>
    public partial class UploadFileWin : Window
    {
        private ObservableCollection<FileVM> list = new ObservableCollection<FileVM>();
        IUnitofWork _uow;
        public UploadFileWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            dataGrid.ItemsSource = list;
        }

        private void LoadGrid()
        {
            try
            {
                dataGrid.ItemsSource = list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog()==true)
            {
                string fileName = Guid.NewGuid().ToString() + file.FileName.Substring(file.FileName.IndexOf('.'));
                //string workingDirectory = Environment.CurrentDirectory;
                //string projectDirectory = Directory.GetParent(workingDirectory).Parent.FullName;
                string projectDirectory = @"C:\";

                string _fileName = file.FileName;
                
                //string Path = System.IO.Path.Combine(projectDirectory,"Files", fileName);
                //string Path = System.IO.Path.Combine(projectDirectory, fileName);
                //File.Copy(file.FileName, Path);
                _uow.Account.AddFile(_fileName);
                list.Add(new FileVM { Url = _fileName, CreatedDate = DateTime.Now.ToString() });
            }
        }
    }
}
