﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for DashboardContent.xaml
    /// </summary>
    public partial class DashboardContent : Page
    {
        private IUnitofWork _uow;
        public DashboardContent()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            populateData();
        }


        public void populateData()
        {
            try
            {
                var response = _uow.Account.GetDashboardData();
                txt_Messages.Text = response.Messages.ToString();
                txtChannel.Text =  response.Channels.ToString();
                txt_Number.Text =  response.Numbers.ToString();
                txt_Groups.Text = response.Groups.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

    }
}
