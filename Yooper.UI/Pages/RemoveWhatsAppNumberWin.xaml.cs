﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.UOW;
using Ninject;
using System.Threading;
using System.Collections.ObjectModel;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for RemoveWhatsAppNumberWin.xaml
    /// </summary>
    public partial class RemoveWhatsAppNumberWin : Window
    {
        private int GroupId = 0;
        private IUnitofWork uow;

        private ObservableCollection<string> number;
        public RemoveWhatsAppNumberWin()
        {
            InitializeComponent();
            uow = App.GlobalKernel.Get<IUnitofWork>();
            try
            {
                GroupId = Convert.ToInt32(SessionItem.item["groupId"]);
            }
            catch (Exception)
            {
                GroupId = 0;
            }
            OnLoad();
        }

        private void OnLoad()
        {
            try
            {
                number = new ObservableCollection<string>();
                uow.Account.GetGroupNumbers(this.GroupId).ForEach(x=>number.Add(x.Number));
                listView.ItemsSource = number;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                button.IsEnabled = false;
                var group = uow.Group.Get(this.GroupId).FirstOrDefault();
                System.Collections.IList numbers = listView.SelectedItems;
                var obj = numbers.Cast<string>().ToList();
                obj.ForEach(x => number.Remove(x));
                var thread = new Thread(() => {
                    SessionItem._whatsapp.DeleteNumbersFromWhatsAppGroup(group.WhatsAppGroupId, obj);
                    Dispatcher.Invoke(() =>
                    {
                        deleteNumberFromDatabaseGroup(group.Id, obj);
                    });
                }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            catch (Exception ex)
            {
            }
        }

        private void deleteNumberFromDatabaseGroup(int GroupId,List<string> numbers)
        {
            try
            {
                uow.Group.DeleteFromGroupNumbers(GroupId, numbers);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
