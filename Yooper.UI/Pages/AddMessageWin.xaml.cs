﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.UOW;
using Ninject;
using Yooper.DTO;
using System.Collections.ObjectModel;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for AddMessageWin.xaml
    /// </summary>
    public partial class AddMessageWin : Window
    {
        private ObservableCollection<MessagesVM> message = new ObservableCollection<MessagesVM>();
        IUnitofWork _uow;
        public AddMessageWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            dataGrid.ItemsSource = message;
        }
        private void reset()
        {
            message_txtbx.Text = "";
        }

        private void Add_message_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(message_txtbx.Text))
                {
                    throw new Exception("Message box can not be empty.");
                }
                _uow.Account.AddMessage(message_txtbx.Text.ToString());
                message.Add(new MessagesVM()
                {
                    Id = 0,
                    Text = message_txtbx.Text,
                    CreatedDate = DateTime.Now.ToString()
                });
                this.reset();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var text = message_txtbx.Text;
                text = text + DateTime.Now.ToString();
                message_txtbx.Text = text;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
