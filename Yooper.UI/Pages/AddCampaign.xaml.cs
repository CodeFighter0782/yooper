﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.UOW;
using Ninject;
using System.Collections.ObjectModel;
using Yooper.DTO;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for AddCampaign.xaml
    /// </summary>
    public partial class AddCampaign : Window
    {
        private IUnitofWork uow;
        private int campaignId = 0;
        private ObservableCollection<string> Numbers, Messages, Files, AddedNumbers, AddedMessages, AddedFiles;
        public AddCampaign()
        {
            InitializeComponent();
            uow = App.GlobalKernel.Get<IUnitofWork>();
            try
            {
                this.campaignId = Convert.ToInt32(SessionItem.item["campaignId"].ToString());
            }
            catch
            {
                this.campaignId = 0;
            }
            OnLoad();
        }

        private void OnLoad()
        {
            try
            {
                var campaignObj = uow.Campaign.Get(this.campaignId).FirstOrDefault();
                Files = new ObservableCollection<string>(); Messages = new ObservableCollection<string>(); Numbers = new ObservableCollection<string>();
                AddedNumbers = new ObservableCollection<string>(); AddedMessages = new ObservableCollection<string>(); AddedFiles = new ObservableCollection<string>();
                if (campaignObj == null)
                {
                    
                    uow.Account.GetNumbers().Select(x => x.PhoneNumber).ToList().ForEach(x => Numbers.Add(x));
                    uow.Account.GetMessages().Where(x => x.IsText == 1).Select(x => x.Text).ToList().ForEach(x => Messages.Add(x));
                    uow.Account.GetMultimediaMessages().Where(x => x.IsMultimedia == 1).Select(x => x.Text).ToList().ForEach(x => Files.Add(x));
                    listView.ItemsSource = Numbers;
                    listView1.ItemsSource = AddedNumbers;
                    listView2.ItemsSource = Messages;
                    listView3.ItemsSource = AddedMessages;
                    listView4.ItemsSource = Files;
                    listView5.ItemsSource = AddedFiles;

                    Id.Text = this.campaignId.ToString();
                }
                else
                    {
                    uow.Account.GetNumbers().Select(x => x.PhoneNumber).ToList().ForEach(x => Numbers.Add(x));
                    uow.Account.GetMessages().Where(x => x.IsText == 1).Select(x => x.Text).ToList().ForEach(x => Messages.Add(x));
                    uow.Account.GetMultimediaMessages().Select(x => x.Text).ToList().ForEach(x => Files.Add(x));

                    string[] _AddedNumbers = campaignObj.Numbers.Split(',');
                    string[] _AddedMessages = campaignObj.Messages.Split(',');
                    string[] _AddedFiles = campaignObj.Urls.Split(',');

                    _AddedNumbers.ToList().ForEach(x => AddedNumbers.Add(x));
                    _AddedMessages.ToList().ForEach(x => AddedMessages.Add(x));
                    _AddedFiles.ToList().ForEach(x => AddedFiles.Add(x));

                    _AddedNumbers.ToList().ForEach(x => Numbers.Remove(x));
                    _AddedMessages.ToList().ForEach(x => Messages.Remove(x));
                    _AddedFiles.ToList().ForEach(x => Files.Remove(x));

                    listView.ItemsSource = Numbers;
                    listView1.ItemsSource = AddedNumbers;
                    listView2.ItemsSource = Messages;
                    listView3.ItemsSource = AddedMessages;
                    listView4.ItemsSource = Files;
                    listView5.ItemsSource = AddedFiles;
                    textBox.Text = campaignObj.Name;
                    Id.Text = campaignObj.Id.ToString();
                    checkBox.IsChecked = campaignObj.Immediate == 1 ? true : false;
                    if (!checkBox.IsChecked.GetValueOrDefault())
                    {
                        var temp = campaignObj.CampaignTimer.Split(' ');
                        this.datetime.Text = temp[0];
                        var temp1 = temp[1].Split(':');
                        textBox1.Text = temp1[0];
                        textBox2.Text = temp1[1];
                        textBox3.Text = temp1[2];
                        comboBox.Text = temp[2];
                        EnableDisableDateTime(true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Collections.IList listItem = listView.SelectedItems;
                var res = listItem.Cast<string>().ToList();
                    foreach (var item in res)
                    {
                        this.Numbers.Remove(item.ToString());
                        this.AddedNumbers.Add(item.ToString());
                    } 
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Collections.IList listItem = listView1.SelectedItems;
                var res = listItem.Cast<string>().ToList();
                foreach (var item in res)
                {
                    AddedNumbers.Remove(item.ToString());
                    Numbers.Add(item);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button5_Click_1(object sender, RoutedEventArgs e)
        {
            System.Collections.IList listItem = listView5.SelectedItems;
            var res = listItem.Cast<string>().ToList();
            foreach (var item in res)
            {
                this.AddedFiles.Remove(item);
                this.Files.Add(item);
            }
        }

        private void Button6_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                int campaignId = Convert.ToInt32(Id.Text);
                string messages = string.Empty, numbers = string.Empty, urls = string.Empty;
                int count = 0;
                foreach (var item in listView3.Items)
                {
                    
                    messages += (count<listView3.Items.Count-1)?item.ToString() + ",": item.ToString();
                    count++;
                }
                count = 0;
                foreach (var item in listView1.Items)
                {
                    numbers += (count < listView1.Items.Count-1)?item.ToString() + ",":item.ToString();
                    count++;
                }
                count = 0;
                foreach (var item in listView5.Items)
                {
                    urls += (count < listView5.Items.Count-1)?item.ToString() + ",":item.ToString();
                    count++;
                }
                string campaignName = textBox.Text;
                string fullDateTime = "";
                if (!checkBox.IsChecked.GetValueOrDefault())
                {
                    fullDateTime = this.datetime.ToString() + " " + textBox1.Text + ":" + textBox2.Text + ":" + textBox3.Text + " " + comboBox.Text;
                    string[] temp = fullDateTime.Split(' ');
                    fullDateTime = temp[0] + " " + temp[3] + " " + temp[4];
                }
                bool immediate = checkBox.IsChecked.GetValueOrDefault();
                var campaign = new SqliteCampaigns()
                {
                    Name = textBox.Text,
                    Messages = messages,
                    Numbers = numbers,
                    Urls = urls,
                    Enable = 1,
                    CreatedDate = DateTime.Now.ToString(),
                    CampaignTimer= fullDateTime,
                    Immediate= immediate?1:0
                };
                if (campaignId >0)
                {
                    uow.Campaign.Update(campaignId, campaign);
                }
                else
                {
                    uow.Campaign.Add(campaign);
                }
                new CampaignWin().Show();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EnableDisableDateTime(bool check)
        {
            datetime.IsEnabled = check;
            textBox1.IsEnabled = check;
            textBox2.IsEnabled = check;
            textBox3.IsEnabled = check;
            comboBox.IsEnabled = check;
        }

        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            //if (checkBox.IsChecked.GetValueOrDefault())
            //{
            //    EnableDisableDateTime(false);
            //}
            //else
            //{
            //    EnableDisableDateTime(true);
            //}
        }

        private void checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (checkBox.IsChecked.GetValueOrDefault())
            {
                EnableDisableDateTime(false);
            }
            else
            {
                EnableDisableDateTime(true);
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
            {
            try
            {
                System.Collections.IList listItem = listView4.SelectedItems;
                var res = listItem.Cast<string>().ToList();

                foreach (var item in res)
                {
                    this.Files.Remove(item);
                    this.AddedFiles.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Collections.IList listItem = listView2.SelectedItems;
                var res = listItem.Cast<string>().ToList();
                foreach (var item in res)
                {
                    this.Messages.Remove(item);
                    this.AddedMessages.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Collections.IList listItem = listView3.SelectedItems;
                var res = listItem.Cast<string>().ToList();
                foreach(var item in res)
                {
                    AddedMessages.Remove(item);
                    Messages.Add(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        


        


    }
}
