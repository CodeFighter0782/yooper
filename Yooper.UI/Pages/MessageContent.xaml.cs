﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for MessageContent.xaml
    /// </summary>
    public partial class MessageContent : Page
    {
        IUnitofWork _uow;
        public MessageContent()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            this.LoadGrid();
        }

        private void LoadGrid()
        {
            try
            {
                var message = _uow.Account.GetMessages().AsEnumerable().Select(x => new MessagesVM()
                {
                    Id = x.Id,
                    Text = x.Text,
                    CreatedDate = x.CreatedDate
                });
                message_grid.ItemsSource = message.ToList();

                attachment_Grid.ItemsSource = _uow.Account.GetMultimediaMessages().AsEnumerable().Select(x => new MessagesVM()
                {
                    Id = x.Id,
                    Text = x.Text,
                    CreatedDate = x.CreatedDate
                }).ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void Add_number_btn_Click(object sender, RoutedEventArgs e)
        {
            this.Reset();
            new AddMessageWin().ShowDialog();
        }

        private void Individual_btn_Click(object sender, RoutedEventArgs e)
        {
            SessionItem.item.Add("message", send_message_txtblck.Text.ToString());
            this.Reset();
            new IndividualMessageWin().ShowDialog();
        }

        private void Group_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SessionItem.item.Add("message", send_message_txtblck.Text.ToString());
                SessionItem.item.Add("messageId", message_id_txtblck.Text);
                this.Reset();
                new GroupMessageWin().ShowDialog();
            }
            catch (Exception ex)
            {
            }
        }

        private void Refresh_btn_Click(object sender, RoutedEventArgs e)
        {
            message_id_txtblck.Text = "";
            this.Reset();
            this.LoadGrid();
        }

        private void Reset()
        {
            try
            {
                message_grid.UnselectAll();
                send_message_txtblck.Text = "";
                message_id_txtblck.Text = "";
                individual_btn.IsEnabled = false;
                group_btn.IsEnabled = false;
                delete_message_btn.IsEnabled = false;
                button2.IsEnabled = false;
                button3.IsEnabled = false;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message, "Exception");
            }
        }

        private void Message_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var obj = message_grid.SelectedItem as MessagesVM;
            if (obj != null)
            {
                send_message_txtblck.Text = obj.Text;
                message_id_txtblck.Text = obj.Id.ToString();
                individual_btn.IsEnabled = true;
                group_btn.IsEnabled = true;
                button2.IsEnabled = true;
                button3.IsEnabled = true;
                delete_message_btn.IsEnabled = true;
                SessionItem.item.Remove("IsText");
                SessionItem.item.Add("IsText", true);
            }
        }

        private void Delete_message_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _uow.Account.DeleteMessage(Convert.ToInt64(message_id_txtblck.Text.ToString()));
                this.LoadGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: "+ex.Message,"Runtime Exception");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new UploadFileWin().ShowDialog();
        }

        private void Attachment_Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var obj = attachment_Grid.SelectedItem as MessagesVM;
            if (obj != null)
            {
                send_message_txtblck.Text = obj.Text;
                message_id_txtblck.Text = obj.Id.ToString();
                individual_btn.IsEnabled = true;
                group_btn.IsEnabled = true;
                delete_message_btn.IsEnabled = true;
                button2.IsEnabled = false;
                send_message_txtblck.IsEnabled = false;
                button3.IsEnabled = true;
                SessionItem.item.Remove("IsText");
                SessionItem.item.Add("IsText", false);
            }
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            new ManageAutoReply().ShowDialog();
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _uow.Account.UpdateMessage(Convert.ToInt32(message_id_txtblck.Text), send_message_txtblck.Text);
                this.LoadGrid();
                Reset();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            SessionItem.item.Add("message", send_message_txtblck.Text.ToString());
            this.Reset();
            new SendAllWin().ShowDialog();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            new CampaignWin().ShowDialog();
        }
    }
}
