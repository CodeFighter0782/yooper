﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Ninject;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for Group.xaml
    /// </summary>
    public partial class Group : Page
    {
        private IUnitofWork _uow;
        private List<GroupVM> _groups = new List<GroupVM>();
        public Group()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            ResetAll();
            UpdateDataGrid();
        }
        

        private void UpdateDataGrid()
        {
            _groups.Clear();
            _groups = _uow.Account.GetGroups().Select(s => new GroupVM()
            {
                ID = s.Id,
                Id = s.Id,
                Name = s.GroupName,
                CreateDate = s.CreatedDate.ToString()
            }).ToList();
            int count = 1;
            foreach (var item in _groups)
            {
                item.ID = count;
                count++;
            }
            group_data_grid.ItemsSource = _groups;
        }
        

        private void Group_search_txtBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            this._groups= _uow.Account.GetGroups(group_search_txtbx.Text).Select(s => new GroupVM()
            {
                ID = s.Id,
                Name = s.GroupName,
                CreateDate = s.CreatedDate.ToString()
            }).ToList();

            group_data_grid.ItemsSource = this._groups; 
            //if (_groups.Any(a => a.Name.Contains(group_search_txtbx.Text.ToString())))
            //{
            //    var searchGroup = _groups.Where(w => w.Name.Contains(group_search_txtbx.Text.ToString()));

            //    group_data_grid.ItemsSource = searchGroup.ToList();
            //}
            //else
            //{
            //    group_data_grid.ItemsSource = null;
            //}
        }


        private void Create_group_btn_Click(object sender, RoutedEventArgs e)
        {
            string groupName = group_name_txtbox.Text.ToString();

            if (string.IsNullOrEmpty(groupName))
            {
                MessageBox.Show("Enter group name");
            }
            else
            {
                bool isGroup = _uow.Account.IsGroupAvailable(groupName);

                if (isGroup)
                {
                    ResetAll();
                    MessageBox.Show("Group is already exist");

                }
                else
                {
                    _uow.Account.CreateGroup(groupName);

                    GroupVM group = _uow.Account.GetGroupByName(groupName);


                    SessionItem.item.Add("group", group);
                    UpdateDataGrid();
                    ResetAll();

                    new GroupWin().ShowDialog();
                }
            }
        }


        private void Update_group_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string groupName = group_name_txtbox.Text;

                GroupVM group = _uow.Account.GetGroupById(Convert.ToInt32(textBox.Text));
                if (group==null)
                {
                    throw new Exception("Group not found. ");
                }
                SessionItem.item.Add("group", group);

                ResetAll();

                new GroupWin().ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Exception");
            }
        }


        private void Delete_group_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string groupName = group_name_txtbox.Text.ToString();
                var group = _uow.Account.GetGroupByName(groupName);
                List<NumberVM> groupNumbers = new List<NumberVM>();

                groupNumbers = _uow.Account.GetGroupNumbers(group.ID);

                foreach (var item in groupNumbers)
                {
                    _uow.Account.RemoveNumberFromGroup(group.ID, item.Name, item.Number);
                }

                _uow.Account.DeleteGroup(group.ID);

                ResetAll();

                UpdateDataGrid();
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }


        private void Group_data_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (group_data_grid.SelectedItem != null)
            {
                var obj = (GroupVM)group_data_grid.SelectedItem;

                if (obj != null)
                {
                    textBox.Text = obj.ID.ToString();
                    group_name_txtbox.Text = obj.Name;
                    EnableAndDisableGroupButtons(false, true, true,true);
                    button1.IsEnabled = true;
                    button2.IsEnabled = true;
                }
            }
        }


        private void ResetAll()
        {
            EnableAndDisableGroupButtons(true, false, false,false);
            group_data_grid.UnselectAll();
            textBox.Text = "";
            group_search_txtbx.Text = "";
            group_name_txtbox.Text = "";
            button1.Content = "Create WhatsApp Group";
            button1.IsEnabled = false;
            button2.IsEnabled = false;
        }
       

        private void EnableAndDisableGroupButtons(bool createBtn, bool updateBtn, bool deleteBtn, bool AddRemoveNumber)
        {
            create_group_btn.IsEnabled = createBtn;
            update_group_btn.IsEnabled = updateBtn;
            delete_group_btn.IsEnabled = deleteBtn;
            button.IsEnabled = AddRemoveNumber;
        }

        private void Group_refresh_list_btn_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
            UpdateDataGrid();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string val = group_name_txtbox.Text;
                if (string.IsNullOrEmpty(val))
                {
                    MessageBox.Show("Group name can not be empty", "Error");
                }
                else
                {
                    _uow.Account.UpdateGroupName(Convert.ToInt32(textBox.Text), group_name_txtbox.Text);
                    this.UpdateDataGrid();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show("Error: " + ex.Message, "Exception");
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                button1.Content = "Creating....";
                button1.IsEnabled = false;
                var group = group_data_grid.SelectedItem as GroupVM;
                var actualGroup = _uow.Group.Get(group.Id).FirstOrDefault();
                List<string> numbers = new List<string>();
                if (!string.IsNullOrEmpty(actualGroup.WhatsAppGroupId))
                {
                    MessageBox.Show("WhatsApp Group is already Created.", "Error");
                    ResetAll();
                    return;
                }
                if (actualGroup!=null)
                {
                    _uow.Account.GetGroupNumbers(Convert.ToInt64(actualGroup.Id)).ForEach(x => numbers.Add(x.Number));
                }
                else
                {
                    MessageBox.Show("Please select a group first.");
                    return;
                }
                var thread = new Thread(() =>
                {
                    string groupId = string.Empty;
                    foreach (var item in numbers)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(groupId))
                            {
                                while (true)
                                {
                                    if (!SessionItem.InProcess)
                                    {
                                        SessionItem.InProcess = true;
                                        SessionItem._whatsapp.AddNewGroup(actualGroup.GroupName, item, "", out groupId);
                                        _uow.Account.UpdateWhatsAppGroupId(actualGroup.Id, groupId);
                                        SessionItem.InProcess = false;
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                while (true)
                                {
                                    if (!SessionItem.InProcess)
                                    {
                                        SessionItem.InProcess = true;
                                        SessionItem._whatsapp.AddNumberInNewGroup(item, groupId.ToString());
                                        SessionItem.InProcess = false;
                                        break;
                                    }

                                }

                            }


                        }
                        catch (Exception ex)
                        {
                            SessionItem.InProcess = false;
                            Dispatcher.Invoke(() =>
                            {
                                MessageBox.Show("Number is not found in contact list/ whatsapp will only add number of contact list");
                            });
                        }
                    }
                    Dispatcher.Invoke(() =>
                    {
                        ResetAll();
                    });
                })
                { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var group = group_data_grid.SelectedItem as GroupVM;
                SessionItem.item.Remove("groupId");
                SessionItem.item.Add("groupId", group.Id);
                //Hassam
                //new RemoveWhatsAppNumberWin().ShowDialog();
            }
            catch (Exception)
            {
                
            }
        }
    }
}
