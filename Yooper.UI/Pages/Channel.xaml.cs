﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Whatsapp;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for Channel.xaml
    /// </summary>
    public partial class Channel : Page
    {
        private IUnitofWork _uow;
        public Channel()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            if (_uow.Account.CheckChannelStatus())
            {
                open_whatsapp_btn.Content = "Connected";
                open_whatsapp_btn.IsEnabled = false;
                button.IsEnabled = true;
            }
            else
            {
                open_whatsapp_btn.IsEnabled = true;
                open_whatsapp_btn.Content = "Connect WhatsApp";
                button.IsEnabled = false;
            }

        }

        private void AutoReply()
        {

            while (true)
            {
                try
                {
                    var auto_reply_message = _uow.Account.GetAutoReplyMessages().Where(x => x.SetAsReply == 1).FirstOrDefault();
                    if (auto_reply_message != null)
                    {
                        while (true)
                        {
                            if (!SessionItem.InProcess)
                            {
                                SessionItem.InProcess = true;
                                SessionItem._whatsapp.AutoReply(auto_reply_message.Message);
                                SessionItem.InProcess = false;
                                break;
                            }
                        }
                    }
                    Thread.Sleep(5000);
                }
                catch
                {
                }
            }

        }

        private void Open_whatsapp_btn_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Team is working on this feature.");
            open_whatsapp_btn.Content = "Connecting....";
            open_whatsapp_btn.IsEnabled = false;
            var thread = new Thread(x =>
            {
                try
                {
                    while (true)
                    {
                        if (!SessionItem.InProcess)
                        {
                            SessionItem.InProcess = true;
                            SessionItem._whatsapp.run();
                            SessionItem.InProcess = false;
                            break;
                        }
                        
                    }
                    this.Dispatcher.Invoke(() =>
                    {
                    //MessageBox.Show("Connection build successfully.");
                    _uow.Account.MakeChangeInChannelStatus(true);
                        open_whatsapp_btn.Content = "Connected";
                        open_whatsapp_btn.IsEnabled = false;
                        button1.IsEnabled = true;
                        button2.IsEnabled = true;
                        button.IsEnabled = true;
                        var auto_reply_thread = new Thread(() =>
                         {
                             this.AutoReply();
                         })
                        { IsBackground = true };
                        auto_reply_thread.SetApartmentState(ApartmentState.STA);
                        auto_reply_thread.Start();
                    });
                }
                catch (Exception ex)
                {
                    SessionItem.InProcess = false;
                    this.Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show(ex.Message, "Error");
                    });
                }
            })
            { IsBackground = true };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_uow.Account.CheckChannelStatus())
            {
                SessionItem._whatsapp.EndSession();
            }
            open_whatsapp_btn.IsEnabled = true;
            open_whatsapp_btn.Content = "Connect WhatsApp";
            button.IsEnabled = false;
            _uow.Account.MakeChangeInChannelStatus(false);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SessionItem._whatsapp.Maximize();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SessionItem._whatsapp.Minimize();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
