﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for GroupMessageWin.xaml
    /// </summary>
    public partial class GroupMessageWin : Window
    {
        private  ObservableCollection<NumberStatusVM> changeset = new ObservableCollection<NumberStatusVM>();

        private string message;
        private int messageId;
        List<GroupVM> groups;
        IUnitofWork _uow;
        public GroupMessageWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            textBlock3.Text = SessionItem.item["message"].ToString();
            SessionItem.item.Remove("message");
            this.LoadDataGrid();
            dataGrid1.ItemsSource = this.changeset;

            var settingObj = _uow.Setting.Get().FirstOrDefault();
            settingObj.Pause = 0;settingObj.Stop = 0;
            _uow.Setting.Update(settingObj.Id, settingObj);
            messageId = Convert.ToInt32(SessionItem.item["messageId"].ToString());
            SessionItem.item.Remove("messageId");
        }

        private void LoadDataGrid()
        {
            try
            {
                this.groups = _uow.Account.GetGroups().AsEnumerable().Select(x => new GroupVM()
                {
                    ID = x.Id,
                    Name = x.GroupName,
                    CreateDate = x.CreatedDate.ToString()
                }).ToList();
                dataGrid.ItemsSource = this.groups;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void UpdateMessageSent()
        {
            dataGrid1.ItemsSource = changeset;
        }

        private void DataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var obj = dataGrid.SelectedItem as GroupVM;

            if (obj!=null)
            {
                textBlock.Text = obj.ID.ToString();
                textBlock1.Text = obj.Name;
                var numbers = _uow.Account.GetGroupNumbers(obj.ID);
                var temp = string.Empty;
                int count = 0;
                foreach (var item in numbers)
                {
                    if (count<numbers.Count)
                    {
                        temp += item.Number + ","; 
                    }
                    count++;
                }
                textBlock2.Text = temp;
                button.IsEnabled = true;
                button1.IsEnabled = true;
                btn_OneToOne.IsEnabled = true;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.groups= _uow.Account.GetGroups(textBox.Text).AsEnumerable().Select(x => new GroupVM()
            {
                ID = x.Id,
                Name = x.GroupName,
                CreateDate = x.CreatedDate.ToString()
            }).ToList();
            dataGrid.ItemsSource = this.groups; ;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!_uow.Account.CheckChannelStatus())
            {
                MessageBox.Show("Please connect to WhatsApp through channel.");
            }
            else
            {
                button.Content = "SENDING..."; button.IsEnabled = false;
                var thread = new Thread(() => { this.SendGroupMessage(); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            
        }

        private void AddStatus(string number, string Status="Successful")
        {
            this.Dispatcher.Invoke(() => { this.changeset.Add(new NumberStatusVM { Number = number, Status = Status,Time=DateTime.Now.ToString() }); });
            
        }
        private void SendGroupMessage()
        {
            try
            {
                string[] numbers = null;
                string message = string.Empty;
                this.Dispatcher.Invoke(() =>
                {
                    numbers = textBlock2.Text.Split(',');
                    message = textBlock3.Text;
                });

                if (numbers!=null)
                {
                    double totalRecord = numbers.Length, count = 1, percentage= 0 ;
                    foreach (var item in numbers)
                    {
                        while (_uow.Setting.Get().FirstOrDefault().Pause == 1)
                        {

                        }
                        if (_uow.Setting.Get().FirstOrDefault().Stop == 1)
                        {
                            break;
                        }
                        if (!string.IsNullOrEmpty(item))
                        {
                            var numberObj = _uow.Account.GetNumbers(item).FirstOrDefault();
                            if (Convert.ToBoolean(SessionItem.item["IsText"]))
                            {
                                try
                                {
                                    SessionItem._whatsapp.SendMessage(item, message);
                                    this.AddStatus(item);
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, true);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits {NumberId=numberObj.Id, IsCompleted=1, Enable=1, CreatedDate= DateTime.Now.ToString(), CreatedBy=string.Empty, UpdatedBy=string.Empty, UpdatedDate= string.Empty });
                                        _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                        {
                                            Message = message,
                                            Number = item,
                                            MessageType = EMessageType.GROUP.ToString()
                                        });

                                    }
                                }
                                catch
                                {
                                    this.AddStatus(item, "Number is invalid");
                                    if (numberObj!=null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, false);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                    }
                                }
                            }
                            else
                            {
                                try
                                {
                                    SessionItem._whatsapp.SendMessage(item, message, true);
                                    this.AddStatus(item);
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, true);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 1, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                        _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                        {
                                            Message = message,
                                            Number = item,
                                            MessageType = EMessageType.GROUP.ToString()
                                        });
                                    }
                                }
                                catch
                                {
                                    this.AddStatus(item, "Number is invalid");
                                    if (numberObj != null)
                                    {
                                        _uow.Account.SetNumberValidity(numberObj.Id, false);
                                        _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
                                    }
                                }
                            }
                            
                        }
                        var _thread = new Thread(() => { this.Timer(SessionItem.Delay); }) { IsBackground = true };
                        _thread.SetApartmentState(ApartmentState.STA);
                        _thread.Start();
                        Thread.Sleep(1000*SessionItem.Delay);
                        percentage = (count / totalRecord) * 100;
                        this.Dispatcher.Invoke(() =>
                        {
                            label4.Content = percentage.ToString("0.00") + "% Completed";
                        });
                        count++;
                    } 
                }
                this.Dispatcher.Invoke(() =>
                {
                    this.Clear();
                    button.IsEnabled = false;
                    button.Content = "SEND";
                });
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void Timer(int seconds)
        {
            for (int i = seconds; i > 0; i--)
            {
                this.Dispatcher.Invoke(() => {
                    label6.Content = "Time Left: "+i+ "second";
                });
                Thread.Sleep(1000);
            }
        }




        private void Clear()
        {
            textBlock.Text = "";
            textBlock1.Text = "";
            textBlock2.Text = "";

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            button1.Content = "Sending, Please wait....";
            int groupId = Convert.ToInt32(textBlock.Text);
            string message = textBlock3.Text;
            int messageId = this.messageId;

            var messageObj = _uow.Account.GetCurrentMessage().Where(x=>x.Id == messageId).FirstOrDefault();
            var whatsAppGroupId = _uow.Account.GetGroupWhatsAppId(groupId);

            var thread = new Thread(() => {
                if (messageObj.IsText == 1)
                {
                    SessionItem._whatsapp.sendMessageToWhatsAppGroup(whatsAppGroupId, message);
                }
                else
                {
                    SessionItem._whatsapp.sendMessageToWhatsAppGroup(whatsAppGroupId, message,true);
                }
                
                Dispatcher.Invoke(() =>
                {
                    button1.Content = "SEND TO WHATSAPP GROUP";
                    MessageBox.Show("Sent Successfully!", "Success");
                });
            }) { IsBackground = true };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

            
        }

        private void Btn_OneToOne_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btn_OneToOne.Content = "Sending .....";
                string[] numbers = textBlock2.Text.ToString().Split(',');
                string groupName = textBlock1.Text;
                string whatsAppGroupId = string.Empty;
                string message = textBlock3.Text;
                var thread = new Thread(() => {
                    //Logic will go here!!! 
                    if (numbers.Length>0)
                    {
                        foreach (var item in numbers)
                        {
                            try
                            {
                                if (string.IsNullOrEmpty(whatsAppGroupId))
                                {
                                    SessionItem._whatsapp.OneToOneGroup(groupName, item,message, out whatsAppGroupId);

                                }
                                else
                                {
                                    SessionItem._whatsapp.OneToOneGroup(groupName, item, message, out whatsAppGroupId, false);
                                }
                            }
                            catch
                            {
                            }
                        }

                        Dispatcher.Invoke(() =>
                        {
                            btn_OneToOne.Content = "SEND ONE TO ONE GROUP MESSAGE";
                            MessageBox.Show("sent successfully");
                            Clear();
                        });
                        
                    }
                }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
