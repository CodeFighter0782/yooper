﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ninject.Infrastructure.Language;
using Yooper.DTO;
using Yooper.UOW;
using System.Threading;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for GroupWin.xaml
    /// </summary>
    public partial class GroupWin : Window
    {
        private IUnitofWork _uow;
        private GroupVM _group = new GroupVM();
        private NumberVM _number = new NumberVM();
        public static List<NumberVM> numbersAdded = new List<NumberVM>();
        public static List<NumberVM> numbersToBeAdd = new List<NumberVM>();
        public static List<NumberVM> allNumbers = new List<NumberVM>();

        
        public GroupWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();

            @_group = SessionItem.item["group"] as GroupVM;
            SessionItem.item.Remove("group");

            ResetAll();
            UpdateDataGrid();
        }


        private List<NumberVM> UpdateAddedNumberDataGrid()
        {

            try
            {
                return _uow.Account.GetGroupNumbers(_group.ID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private List<NumberVM> UpdateAddNewNumberDataGrid()
        {
            numbersAdded = UpdateAddedNumberDataGrid();
            numbersToBeAdd.Clear();

            allNumbers = _uow.Account.GetNumbers().AsEnumerable().Select(s => new NumberVM()
            {
                ID = s.Id,
                Name = s.Name,
                Number = s.PhoneNumber,
                CreatedDate = s.CreatedDate.ToString()
            }).ToList();
            var temp = allNumbers;

            // this logic for get the number to be added
            foreach (var item in numbersAdded)
            {
                var obj = temp.Where(x => x.Number == item.Number);
                int index = temp.IndexOf(obj.FirstOrDefault());
                if (obj.Any())
                {
                    
                    temp.RemoveAt(index);
                }
            }

            //for (int x = 0; x < allNumbers.Count; x++)
            //{
            //    for (int y = 0; y < numbersAdded.Count; y++)
            //    {
            //        if (numbersAdded[y].Number.Equals(allNumbers[x].Number))
            //        {
                        
            //        }
            //        else
            //        {
            //            numbersToBeAdd.Add(allNumbers[x]);
            //        }
            //    }
            //}

            if (numbersToBeAdd.Count == 0 && numbersAdded.Count == 0)
            {
                return allNumbers;
            }
            else
            {
                return temp;
            }
        }


        private void UpdateDataGrid()
        {
            allNumbers.Clear();
            numbersAdded.Clear();
            numbersToBeAdd.Clear();
            var added = UpdateAddedNumberDataGrid();
            var addNew = UpdateAddNewNumberDataGrid();
            numbersAdded= UpdateAddedNumberDataGrid();
            numbersToBeAdd= UpdateAddNewNumberDataGrid();
            added_number_group_data_grid.ItemsSource = added.ToList();
            add_new_number_group_data_grid.ItemsSource = addNew.ToList();
        }


        private void ResetAll()
        {
            selected_item_number_group_txtBox.Text = string.Empty;
            selected_item_name_group_txtBox.Text = string.Empty;

            added_number_search_group_txtbox.Text = string.Empty;
            add_new_number_search_group_txtBox.Text = string.Empty;

            EnableAndDisableButton(false, false);
        }

        private void Added_number_search_group_txtbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (numbersAdded.Count()>0)
            {
                var temp= numbersAdded.Where(x => x.Name.ToLower().Contains(added_number_search_group_txtbox.Text.ToLower()) || x.Number.ToLower().Contains(added_number_search_group_txtbox.Text.ToLower())).ToList(); ;
                
                added_number_group_data_grid.ItemsSource = temp;
            }


            //if (numbersAdded.Any(a => a.Name.Contains(added_number_search_group_txtbox.Text.ToString())))
            //{
            //    var searchAddedNumberByName = numbersAdded
            //        .Where(w => w.Name.Contains(added_number_search_group_txtbox.Text.ToString()));
                
            //    added_number_group_data_grid.ItemsSource = searchAddedNumberByName;
            //}
            //else if (numbersAdded.Any(a => a.Number.Contains(added_number_search_group_txtbox.Text.ToString())))
            //{
            //    var searchAddedNumberByNumber = numbersAdded.Where(w =>
            //        w.Number.Contains(added_number_search_group_txtbox.Text.ToString()));

            //    added_number_group_data_grid.ItemsSource = searchAddedNumberByNumber;
            //}
            //else
            //{
            //    added_number_group_data_grid.ItemsSource = null;
            //}
        }


        private void Add_new_number_search_group_txtBox_TextChanged(object sender, TextChangedEventArgs e)
       {
            string val = add_new_number_search_group_txtBox.Text;
            if (numbersToBeAdd.Where(x=>x.Name.Contains(val)).Any())
            {
                var searchAddNewNumberByName = numbersToBeAdd.Where(w =>
                    w.Name.Contains(val)).ToList();

                add_new_number_group_data_grid.ItemsSource = searchAddNewNumberByName;
            } else if (numbersToBeAdd.Where(x => x.Number.Contains(val)).Any())
            {
                var searchAddNewNumberByNumber = numbersToBeAdd.Where(w =>
                    w.Number.Contains(val)).ToList();

                add_new_number_group_data_grid.ItemsSource = searchAddNewNumberByNumber;
            }
            else
            {
                add_new_number_group_data_grid.ItemsSource = null;
            }
        }


        private void EnableAndDisableButton(bool addBtn, bool removeBtn)
        {
            add_number_group_btn.IsEnabled = addBtn;
            remove_number_group_btn.IsEnabled = removeBtn;
            
        }


        private void Added_number_group_data_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _number = (NumberVM) added_number_group_data_grid.SelectedItem;

            if (_number == null) return;
            label.Content = _number.ID;
            selected_item_name_group_txtBox.Text = _number.Name;
            selected_item_number_group_txtBox.Text = _number.Number;

            EnableAndDisableButton(false, true);
        }


        private void Add_new_number_group_data_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _number = (NumberVM) add_new_number_group_data_grid.SelectedItem;

                if (_number == null) return;
                selected_item_name_group_txtBox.Text = _number.Name;
                selected_item_number_group_txtBox.Text = _number.Number;

                EnableAndDisableButton(true, false);
        }


        private void Add_number_group_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Collections.IList temp = (System.Collections.IList)add_new_number_group_data_grid.SelectedItems;
                var _numbers = temp.Cast<NumberVM>();
                if (!_uow.Account.CheckChannelStatus())
                {
                    MessageBox.Show("Please connect to WhatsApp through channel.");
                    return;
                }

                if (_numbers !=null)
                {
                    if (_numbers.Count() > 1)
                    {
                        foreach (var item in _numbers)
                        {
                            _uow.Account.AddNumberInGroup(_group.ID, item.ID);
                            var group = _uow.Account.GetGroupWhatsAppId(_group.ID);

                            string groupId = string.IsNullOrEmpty(group) ? "" : group;
                        }
                    }
                    else
                    {
                        _uow.Account.AddNumberInGroup(_group.ID, _number.ID);
                        var group = _uow.Account.GetGroupWhatsAppId(_group.ID);

                        string groupId = string.IsNullOrEmpty(group) ? "" : group;
                    }
                    UpdateDataGrid();
                    ResetAll();
                }
                else
                {
                    MessageBox.Show("Something went wrong.");
                }
            }
            catch (Exception ex)
            {
                SessionItem.InProcess = false;
                MessageBox.Show(ex.Message, "Error");

            }
        }

        private void AddAllNumbersToWhatsApp()
        {
            try
            {
                var numbers= _uow.Account.GetGroupNumbers(_group.ID);

                var thread = new Thread(() =>
                {
                    foreach (var item in numbers)
                    {
                        var group = _uow.Account.GetGroupWhatsAppId(_group.ID);
                        string groupId = string.Empty;
                        try
                        {
                            if (string.IsNullOrEmpty(group))
                            {
                                while (true)
                                {
                                    if (!SessionItem.InProcess)
                                    {
                                        SessionItem.InProcess = true;
                                        SessionItem._whatsapp.AddNewGroup(_group.Name, item.Number, groupId, out groupId);
                                        _uow.Account.UpdateWhatsAppGroupId(_group.ID, groupId);

                                        SessionItem.InProcess = false;
                                        break;
                                    }

                                }
                            }
                            else
                            {
                                while (true)
                                {
                                    if (!SessionItem.InProcess)
                                    {
                                        SessionItem.InProcess = true;
                                        SessionItem._whatsapp.AddNumberInNewGroup(item.Number, group);
                                        SessionItem.InProcess = false;
                                        break;
                                    }

                                }

                            }


                        }
                        catch (Exception ex)
                        {
                            SessionItem.InProcess = false;
                            Dispatcher.Invoke(() =>
                            {
                                MessageBox.Show("Number is not found in contact list/ whatsapp will only add number of contact list");
                            });
                        }
                    }
                    Dispatcher.Invoke(() =>
                    {
                        //button.Content = "Add to WhatsApp";
                    });
                })
                { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void Remove_number_group_btn_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                var temp = (System.Collections.IList)added_number_group_data_grid.SelectedItems;
                var number = temp.Cast<NumberVM>();
                remove_number_group_btn.Content = "Wait Removing...";
                remove_number_group_btn.IsEnabled = false;
                if (number.Count() > 0)
                {
                    foreach (var item in number)
                    {
                        _uow.Account.RemoveNumberFromGroup(Convert.ToInt64(label.Content.ToString()), item.Name, item.Number);
                    }
                }
                else
                {
                    _uow.Account.RemoveNumberFromGroup(Convert.ToInt64(label.Content.ToString()), _number.Name, _number.Number);
                }
                //removeFromWhatsAppGroup(number);
                UpdateDataGrid();
                ResetAll();
            }
        }

        private void removeFromWhatsAppGroup(IEnumerable<NumberVM> numbers)
        {
            var thread = new Thread(() =>
            {
                try
                {
                    foreach (var item in numbers)
                    {
                        var group = _uow.Account.GetGroupWhatsAppId(item.ID);
                        while (true)
                        {
                            if (!SessionItem.InProcess)
                            {
                                SessionItem.InProcess = true;
                                SessionItem._whatsapp.RemoveNumberFromGroup(item.Number, group);
                                //_uow.Account.RemoveNumberFromGroup(Convert.ToInt64(label.Content.ToString()), _number.Name, _number.Number);
                                //UpdateDataGrid();
                                SessionItem.InProcess = false;
                                break;
                            }
                        }
                    }
                    remove_number_group_btn.Content = "Remove";
                    remove_number_group_btn.IsEnabled = true;
                }
                catch (Exception ex)
                {
                    SessionItem.InProcess = false;
                    Dispatcher.Invoke(() => { MessageBox.Show(ex.Message, "Error"); });
                }
            })
            { IsBackground = true };
            thread.ApartmentState = ApartmentState.STA;
            thread.Start();
            
        }


        private void Reset_btn_Click(object sender, RoutedEventArgs e)
        {
            ResetAll();
            UpdateDataGrid();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //button.Content = "Creating...";
                var thread = new Thread(() =>
                {
                    AddAllNumbersToWhatsApp();
                })
                { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }



        //private void updateAddedNumberDataGrid()
        //{
        //   var group = SessionItem.item["group"] as GroupVM;
        //    SessionItem.item.Remove("group");

        //    this.number_added = _uow.Account.GetGroupNumbers(group.ID).AsEnumerable().Select(s => new NumberVM()
        //    {
        //        ID = s.Number.Id,
        //        Name = s.Number.Name,
        //        Number = s.Number.PhoneNumber,
        //        CreatedDate = s.Number.CreatedDate.ToString()
        //    }).ToList();
        //    added_number_group_data_grid.ItemsSource = this.number_added;
        //}

        //private void updateAddNewNumber()
        //{
        //    ;

        //    this.allNumbers = _uow.Account.GetNumbers().AsEnumerable().Select(s => new NumberVM() {
        //        ID = s.Id,
        //        Name = s.Name,
        //        Number = s.PhoneNumber,
        //        CreatedDate = s.CreatedDate.ToString()
        //    }).ToList();

        //    this.numbersAdded = _uow.Account.GetGroupNumbers(_group.ID).AsEnumerable().Select(s => new NumberVM()
        //    {
        //        ID = s.Number.Id,
        //        Name = s.Number.Name,
        //        Number = s.Number.PhoneNumber,
        //        CreatedDate = s.Number.CreatedDate.ToString()
        //    }).ToList();

        //    for (int x = 0; x < allNumbers.Count; x++) {
        //        for (int y = 0; y < numbersAdded.Count; y++) {
        //            if (numbersAdded[y].Number.Equals(allNumbers[x].Number)) {
        //                break;
        //            }
        //            else if ((numbersAdded.Count - 1) == y) {
        //                numbersToBeAdd.Add(allNumbers[x]);
        //            }
        //        }
        //    }

        //    added_number_group_data_grid.ItemsSource = numbersAdded;

        //    add_new_number_group_data_grid.ItemsSource = numbersToBeAdd;

        //}


    }
}
