﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Ninject;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for CampaignWin.xaml
    /// </summary>
    public partial class CampaignWin : Window
    {
        private IUnitofWork uow;
        public CampaignWin()
        {
            InitializeComponent();
            uow = App.GlobalKernel.Get<IUnitofWork>();
            label.Content = "0 %";
            OnLoad();
        }


        private void OnLoad()
        {
            try
            {
                var campaign = uow.Campaign.Get();
                
                dataGrid.ItemsSource = campaign;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {



            SessionItem.item.Remove("campaignId");
            new AddCampaign().ShowDialog();
            this.Close();
        }

        private void btn_Update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var campaign = dataGrid.SelectedItem as SqliteCampaigns;
                SessionItem.item.Remove("campaignId");
                SessionItem.item.Add("campaignId", campaign.Id);
                new AddCampaign().ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("no data in campaign.","Error");
            }

        }

        private void btn_Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    var campaign = dataGrid.SelectedItem as SqliteCampaigns;
                    uow.Campaign.Delete(campaign.Id);
                    OnLoad();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PauseStopButton(bool Play, bool Pause, bool Stop)
        {
            button.IsEnabled = Play;
            button1.IsEnabled = Pause;
            button2.IsEnabled = Stop;
        }

        private void btn_StartCampaign_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!SessionItem._whatsapp.CheckLoggedIn())
                {
                    MessageBox.Show("Please connect your channel.");
                    return;
                }
                if (Helper.IsCampaignRunning)
                {
                    MessageBox.Show("Campaign is already running. Please wait for completion. Thank you.");
                    return;
                }
                PauseStopButton(false, true, false);
                label.Content = "0 %";
                Helper.IsCampaignRunning = true;
                var campaign = dataGrid.SelectedItem as SqliteCampaigns;
                DateTime timer = new DateTime();
                if (campaign.Immediate == 0)
                {
                    timer = Convert.ToDateTime(campaign.CampaignTimer);
                    if (DateTime.Now < timer)
                        {
                         MessageBox.Show("This campaign will initialize on " + timer.ToString());
                        }
                }
                string[] numbers = campaign.Numbers.Split(','),
                    messages = campaign.Messages.Split(','),
                    files = campaign.Urls.Split(',');
                
                int percentage = 1;
                var thread = new Thread(() => {
                    if (campaign.Immediate ==0)
                    {
                        while (true)
                        {
                            if (DateTime.Now>timer)
                            {
                                break;
                            }
                        }
                    }
                    foreach (var item in numbers)
                    {
                        try
                        {
                            while (Helper.PauseCampaign)
                            {

                            }
                            if (Helper.StopCampaign)
                            {
                                break;
                            }
                            SessionItem._whatsapp.SendCampaign(item, messages, files);
                        }
                        catch (Exception ex)
                        {
                            
                        }
                        Dispatcher.Invoke(() => {
                            label.Content = (percentage/numbers.Length)*100+ "%";
                            
                            percentage++;
                        });
                        
                    }
                    Dispatcher.Invoke(() => { PauseStopButton(false, false, false); });
                    Helper.IsCampaignRunning = false;
                }) { IsBackground = true };
                thread.Name = "campaignThread";
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                Helper.threads.Remove("campaignThread");
                Helper.threads.Add("campaignThread", thread);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Helper.PauseCampaign = false;
            Helper.StopCampaign = false;
            PauseStopButton(false, true, true);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Helper.PauseCampaign = true;
            Helper.StopCampaign = false;
            PauseStopButton(true, false, false);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Helper.PauseCampaign = false;
            Helper.StopCampaign = true;
            PauseStopButton(false, false, false);
        }

        private void btn_StopCampaign_Click(object sender, RoutedEventArgs e)
        {
            Helper.IsCampaignRunning = false;
            Thread t = Helper.threads["campaignThread"];
            t.Abort();
            MessageBox.Show("campaign stopped successfully.");
        }
    }
}
