﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.UI.Pages
{
    /// <summary>
    /// Interaction logic for IndividualMessageWin.xaml
    /// </summary>
    public partial class IndividualMessageWin : Window
    {
        IUnitofWork _uow;
        public IndividualMessageWin()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            message_txtbx.Text = SessionItem.item["message"].ToString();
            SessionItem.item.Remove("message");
            this.LoadNumbers();
        }
        private List<NumberVM> numberList;
        private void LoadNumbers()
        {
            numberList= _uow.Account.GetNumbers().AsEnumerable().Select(x => new NumberVM()
            {
                ID = x.Id,
                Name = x.Name,
                Number = x.PhoneNumber,
                CreatedDate = x.CreatedDate
            }).ToList();
            number_grid.ItemsSource = numberList;
        }

        private void Send_btn_Click(object sender, RoutedEventArgs e)
        {
            
            if (!_uow.Account.CheckChannelStatus())
            {
                MessageBox.Show("Please connect to WhatsApp through channel.");
            }
            else
            {
                send_btn.Content = "Sending...";
                send_btn.IsEnabled = false;
                var thread = new Thread(() =>
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        if (Convert.ToBoolean(SessionItem.item["IsText"]))
                        {
                            while (true)
                            {
                                if (!SessionItem.InProcess)
                                {
                                    SessionItem.InProcess = true;
                                    try
                                    {
                                        SessionItem._whatsapp.SendMessage(number_txtblck.Text, message_txtbx.Text);
                                        _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                        {
                                            Message = message_txtbx.Text ,
                                            Number = number_txtblck.Text,
                                            MessageType = EMessageType.INDIVIDUAL.ToString()
                                        });
                                        _uow.Account.SetNumberValidity(Convert.ToInt32(id_txtblck.Text), true);
                                    }
                                    catch
                                    {
                                        _uow.Account.SetNumberValidity(Convert.ToInt32(id_txtblck.Text), false);
                                    }
                                    SessionItem.InProcess = false;
                                    break;
                                } 
                            }
                        }
                        else
                        {
                            while (true)
                            {
                                if (!SessionItem.InProcess)
                                {
                                    SessionItem.InProcess = true;
                                    SessionItem._whatsapp.SendMessage(number_txtblck.Text, message_txtbx.Text, true);
                                    _uow.CampaignAudit.Add(new SqliteCampaignAudits()
                                    {
                                        Message = message_txtbx.Text,
                                        Number = number_txtblck.Text,
                                        MessageType = EMessageType.INDIVIDUAL.ToString()
                                    });
                                    SessionItem.InProcess = false;
                                    break;
                                }
                            }
                        }
                        MessageBox.Show("Message sent successfully");
                        send_btn.Content = "Send";
                        this.Clear();
                    });
                })
                { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
            }
        }

        private void Clear()
        {
            id_txtblck.Text = "";
            number_txtblck.Text = "";
            number_grid.UnselectAll();
        }

        

        private void Number_grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var obj = number_grid.SelectedItem as NumberVM;
                if (obj != null)
                {
                    number_txtblck.Text = obj.Number;
                    id_txtblck.Text = obj.ID.ToString() ;
                    send_btn.IsEnabled = true;
                }
            }
            catch
            {
            }
        }

        private void Search_txtbx_KeyUp(object sender, KeyEventArgs e)
       {
            this.numberList= _uow.Account.GetNumbers(search_txtbx.Text.ToString()).AsEnumerable().Select(x => new NumberVM()
            {
                ID = x.Id,
                Name = x.Name,
                Number = x.PhoneNumber,
                CreatedDate = x.CreatedDate
            }).ToList();
            number_grid.ItemsSource = this.numberList;
        }

        private void Search_txtbx_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.numberList = _uow.Account.GetNumbers(search_txtbx.Text.ToString()).AsEnumerable().Select(x => new NumberVM()
            {
                ID = x.Id,
                Name = x.Name,
                Number = x.PhoneNumber,
                CreatedDate = x.CreatedDate
            }).ToList();
            number_grid.ItemsSource = this.numberList;
        }

        private void Refresh_btn_Click(object sender, RoutedEventArgs e)
        {
            this.LoadNumbers();
        }
    }
}
