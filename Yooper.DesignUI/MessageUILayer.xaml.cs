﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YooperDesignUI.HelperClasses;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for MessageUILayer.xaml
    /// </summary>
    public partial class MessageUILayer : UserControl
    {
        public MessageUILayer()
        {
            InitializeComponent();
        }

        private List<string> numberList = new List<string>();

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var numbers = GenerateNumber.GenerateNumberList(Convert.ToInt64(number_textBox.Text), Convert.ToInt64(Range_textBox.Text));

            numberList = numbers;

            listView.ItemsSource = numbers;


        }
    }
}
