﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YooperDesignUI.HelperClasses;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for DashBoard.xaml
    /// </summary>
    public partial class DashBoard : Window
    {
        public DashBoard()
        {
            InitializeComponent();
            MainSection.Content = new dashboraduserWpf();
        }




        //Dashboard Button Start///

        //Click
        private void Dashboard_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
            MainSection.Content = new dashboraduserWpf();
        }
        //Dashboard Button End///


        //Channel Button Start///

        //Click
        private void Channel_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
            MainSection.Content = new Channel();
        }
        //Channel Button End///


        //Number Button Start///

        //Click
        private void Number_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
            MainSection.Content = new Number();
        }
        //Number Button End///


        //Message Button Start///

        //Click
        private void Messages_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
            MainSection.Content = new MessageThird();
        }
        //Message Button End///

        //Group Button Start///

        //Click
        private void Group_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
            //MainSection.Content = new GroupUI();
        }
        //Group Button End///


        //Settings Button Start///

        //Click
        private void Settings_nav_dashboard_button_Click(object sender, RoutedEventArgs e)
        {
          MainSection.Content = new MessageLayer();
        }

        private void Dragmove_TouchMove(object sender, TouchEventArgs e)
        {
            //DragMove();
        }

        private void Dragmove_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                DragMove();
            }
            catch (Exception)
            {
                
            }
        }

        private void Dashboard_nav_dashboard_button_MouseEnter(object sender, MouseEventArgs e)
        {
            //string packUri = @"./Assets/ Icons / dashboard.png";
            ////string packUri = @"pack://application:,,,/Resources/Polaroid.png";
            //dashboard_nav_dashboard_button.= new ImageSourceConverter().ConvertFromString(packUri) as ImageSource;
        }

        private void Dashboard_nav_dashboard_button_MouseLeave(object sender, MouseEventArgs e)
        {
            //string packUri = @"./Assets/ Icons / dashboard.png";
            ////string packUri = @"pack://application:,,,/Resources/Polaroid.png";
            //dashboard_nav_dashboard_button.Background.Source = new ImageSourceConverter().ConvertFromString(packUri) as ImageSource;
        }

        private void Logout_button_Click(object sender, RoutedEventArgs e)
        {
            SessionItem.item.Remove(ItemsEnum.IsText.ToString());
            SessionItem.item.Remove(ItemsEnum.IsAttachFile.ToString());
            SessionItem.item.Remove(ItemsEnum.IsDocFile.ToString());
            SessionItem.item.Remove(ItemsEnum.DocFilePath.ToString());
            SessionItem.item.Remove(ItemsEnum.MessageTest.ToString());
            SessionItem.item.Remove(ItemsEnum.ImageFilePath.ToString());

            new MainWindow().Show();
            this.Close();
        }
        //Settings Button End///

        //private void Dashboard_nav_dashboard_button_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    ImageBrush image = new ImageBrush();
        //    image.ImageSource = new BitmapImage(new Uri(@"Assets/newIcon/2h.png"));

        //    dashboard_nav_dashboard_button.Background = image;
        //}



        //private void dashboard_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    dashboard.DragMove();
        //}

        //private void dashboard_Loaded(object sender, RoutedEventArgs e)
        //{
        //    channelhover.Visibility = Visibility.Hidden;
        //    grouphover.Visibility = Visibility.Hidden;
        //    messagehover.Visibility = Visibility.Hidden;
        //    numberbtn_Copy.Visibility = Visibility.Hidden;

        //}

        //private void btnhome2_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnhome2_MouseEnter(object sender, MouseEventArgs e)
        //{

        //}

        //private void btnnext_Click(object sender, RoutedEventArgs e)
        //{
        //    dashboraduserWpf gd = new dashboraduserWpf();
        //    grid1.Children.Clear();
        //    grid1.Children.Add(gd);
        //}

        //private void btnnext_Copy_Click(object sender, RoutedEventArgs e)
        //{
        //    Messages msg = new  Messages();
        //    grid1.Children.Clear();
        //    grid1.Children.Add(msg);
        //}

        //private void btnnext_Copy1_Click(object sender, RoutedEventArgs e)
        //{
        //    Number gd = new Number();
        //    grid1.Children.Clear();
        //    grid1.Children.Add(gd);

        //}

        //private void btnnext_Copy3_Click(object sender, RoutedEventArgs e)
        //{
        //    GroupLayerUIWpf gd = new GroupLayerUIWpf();
        //    grid1.Children.Clear();
        //    grid1.Children.Add(gd);
        //}

        //private void btnnext_Copy2_Click(object sender, RoutedEventArgs e)
        //{
        //    Messages msg = new  Messages();
        //    grid1.Children.Clear();

        //    grid1.Children.Add(msg);
        //}

        //private void btnnext_Copy3_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    btnnext_Copy3.Visibility = Visibility.Hidden;
        //    grouphover.Visibility = Visibility.Visible;
        //}

        //private void grouphover_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    btnnext_Copy3.Visibility = Visibility.Visible;
        //    grouphover.Visibility = Visibility.Hidden;
        //}

        //private void messagetbn_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    messagetbn.Visibility = Visibility.Hidden;
        //    messagehover.Visibility = Visibility.Visible;
        //}

        //private void messagehover_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    messagetbn.Visibility = Visibility.Visible;
        //    messagehover.Visibility = Visibility.Hidden;
        //}

        //private void btnnext_Copy_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    btnnext_Copy.Visibility = Visibility.Hidden;
        //    channelhover.Visibility = Visibility.Visible;
        //}

        //private void channelhover_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    btnnext_Copy.Visibility = Visibility.Visible;
        //    channelhover.Visibility = Visibility.Hidden;

        //}

        //private void numberbtn_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    numberbtn.Visibility = Visibility.Hidden;
        //    numberbtn_Copy.Visibility = Visibility.Visible;
        //}

        //private void numberbtn_Copy_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    numberbtn.Visibility = Visibility.Visible;
        //    numberbtn_Copy.Visibility = Visibility.Hidden;
        //}

        //private void btnnext_Copy3_Click_1(object sender, RoutedEventArgs e)
        //{

        //}

        //private void grouphover_Click(object sender, RoutedEventArgs e)
        //{
        //    GroupUI gui = new GroupUI();
        //    grid1.Children.Clear();
        //    grid1.Children.Add(gui);

        //}

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    Application.Current.Shutdown();
        //}

        //private void Btnnext_Copy4_Click(object sender, RoutedEventArgs e)
        //{

        //}
    }
}
