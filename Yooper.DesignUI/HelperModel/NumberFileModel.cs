﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperModel
{
    internal class NumberFileModel
    {
        public List<string> Number { get; set; }

        public List<string> Name { get; set; }

        public List<string> Exist { get; set; }

        public List<string> Status { get; set; }
    }
}
