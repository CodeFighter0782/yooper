﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperModel
{
    class NumberModel
    {
        public string Number { get; set; }

        public string Name { get; set; }

        public string Exist { get; set; }

        public string Status { get; set; }
    }
}
