﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Yooper.UOW;
using YooperDesignUI.HelperClasses;
using Yopper.Common;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IUnitofWork _uow;

        public MainWindow()
        {
            InitializeComponent();
           _uow = App.GlobalKernel.Get<IUnitofWork>();
        }

        // Placeholder values
        private string emailPlaceholderValue = "Email or Mobile";
        private string passwordPlaceholderValue = "Password";
        private string keyPlaceoldervalue = "Key";

        // colors

        //private Brush textPlaceholderColor = new SolidColorBrush();
        SolidColorBrush textPlaceholderColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF787676"));

        //private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        //{
        //    this.SignUPform.DragMove();
        //}


        ///// Email TextBox Start /////
        ///// Email Placeholder Start /////
        private void Email_login_textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (email_login_textBox.Text == emailPlaceholderValue)
            {
                email_login_textBox.Text = string.Empty;
                email_login_textBox.Foreground = Brushes.Black;
            }
        }

        private void Email_login_textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(email_login_textBox.Text))
            {
                email_login_textBox.Text = emailPlaceholderValue;
                email_login_textBox.Foreground = textPlaceholderColor;
            }
        }
        ///// Email Placeholder End /////
        ///// Email TextBox Start /////


        ///// Password TextBox Start /////
        ///// Password Placeholder Start /////
        private void Password_login_textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (password_login_textBox.Password == passwordPlaceholderValue)
            {
                password_login_textBox.Password = string.Empty;
                password_login_textBox.Foreground = Brushes.Black;
            }
        }

        private void Password_login_textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(password_login_textBox.Password))
            {
                password_login_textBox.Password = passwordPlaceholderValue;
                password_login_textBox.Foreground = textPlaceholderColor;
            }
        }
        ///// Password placeholder End /////
        ///// Key TextBox End /////


        ///// Key TextBox Start /////
        ///// Key Placeholder Start /////
        private void Key_login_textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (key_login_textBox.Text == keyPlaceoldervalue)
            {
                key_login_textBox.Text = string.Empty;
                key_login_textBox.Foreground = Brushes.Black;
            }
        }

        private void Key_login_textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(key_login_textBox.Text))
            {
                key_login_textBox.Text = keyPlaceoldervalue;
                key_login_textBox.Foreground = textPlaceholderColor;
            }
        }
        ///// Key Placeholder End /////
        ///// Key TextBox End /////


        ///// SignIn Button Start /////
        private void Login_button_Click(object sender, RoutedEventArgs e)
        {
            //if (!string.IsNullOrWhiteSpace(email_login_textBox.Text) && email_login_textBox.Text != emailPlaceholderValue)
            //{

            //}

            //if (!string.IsNullOrWhiteSpace(password_login_textBox.Text) && password_login_textBox.Text != passwordPlaceholderValue)
            //{

            //}

            //if (!string.IsNullOrWhiteSpace(key_login_textBox.Text) && key_login_textBox.Text != keyPlaceoldervalue)
            //{

            //}

            //new DashBoard().Show();

            //this.Close();

            try
            {
            //    if (email_login_textBox.Text == emailPlaceholderValue)
            //    {
            //        messageBoox = new MessageBoox("Attack on titan");
            //        messageBoox.ShowDialog();
            //        return;
            //    }
            //    else
            //    {
                    login_button.Content = "Loading...";
                    var thread = new Thread(x => { LoginWrapper(); }) { IsBackground = true };
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.Start();
                
            }
            catch (YooperException ex)
            {
                MessageBox.Show(ex.ErrorMessage);
            }
        }
        ///// Key TextBox End /////


        private void LoginWrapper()
        {
            try
            {
                string username = string.Empty, password = string.Empty, key = string.Empty;
                this.Dispatcher.Invoke(() =>
                {
                    username = email_login_textBox.Text.ToString();
                    password = password_login_textBox.Password.ToString();
                    key = key_login_textBox.Text.ToString();
                });
                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(key) || username == emailPlaceholderValue || key == keyPlaceoldervalue)
                {

                    if (string.IsNullOrEmpty(username) || username == emailPlaceholderValue)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            ShowMessageBox.ShowMessage("Email or Number is required");
                            return;
                        });
                    }
                    else if (string.IsNullOrEmpty(password))
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            ShowMessageBox.ShowMessage("Password is required");
                            return;
                        });
                    }
                    else if(string.IsNullOrWhiteSpace(key) || key == keyPlaceoldervalue)
                    {
                        this.Dispatcher.Invoke(() =>
                        {
                            ShowMessageBox.ShowMessage("Key is required");
                            return;
                        });
                    }

                }
                else
                {
                    var user = _uow.Account.Login(username, password, key);
                    if (user != null)
                    {

                        _uow.Account.AddUser(new Yooper.DTO.SqliteCustomUsers()
                        {
                            LicenseKey = user.SecretKey,
                            CreatedBy = user.Username,
                            CreatedDate = DateTime.UtcNow.ToString(),
                            Enable = 1,
                            IsLogin = 1,
                            Restart = 0,
                            TwoWayAuthentication = 0,
                            Username = user.Username,
                            UserId = (int)user.Id,
                            UpdatedBy = string.Empty,
                            UpdatedDate = string.Empty
                        });
                        //MessageBox.Show("Login successful");

                        SessionItem.Username = user.Username;
                        SessionItem.UserId = user.Id;
                        SessionItem.item.Add(ItemsEnum.IsText.ToString(), false);
                        SessionItem.item.Add(ItemsEnum.IsAttachFile.ToString(), false);
                        SessionItem.item.Add(ItemsEnum.IsDocFile.ToString(), false);
                        SessionItem.item.Add(ItemsEnum.MessageTest.ToString(), string.Empty);
                        SessionItem.item.Add(ItemsEnum.ImageFilePath.ToString(), string.Empty);
                        SessionItem.item.Add(ItemsEnum.DocFilePath.ToString(), string.Empty);

                        this.Dispatcher.Invoke(() =>
                        {
                            new DashBoard().Show();
                            this.Close();
                        });

                        var thread = new Thread(x => { CheckRestartBP(user.Id); }) { IsBackground = true };
                        thread.SetApartmentState(ApartmentState.STA);
                        thread.Start();
                        _uow.Account.MakeUserLogin(true);
                    }
                    else
                    {
                        _uow.Account.MakeUserLogin(false);
                        ShowMessageBox.ShowMessage("username/password is invalid");
                        //MessageBox.Show("username/password is invalid");
                    }
                    this.Dispatcher.Invoke(() =>
                    {
                        email_login_textBox.Text = "";
                        password_login_textBox.Password = "";
                        key_login_textBox.Text = "";
                    });

                }
                this.Dispatcher.Invoke(() =>
                {
                    login_button.IsEnabled = true;
                    login_button.Content = "Sign In";
                });

            }
            catch (YooperException ex)
            {
                this.Dispatcher.Invoke(() =>
                {
                    ShowMessageBox.ShowMessage(ex.ErrorMessage);
                    login_button.IsEnabled = true;
                    login_button.Content = "Sign In";
                });
            }
        }

        private void CheckRestartBP(long userId)
        {
            while (true)
            {
                if (_uow.Account.CheckIfApplicationRestart(userId))
                {
                    _uow.Account.EnableRestart(userId);
                }
                Thread.Sleep(5000);
            }
        }

        private void Drag_header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //DragMove();
        }

        private void Drag_header_DragOver(object sender, DragEventArgs e)
        {
           // DragMove();
        }

        private void DragWin_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void ExitProgram_button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }

}
