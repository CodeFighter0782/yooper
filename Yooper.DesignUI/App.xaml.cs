﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Yooper.UOW;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public static IKernel GlobalKernel;
        public App()
        {
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            IKernel kernel = new StandardKernel();
            UOWRegistration.BindAll(kernel);
            kernel.Bind<MainWindow>().ToSelf();
            GlobalKernel = kernel;

            var uow = GlobalKernel.Get<IUnitofWork>();
            if (uow.Setting.Get().Count <= 0)
            {
                uow.Setting.Add(new Yooper.DTO.SqliteSettings()
                {
                    Delay = 3,
                    Pause = 0,
                    Stop = 0,
                    Enable = 1
                });
            };
        }
    }
}