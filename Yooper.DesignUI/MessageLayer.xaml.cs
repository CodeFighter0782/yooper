﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using YooperDesignUI.HelperClasses;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for MessageLayer.xaml
    /// </summary>
    public partial class MessageLayer : UserControl
    {
        public MessageLayer()
        {
            InitializeComponent();
        }

        private void SnigleMessageDelayMin_textBox_KeyUp(object sender, KeyEventArgs e)
        {

            //string value = snigleMessageDelayMin_textBox.Text;
            //if (!string.IsNullOrWhiteSpace(value) && !value.All(char.IsDigit))
            //{
            //    ShowMessageBox.ShowMessage("Please enter number only");
            //    return;
            //}

            //if (!string.IsNullOrWhiteSpace(value) && Convert.ToInt32(value) <= 0)
            //{
            //    ShowMessageBox.ShowMessage(string.Format("Value should greater then 0"));
            //    return;
            //}

            //if (!string.IsNullOrWhiteSpace(value))
            //{
            //    ShowMessageBox.ShowMessage("OK");
            //}


        }

        private void SnigleMessageDelayMax_textBox_KeyUp(object sender, KeyEventArgs e)
        {
            //string value = snigleMessageDelayMax_textBox.Text;
            //if (!string.IsNullOrWhiteSpace(value) && !value.All(char.IsDigit))
            //{
            //    ShowMessageBox.ShowMessage("Please enter number only");
            //    return;
            //}

            //if (!string.IsNullOrWhiteSpace(value) && Convert.ToInt32(value) < Convert.ToInt32(snigleMessageDelayMin_textBox.Text))
            //{
            //    ShowMessageBox.ShowMessage(string.Format("Value should greater then {0}", snigleMessageDelayMin_textBox.Text));
            //    return;
            //}

            //if (!string.IsNullOrWhiteSpace(value))
            //{
            //    ShowMessageBox.ShowMessage("OK");
            //}
        }

        private void SnigleMessageDelayMin_textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            string value = snigleMessageDelayMin_textBox.Text;

            if (string.IsNullOrWhiteSpace(value))
            {

            }

            if (!string.IsNullOrWhiteSpace(value) && !value.All(char.IsDigit))
            {
                ShowMessageBox.ShowMessage("Please enter number only");
                return;
            }

            if (!string.IsNullOrWhiteSpace(value) && Convert.ToInt32(value) <= 0)
            {
                ShowMessageBox.ShowMessage(string.Format("Value should greater then 0"));
                return;
            }
            if (!string.IsNullOrWhiteSpace(value))
            {
                return;
            } 
        }
    }
}
