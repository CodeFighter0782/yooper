﻿using Ninject;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.DTO;
using Yooper.UOW;
using YooperDesignUI.HelperClasses;
using YooperDesignUI.HelperModel;
using MessageBox = System.Windows.MessageBox;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for Number.xaml
    /// </summary>
    public partial class Number : Page
    {
        private IUnitofWork _uow;

        private MessageBoox messageBoox;

        private int sendingMethod = 1;

        public Number()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();

            checkNumberList();
            checkRadioButton();
        }

        private NumberFileModel numberFileModel = new NumberFileModel()
        {
            Number = new List<string>(),
            Name = new List<string>(),
            Exist = new List<string>(),
            Status = new List<string>()
        };

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            show_number.Visibility = Visibility.Visible;
            show_number.Content = new MessageUILayer();
        }

        // Generate Number Start///
        private void GenrateNumber_button_Click(object sender, RoutedEventArgs e)
        {
            show_number.Visibility = Visibility.Visible;
            show_number.Content = new MessageUILayer();
        }

        private void AddFile_Click(object sender, RoutedEventArgs e)
        {

            var ofd = new Microsoft.Win32.OpenFileDialog(); // open file dialog
            var result = ofd.ShowDialog();
            if (result == false) return;
            StreamReader st = new StreamReader(ofd.FileName); // read file

            var file = NumberFileReader.File(st);

            addNumberList(file);

            numberFileModel = file;

            //numbers_column.ItemsSource = file.Number;
            //numbers_column_1.ItemsSource = file.Name;
            //numbers_column_2.ItemsSource = file.Exist;
            //numbers_column_3.ItemsSource = file.Status;


            //var ofd = new Microsoft.Win32.OpenFileDialog(); // open file dialog
            //var result = ofd.ShowDialog();
            //if (result == false) return;
            //TextReader tr = new StreamReader(ofd.FileName); // read file

            //NumberFileReader.File(tr);

            //string row_Val;
            //while ((row_Val = tr.ReadLine()) != null)
            //{

            //    var numberListTemp = _uow.Account.GetNumbers().ToList();

            //    if (numberListTemp.Count != 0)
            //    {
            //        for (int z = 0; z < numberListTemp.Count; z++)
            //        {
            //            string row_value = row_Val != null ? row_Val.ToString().TrimEnd() : "";
            //            if (!row_value.Contains(","))
            //            {
            //                if (numberListTemp[z].PhoneNumber.Equals(row_value))
            //                {
            //                    //MessageBox.Show("This Number is already exist in database");
            //                    break;
            //                }
            //                else if (z == (numberListTemp.Count - 1))
            //                {
            //                    add_Number("", row_Val.Trim().Replace(" ", ""));
            //                }
            //            }
            //            else
            //            {
            //                string[] temp = row_value.Split(',');
            //                if (numberListTemp[z].PhoneNumber.Equals(temp[0].Trim().Replace(" ", "")))
            //                {
            //                    //MessageBox.Show("This Number is already exist in database");
            //                    break;
            //                }
            //                else if (z == (numberListTemp.Count - 1))
            //                {
            //                    add_Number(temp[1], temp[0].Trim().Replace(" ", ""));
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        string row_value = row_Val != null ? row_Val.ToString().TrimEnd() : "";
            //        string[] temp = row_value.Split(',');
            //        if (row_Val.Contains(","))
            //        {
            //            add_Number(temp[1], temp[0].Trim().Replace(" ", ""));
            //        }
            //        else
            //        {
            //            add_Number("", row_value.Trim().Replace(" ", ""));
            //        }
            //    }
            //}

            //var numbers = _uow.Account.GetNumbers();

            //List<string> no = new List<string>();

            //foreach(var item in numbers)
            //{
            //    no.Add(item.PhoneNumber);
            //}

            //numbers_column.ItemsSource = no;

            //this.numbers = no;
        }
        // Generate Number End///

        private void add_Number(string name, string number)
        {
            try
            {
                _uow.Account.AddNumber(name, number);
                //this.UpdateDataGrid();
                //this.ResetAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
        }

        private void SaveNumbers_Click(object sender, RoutedEventArgs e)
        {
            if (numberFileModel.Number.Count > 0)
            {
                using (System.Windows.Forms.SaveFileDialog sfvDialog = new System.Windows.Forms.SaveFileDialog() {Filter = "CSV|*.csv", ValidateNames = true})
                {
                    if (sfvDialog.ShowDialog() == DialogResult.OK)
                    {
                        string fileName = sfvDialog.FileName;

                        if (!string.IsNullOrWhiteSpace(fileName))
                        {

                            bool isSaved = NumberFileWriter.CSVFileWriter(fileName, numberFileModel);

                            if (isSaved)
                            {
                                ShowMessageBox.ShowMessage("File saved");
                            }
                            else
                            {
                                ShowMessageBox.ShowMessage("File not saved");
                            }

                        }
                        else
                        {
                            ShowMessageBox.ShowMessage("Make a file to save the number");
                        }
                    }
                }
            }
            else
            {
                ShowMessageBox.ShowMessage("No number to save");
                return;
            }
        }

        private void Start_button_Click(object sender, RoutedEventArgs e)
        {
            // What is going here


            // Message
            //var sms = _uow.Account.GetMessages();
            //string massage = "helloworld";
            //if (sms.Count > 0)
            //{
            //    massage = sms[sms.Count - 1].Text;
            //}


            if (numberFileModel.Number.Count == 0)
            {
                messageBoox = new MessageBoox("Add Phone Numbers");
                messageBoox.ShowDialog();
                return;
            }

            if (sendingMethod == (int) SendingMethod.INDIVIDUAL)
            {
                var thread = new Thread(x => { IndividualMessageSend(); }) { IsBackground = true };
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();

            } else if (sendingMethod == (int)SendingMethod.GROUP)
            {
                //MessageBox.Show("Hello world");
                //var thread = new Thread(() => { this.SendGroupMessage(); }) { IsBackground = true };
                //thread.SetApartmentState(ApartmentState.STA);
                //thread.Start();
                //try
                //{
                //    string groupName = "TokyoGhoul";
                //    string whatsAppGroupId = string.Empty;
                //    var thread = new Thread(() => {
                //        //Logic will go here!!! 
                //        if (numberFileModel.Number.Count > 0)
                //        {
                //            foreach (var item in numberFileModel.Number)
                //            {
                //                try
                //                {
                //                    if (string.IsNullOrEmpty(whatsAppGroupId))
                //                    {
                //                        SessionItem._whatsapp.OneToOneGroup(groupName, item, massage, out whatsAppGroupId);

                //                    }
                //                    else
                //                    {
                //                        SessionItem._whatsapp.OneToOneGroup(groupName, item, massage, out whatsAppGroupId, false);
                //                    }
                //                }
                //                catch
                //                {
                //                }
                //            }

                //            Dispatcher.Invoke(() =>
                //            {

                //            });

                //        }
                //    })
                //    { IsBackground = true };
                //    thread.SetApartmentState(ApartmentState.STA);
                //    thread.Start();
                //}
                //catch (Exception ex)
                //{

                //    throw ex;
                //}
            }
        }

        private void ClearNumbers_Click(object sender, RoutedEventArgs e)
        {
            clearNumberList();
        }

        ////private void Button_Click(object sender, RoutedEventArgs e)
        ////{
        ////    show_number.Visibility = Visibility.Visible;
        ////    show_number.Content = new MessageUILayer();
        ////}
        ///

        private void checkNumberList()
        {
            try
            {
                var numberList = SessionItem.item["NumberList"] as NumberFileModel;

                if (numberList != null)
                {
                    numbers_column.ItemsSource = numberList.Number;
                    numbers_column_1.ItemsSource = numberList.Name;
                    numbers_column_2.ItemsSource = numberList.Exist;
                    numbers_column_3.ItemsSource = numberList.Status;

                    numberFileModel = numberList;
                }
            }
            catch (Exception)
            {

            }

            
        }


        private void addNumberList(NumberFileModel model)
        {
            SessionItem.item.Remove("NumberList");   
            SessionItem.item.Add("NumberList", model);

            numbers_column.ItemsSource = model.Number;
            numbers_column_1.ItemsSource = model.Name;
            numbers_column_2.ItemsSource = model.Exist;
            numbers_column_3.ItemsSource = model.Status;
        }

        private void clearNumberList()
        {

            if (numberFileModel.Number.Count > 0)
            {
                SessionItem.item.Remove("NumberList");
                numberFileModel = new NumberFileModel()
                {
                    Number = new List<string>(),
                    Name = new List<string>(),
                    Exist = new List<string>(),
                    Status = new List<string>()
                };
                numbers_column.ItemsSource = null;
                numbers_column_1.ItemsSource = null;
                numbers_column_2.ItemsSource = null;
                numbers_column_3.ItemsSource = null;
            }
            
        }

        // Radio Button Start////
        private void Individual_radioButton_Click(object sender, RoutedEventArgs e)
        {
            addRadioButton((int)SendingMethod.INDIVIDUAL);
        }

        private void Group_radioButton_Click(object sender, RoutedEventArgs e)
        {
            addRadioButton((int)SendingMethod.GROUP);
        }

        private void OneGroupConer_radioButton_Click(object sender, RoutedEventArgs e)
        {
            addRadioButton((int)SendingMethod.ONE_GROUP_CORNER);
        }

        private void Forward_radioButton_Click(object sender, RoutedEventArgs e)
        {
            addRadioButton((int)SendingMethod.FORWARD);
        }

        private void addRadioButton(int value)
        {
            SessionItem.item.Remove("SendingMethod");
            SessionItem.item.Add("SendingMethod", value);
            sendingMethod = value;
        }

        private void checkRadioButton()
        {
            try
            {
                int value = Convert.ToInt32(SessionItem.item["SendingMethod"]);

                if (value == (int)SendingMethod.INDIVIDUAL)
                {
                    individual_radioButton.IsChecked = true;
                    sendingMethod = 1;
                }
                else if (value == (int)SendingMethod.GROUP)
                {
                    group_radioButton.IsChecked = true;
                    sendingMethod = 2;
                }
                else if (value == (int)SendingMethod.ONE_GROUP_CORNER)
                {
                    oneGroupConer_radioButton.IsChecked = true;
                    sendingMethod = 3;
                }
                else if (value == (int)SendingMethod.FORWARD)
                {
                    forward_radioButton.IsChecked = true;
                    sendingMethod = 4;
                }
            }
            catch (Exception)
            {

                individual_radioButton.IsChecked = true;
                sendingMethod = 1;
            }
        }

        // Radio Button Start////

        private void IndividualMessageSend()
        {
            try
            {
                bool? isText = SessionItem.item[ItemsEnum.IsText.ToString()] as bool?;
                bool? isImageFile = SessionItem.item[ItemsEnum.IsAttachFile.ToString()] as bool?;
                bool? isDocFile = SessionItem.item[ItemsEnum.DocFilePath.ToString()] as bool?;
                string textMassage = SessionItem.item[ItemsEnum.MessageTest.ToString()] as string;
                string imageFilePath = SessionItem.item[ItemsEnum.ImageFilePath.ToString()] as string;
                string docFilePath = SessionItem.item[ItemsEnum.DocFilePath.ToString()] as string;

            if (!_uow.Account.CheckChannelStatus())
            {
                    messageBoox = new MessageBoox("Please connect to WhatsApp through channel.");
                    messageBoox.ShowDialog();
                    return;
            }
            else
            {
                int massageCount = 0;

                if (isText.Value && isImageFile.Value)
                {
                    if (string.IsNullOrWhiteSpace(textMassage) || string.IsNullOrWhiteSpace(imageFilePath))
                    {
                        messageBoox = new MessageBoox("Add Message Text and Image File");
                        messageBoox.ShowDialog();
                        return;
                    }

                    foreach (var item in numberFileModel.Number)
                    {
                        SessionItem._whatsapp.SendMessage(item, imageFilePath, textMassage, isImageFile.Value);

                        //Thread.Sleep(3000);
                        //SessionItem._whatsapp.SendMessage(item, textMassage + "  " + massageCount.ToString());

                        Thread.Sleep(3000);
                        massageCount++;
                    }

                    messageBoox = new MessageBoox("Completed");
                    messageBoox.ShowDialog();
                }
                else if (isText.Value)
                {
                    if (string.IsNullOrWhiteSpace(textMassage))
                    {
                        messageBoox = new MessageBoox("Add text message");
                        messageBoox.ShowDialog();
                        return;
                    }

                    foreach (var item in numberFileModel.Number)
                    {
                        SessionItem._whatsapp.SendMessage(item, textMassage);

                        Thread.Sleep(3000);
                        massageCount++;
                    }
                    messageBoox = new MessageBoox("Completed");
                    messageBoox.ShowDialog();
                    }
                else if (isImageFile.Value)
                {
                    if (string.IsNullOrWhiteSpace(imageFilePath))
                    {
                        MessageBox.Show("Add image file");
                        messageBoox = new MessageBoox("Add image file");
                        messageBoox.Show();
                        return;
                    }

                    foreach (var item in numberFileModel.Number)
                    {
                        SessionItem._whatsapp.SendMessage(item, imageFilePath, isImageFile.Value);

                        Thread.Sleep(3000);
                        massageCount++;
                    }

                    MessageBox.Show("Completed");
                }
                else
                {
                    messageBoox = new MessageBoox("Please mark text, image or both");
                    messageBoox.ShowDialog();
                    return;
                }

            }

            }
            catch (Exception ex)
            {
                
            }
        }

        private void SendGroupMessage(NumberFileModel model, string groupNames, string message)
        {

            

            //    SessionItem.item.Add("IsText", true);

            //    try
            //    {
            //        string[] numbers = null;
            //        string message = string.Empty;
            //        this.Dispatcher.Invoke(() =>
            //        {
            //            // What is This?
            //            //numbers = textBlock2.Text.Split(',');
            //            //message = textBlock3.Text;
            //        });

            //        if (numbers != null)
            //        {
            //           // double totalRecord = numbers.Length, count = 1, percentage = 0;
            //            foreach (var item in model.Number)
            //            {
            //                //while (_uow.Setting.Get().FirstOrDefault().Pause == 1)
            //                //{

            //                //}
            //                //if (_uow.Setting.Get().FirstOrDefault().Stop == 1)
            //                //{
            //                //    break;
            //                //}
            //                if (!string.IsNullOrEmpty(item))
            //                {
            //                    //var numberObj = _uow.Account.GetNumbers(item).FirstOrDefault();
            //                    if (Convert.ToBoolean(SessionItem.item["IsText"]))
            //                    {
            //                        try
            //                        {
            //                            SessionItem._whatsapp.SendMessage(item, message);
            //                            this.AddStatus(item);
            //                            if (numberObj != null)
            //                            {
            //                                _uow.Account.SetNumberValidity(numberObj.Id, true);
            //                                _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 1, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
            //                                _uow.CampaignAudit.Add(new SqliteCampaignAudits()
            //                                {
            //                                    Message = message,
            //                                    Number = item,
            //                                    MessageType = EMessageType.GROUP.ToString()
            //                                });

            //                            }
            //                        }
            //                        catch
            //                        {
            //                            this.AddStatus(item, "Number is invalid");
            //                            if (numberObj != null)
            //                            {
            //                                _uow.Account.SetNumberValidity(numberObj.Id, false);
            //                                _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        try
            //                        {
            //                            SessionItem._whatsapp.SendMessage(item, message, true);
            //                            this.AddStatus(item);
            //                            if (numberObj != null)
            //                            {
            //                                _uow.Account.SetNumberValidity(numberObj.Id, true);
            //                                _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 1, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
            //                                _uow.CampaignAudit.Add(new SqliteCampaignAudits()
            //                                {
            //                                    Message = message,
            //                                    Number = item,
            //                                    MessageType = EMessageType.GROUP.ToString()
            //                                });
            //                            }
            //                        }
            //                        catch
            //                        {
            //                            this.AddStatus(item, "Number is invalid");
            //                            if (numberObj != null)
            //                            {
            //                                _uow.Account.SetNumberValidity(numberObj.Id, false);
            //                                _uow.Account.AddSentAudits(new SqliteSendAudits { NumberId = numberObj.Id, IsCompleted = 0, Enable = 1, CreatedDate = DateTime.Now.ToString(), CreatedBy = string.Empty, UpdatedBy = string.Empty, UpdatedDate = string.Empty });
            //                            }
            //                        }
            //                    }

            //                }
            //                var _thread = new Thread(() => { this.Timer(SessionItem.Delay); }) { IsBackground = true };
            //                _thread.SetApartmentState(ApartmentState.STA);
            //                _thread.Start();
            //                Thread.Sleep(1000 * SessionItem.Delay);
            //                percentage = (count / totalRecord) * 100;
            //                this.Dispatcher.Invoke(() =>
            //                {
            //                    label4.Content = percentage.ToString("0.00") + "% Completed";
            //                });
            //                count++;
            //            }
            //        }
            //        this.Dispatcher.Invoke(() =>
            //        {
            //            this.Clear();
            //            button.IsEnabled = false;
            //            button.Content = "SEND";
            //        });
            //    }
            //    catch (Exception ex)
            //    {
            //        //MessageBox.Show(ex.Message);
            //    }
        }

        
    }
}
