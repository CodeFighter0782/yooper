﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Yooper.UOW;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for Channel.xaml
    /// </summary>
    public partial class Channel : UserControl
    {
        private IUnitofWork _uow;

        public Channel()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
        }

        // Placeholder values
        private string searchPlaceholderValue = "Channel";

        SolidColorBrush textPlaceholderColor = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF787676"));

        // Search TextBox Start///

        // GotFocus
        private void Search_channel_textBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (search_channel_textBox.Text == searchPlaceholderValue)
            {
                search_channel_textBox.Text = string.Empty;
                search_channel_textBox.Foreground = Brushes.Black;
            }
        }

        // LostFocus
        private void Search_channel_textBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(search_channel_textBox.Text))
            {
                search_channel_textBox.Text = searchPlaceholderValue;
                search_channel_textBox.Foreground = textPlaceholderColor;
            }
        }

        // Search TextBox End///


        // Open Chrome Button Start///

        // Click
        private void OpenChrome_channel_button_Click(object sender, RoutedEventArgs e)
        {
            var thread = new Thread(x =>
            {
                try
                {
                    while (true)
                    {
                        if (!SessionItem.InProcess)
                        {
                            SessionItem.InProcess = true;
                            SessionItem._whatsapp.run();
                            SessionItem.InProcess = false;
                            break;
                        }

                    }
                    this.Dispatcher.Invoke(() =>
                    {
                        //MessageBox.Show("Connection build successfully.");
                        _uow.Account.MakeChangeInChannelStatus(true);
                        //open_whatsapp_btn.Content = "Connected";
                        //open_whatsapp_btn.IsEnabled = false;
                        //button1.IsEnabled = true;
                        //button2.IsEnabled = true;
                        //button.IsEnabled = true;
                        //var auto_reply_thread = new Thread(() =>
                        //{
                        //    this.AutoReply();
                        //})
                        //{ IsBackground = true };
                        //auto_reply_thread.SetApartmentState(ApartmentState.STA);
                        //auto_reply_thread.Start();
                    });
                }
                catch (Exception ex)
                {
                    SessionItem.InProcess = false;
                    this.Dispatcher.Invoke(() =>
                    {
                        MessageBox.Show(ex.Message, "Error");
                    });
                }
            })
            { IsBackground = true };
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();

        }
        // Open Chrome Button End///


        // Hide Chrome Button Start///
        private void HideChrome_channel_button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SessionItem._whatsapp.Minimize();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        // Hide Chrome Button End///

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void Button_Click_1(object sender, RoutedEventArgs e)
        //{
        //    Application.Current.Shutdown();
        //}
    }
}