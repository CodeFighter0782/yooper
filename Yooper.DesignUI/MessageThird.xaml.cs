﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Yooper.UOW;
using YooperDesignUI.HelperClasses;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for MessageThird.xaml
    /// </summary>
    public partial class MessageThird : Page
    {
        private IUnitofWork _uow;

        public MessageThird()
        {
            InitializeComponent();
            _uow = App.GlobalKernel.Get<IUnitofWork>();
            GetSessionItems();
        }


        // Add Text Button Start///
        // Click
        private void AddMassegaText_button_Click(object sender, RoutedEventArgs e)
        {
        //    _uow.Account.AddMessage(massageText_textBox.Text);

        //    massegaText_lable.Content = massageText_textBox.Text;

        //    massageText_textBox.Text = string.Empty;
            
            if (!string.IsNullOrWhiteSpace(massageText_textBox.Text))
            {
                StringBuilder sb = new StringBuilder();
                
                string prevText = SessionItem.item[ItemsEnum.MessageTest.ToString()] as string;
                
                string newMassega = massageText_textBox.Text;

                prevText += newMassega;

                SessionItem.item[ItemsEnum.MessageTest.ToString()] = prevText;
                GetSessionItems();

                massageText_textBox.Text = string.Empty;
            }
        }
        // Add Text Button End///


        // Add Image/Video Button Start///
        // Click
        private void AddImageOrVideo_Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog file = new OpenFileDialog();
            if (file.ShowDialog() == true)
            {
                string fileName = Guid.NewGuid().ToString() + file.FileName.Substring(file.FileName.IndexOf('.'));
                string _fileName = file.FileName;

                if (!string.IsNullOrWhiteSpace(_fileName))
                {
                    SessionItem.item[ItemsEnum.ImageFilePath.ToString()] = _fileName;

                    GetSessionItems();
                }
            }
        }
        // Add Image/Video Button End///


        // Text CheckBox Start///
        // Click
        private void Text_checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (!text_checkBox.IsChecked.Value)
            {
                SessionItem.item[ItemsEnum.IsText.ToString()] = false;
            } else
            {
                SessionItem.item[ItemsEnum.IsText.ToString()] = true;
            }
        }
        // Text CheckBox End///


        // Image/Video CheckBox Start///
        // Click
        private void ImageVideo_checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (!imageVideo_checkBox.IsChecked.Value)
            {
                SessionItem.item[ItemsEnum.IsAttachFile.ToString()] = false;
            } else
            {
                SessionItem.item[ItemsEnum.IsAttachFile.ToString()] = true;
            }
        }
        // Image/Video CheckBox End///


        // Document CheckBox Start///
        private void Doc_checkBox_Click(object sender, RoutedEventArgs e)
        {
            if (!doc_checkBox.IsChecked.Value)
            {
                SessionItem.item[ItemsEnum.IsDocFile.ToString()] = false;
            } else
            {
                SessionItem.item[ItemsEnum.IsDocFile.ToString()] = true;
            }
        }
        // Document CheckBox End///


        // Clear Text Massega Button Start///
        // Click
        private void ClearTextMessage_button_Click(object sender, RoutedEventArgs e)
        {
            SessionItem.item[ItemsEnum.MessageTest.ToString()] = string.Empty;

            GetSessionItems();
        }
        // Clear Text Massega Button End///
        // Click
        private void ClearImageFilePath_button_Click(object sender, RoutedEventArgs e)
        {
            SessionItem.item[ItemsEnum.ImageFilePath.ToString()] = string.Empty;
            GetSessionItems();
        }
        // Clear Image File Path Start ///

        private void GetSessionItems()
        {
            text_checkBox.IsChecked = SessionItem.item[ItemsEnum.IsText.ToString()] as bool?;
            imageVideo_checkBox.IsChecked = SessionItem.item[ItemsEnum.IsAttachFile.ToString()] as bool?;
            doc_checkBox.IsChecked = SessionItem.item[ItemsEnum.IsDocFile.ToString()] as bool?;
            massegaText_lable.Content = SessionItem.item[ItemsEnum.MessageTest.ToString()] as string;
            imageFilePath_lable.Content = SessionItem.item[ItemsEnum.ImageFilePath.ToString()] as string;
            docFilePath_lable.Content = SessionItem.item[ItemsEnum.DocFilePath.ToString()] as string;
        }

        
    }
}
