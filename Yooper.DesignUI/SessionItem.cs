﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whatsapp;
using Yooper.UOW;
using YooperDesignUI;

namespace YooperDesignUI
{
    internal static class SessionItem
    {
        public static bool InProcess = false;
        public static string Username { get; set; }
        public static bool Pause = IsPause();
        public static bool Stop = IsStop();
        public static int Delay = GetDelay();

        public static bool IsPause()
        {
            try
            {
                var uow = App.GlobalKernel.Get<IUnitofWork>();
                return uow.Setting.Get().Select(x => x.Pause).FirstOrDefault() == 1 ? true : false;
            }
            catch (Exception ex)
            {

                throw ex; 
            }
        }

        public static bool IsStop()
        {
            try
            {
                var uow = App.GlobalKernel.Get<IUnitofWork>();
                return uow.Setting.Get().Select(x => x.Stop).FirstOrDefault() == 1 ? true : false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static int GetDelay()
        {
            try
            {
                var uow = App.GlobalKernel.Get<IUnitofWork>();
                return uow.Setting.Get().Select(x => x.Delay).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static long UserId { get; set; }

        // What is this?

        public static Dictionary<string, object> item { get; set; } = new Dictionary<string, object>();

        public static WhatsappWrapper _whatsapp { get; set; } = new WhatsappWrapper();

    }
}
