﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YooperDesignUI
{
    /// <summary>
    /// Interaction logic for MessageBoox.xaml
    /// </summary>
    public partial class MessageBoox : Window
    {

        private string message = string.Empty;
        public MessageBoox(string message)
        {
            InitializeComponent();
            this.message = message;

            warring_messageBox_lable.Content = this.message;
        }

        private void Ok_messageBox_button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Close_messageBox_image_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
