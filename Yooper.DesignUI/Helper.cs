﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Yooper.UOW;
using YooperDesignUI;

namespace YooperDesignUI
{
    public class Helper
    {
        public static IUnitofWork _uow= App.GlobalKernel.Get<IUnitofWork>() ;

        public static bool IsCampaignRunning = false;
        public static bool PauseCampaign = false;
        public static bool StopCampaign = false;

        public static Dictionary<string, Thread> threads = new Dictionary<string, Thread>();

        public static bool CheckRestart(long userId)
        {
            return _uow.Account.CheckRestartStartEnabled(userId);
        }

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

    }
}
