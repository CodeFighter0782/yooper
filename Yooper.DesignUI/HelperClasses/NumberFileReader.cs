﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YooperDesignUI.HelperModel;

namespace YooperDesignUI.HelperClasses
{
    internal class NumberFileReader
    {
        public static NumberFileModel File(StreamReader sr)
        {
            try
            {
                string notAvilable = "N/A";

                List<string> numbers = new List<string>();
                List<string> names = new List<string>();
                List<string> exists = new List<string>();
                List<string> status = new List<string>();

                string fileRow = string.Empty;

                while ((fileRow = sr.ReadLine()) != null)
                {
                    int count = fileRow.Count(f => f == ',');

                    string[] temp = fileRow.Split(',');

                    if (count == 0)
                    {
                        if (temp[0].All(char.IsDigit) && !string.IsNullOrWhiteSpace(temp[0]))
                        {
                            numbers.Add(temp[0]);
                            names.Add(notAvilable);
                            exists.Add(notAvilable);
                            status.Add(notAvilable);
                        }
                    } else if (count == 1)
                    {
                        if (temp[0].All(char.IsDigit) && !string.IsNullOrWhiteSpace(temp[0]))
                        {
                            numbers.Add(temp[0]);
                            names.Add(temp[1]);
                            exists.Add(notAvilable);
                            status.Add(notAvilable);
                        }
                    } else if (count == 2)
                    {
                        bool isYesNo = temp[2].ToUpper() == "YES" || temp[2].ToUpper() == "NO" || temp[2] == notAvilable;

                        if (temp[0].All(char.IsDigit) && !string.IsNullOrWhiteSpace(temp[0]) && isYesNo)
                        {
                            numbers.Add(temp[0]);
                            names.Add(temp[1]);
                            exists.Add(temp[2]);
                            status.Add(notAvilable);
                        }
                    } else if (count == 3)
                    {
                        bool isYesNo = temp[2].ToUpper() == "YES" || temp[2].ToUpper() == "NO" || temp[2] == notAvilable;

                        if (temp[0].All(char.IsDigit) && !string.IsNullOrWhiteSpace(temp[0]) && isYesNo)
                        {
                            numbers.Add(temp[0]);
                            names.Add(temp[1]);
                            exists.Add(temp[2]);
                            status.Add(temp[3]);
                        }
                    }
                }

                return new NumberFileModel()
                {
                    Number = numbers,
                    Name = names,
                    Exist = exists,
                    Status = status
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public static List<NumberModel> NumberFileModel2(TextReader tr)
        //{
        //    try
        //    {
                
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //    return null;
        //}
    }
}
