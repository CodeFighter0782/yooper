﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperClasses
{
    internal class ShowMessageBox
    {
        public static void ShowMessage(string message)
        {
            MessageBoox messageBoox = new MessageBoox(message);
            messageBoox.ShowDialog();
        }
    }
}
