﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperClasses
{
    internal enum SendingMethod
    {
        INDIVIDUAL = 1,
        GROUP = 2,
        ONE_GROUP_CORNER = 3,
        FORWARD = 4
    }
}
