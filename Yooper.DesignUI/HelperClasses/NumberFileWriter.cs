﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using YooperDesignUI.HelperModel;

namespace YooperDesignUI.HelperClasses
{
    class NumberFileWriter
    {
        public static bool CSVFileWriter(string fileName, NumberFileModel numberFileModel) 
        {
            try
            {
                string notAvilable = "N/A";
                List<NumberModel> numberModels = new List<NumberModel>();

                if (numberFileModel.Number.Count > 0 && !string.IsNullOrWhiteSpace(fileName))
                {
                    

                    for (int i = 0; i < numberFileModel.Number.Count; i++)
                    {
                        NumberModel number = new NumberModel();

                        // Number
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(numberFileModel.Number[i]))
                            {
                                number.Number = numberFileModel.Number[i];
                            }
                            else
                            {
                                number.Number = notAvilable;
                            }
                        }
                        catch (Exception )
                        {
                            number.Number = notAvilable;
                        }

                        // Name
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(numberFileModel.Name[i]))
                            {
                                number.Name = numberFileModel.Name[1];
                            }
                            else
                            {
                                number.Name = notAvilable;
                            }
                        }
                        catch (Exception)
                        {
                            number.Name = notAvilable;
                        }
                        
                        // Exist
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(numberFileModel.Exist[i]))
                            {
                                number.Exist = numberFileModel.Exist[i];
                            }
                            else
                            {
                                number.Exist = notAvilable;
                            }
                        }
                        catch (Exception)
                        {
                            number.Exist = notAvilable;
                        }

                        // Status
                        try
                        {
                            if (!string.IsNullOrWhiteSpace(numberFileModel.Status[i]))
                            {
                                number.Status = numberFileModel.Status[i];
                            }
                            else
                            {
                                number.Status = notAvilable;
                            }
                        }
                        catch (Exception)
                        {
                            number.Status = notAvilable;
                        }

                        numberModels.Add(number);
                    }
                }
                else
                {
                    return false;
                }

                if (numberModels.Count > 0)
                {
                    using (StreamWriter sw = new StreamWriter(new FileStream(fileName, FileMode.Create), Encoding.UTF8))
                    {
                        StringBuilder sp = new StringBuilder();

                        foreach (var item in numberModels)
                        {
                            sp.AppendLine(string.Format("{0},{1},{2},{3}", item.Number, item.Name, item.Exist, item.Status));
                        }

                        sw.Write(sp.ToString());
                    }

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception )
            {
                return false;
            }
        }
    }
}
