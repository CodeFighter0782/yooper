﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperClasses
{
    internal enum ItemsEnum
    {
        IsText = 1,
        IsAttachFile = 2,
        IsDocFile = 3,
        MessageTest = 4,
        ImageFilePath = 5,
        DocFilePath = 6
    }
}
