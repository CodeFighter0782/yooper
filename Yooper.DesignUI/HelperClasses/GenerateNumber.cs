﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YooperDesignUI.HelperClasses
{
    internal class GenerateNumber
    {

        public static List<string> GenerateNumberList(long number, long onwardRange)
        {
            try
            {
                List<string> numberList = new List<string>();

                for (long i = 1; i <= onwardRange; i++)
                {
                    long baseNumber = number + i;

                    string fullNumber = Convert.ToString(baseNumber);

                    numberList.Add(fullNumber);
                }

                return numberList;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //public static List<string> GenerateNumberList(int contryCode, int number, int onwardRange)
        //{
        //    try
        //    {
        //        List<string> numberList = new List<string>();

        //        for (int i = 1; i <= onwardRange; i++)
        //        {
        //            int baseNumber = number + i;

        //            string fullNumber = Convert.ToString(contryCode) + Convert.ToString(baseNumber);

        //            numberList.Add(fullNumber);
        //        }

        //        return numberList;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}
    }
}
