﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;

namespace Yooper.Service.Interface
{
    public interface IAdminService
    {
        void AdminForgetPassword(string email);
        void ChangeAdminPassword(long userId, string newPassword);
        bool VerifyAdminPassword(long userId, string password);
        void SaveUserMaster(long Id, string email, string imageUrl);
        IQueryable<UserMaster> GetUserMaster(long Id);
        IQueryable<UserMaster> GetUserMaster();
        IQueryable<User> Login(string username, string password, string key);
        IQueryable<UserMaster> Login(string username, string password);
        IQueryable<User> GetUsers();
        IQueryable<User> GetUsers(long Id);
        void SaveUser(User u);
        void DeleteUser(long Id);
        void GenerateLicenseKey(long userId, string secretKey, int numberOfLogins, DateTime licenseExpiryDate);
        void RestartUserApplication(long UserId);
        bool CheckEmailAlreadyExist(string Email);
        bool CheckRestart(long userId);
        
    }
}
