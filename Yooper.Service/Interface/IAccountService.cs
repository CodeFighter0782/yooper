﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yopper.DTO;

namespace Yooper.Service.Interface
{
    public interface IAccountService
    {
        void DeleteAllNumbers();
        DashboardDataVM GetDashboardData();
        string GetGroupWhatsAppId(int GroupId);
        void UpdateWhatsAppGroupId(int GroupId, string GroupWhatsAppId);
        List<SqliteMessages> GetCurrentMessage();
        void UpdateMessage(int MessageId, string NewText);

        void UpdateGroupName(int GroupId, string GroupName);
        GroupVM GetGroupById(int GroupId);

        void UnableAllReplyMessages();
        void DeleteAutoReply(int Id);
        void SaveAutoReply(int Id, string AutoReplyMessage, bool MakeAsDefault = true);
        void SetAsAutoReplyMessage(int AutoReplyMessageId);
        List<SqliteAutoReplyMessages> GetAutoReplyMessages();
        void SaveAutoReply(string AutoReplyMessage, bool MakeAsDefault = true);

        void SetNumberValidity(int numberId, bool isValid);
        List<SqliteSendAudits> GetSendAudits();
        void AddSentAudits(SqliteSendAudits obj);
        void AddFile(string url);
        LoginRes Login(string username, string password, string key);
        bool CheckIfApplicationRestart(long userId);
        void EnableRestart(long userId);
        bool CheckRestartStartEnabled(long userId);
        void AddUser(SqliteCustomUsers userObject);

        List<SqliteNumbers> GetNumbers();
        List<SqliteNumbers> GetNumbers(string searchItem);
        void AddNumber(string name, string number);
        void AddNumber(long Id, string name, string number);

        void DeleteNumber(long Id);
        void MakeUserLogin(bool status);
        bool GetIsLogin();
        List<SqliteCustomUsers> GetCustomUser();

        List<SqliteGroups> GetGroups();
        List<SqliteGroups> GetGroups(string searchItem);
        bool IsGroupAvailable(string searchItem);
        void CreateGroup(string groupName);
        GroupVM GetGroupByName(string groupName);
        void DeleteGroup(long id);
        void AddNumberInGroup(int groupId, int numberId);
        void RemoveNumberFromGroup(long groupId, string name, string number);
        List<NumberVM> GetGroupNumbers(long id);

        List<SqliteMessages> GetMessages();
        List<SqliteMessages> GetMultimediaMessages();

        void AddMessage(string message);
        void AddMessage(long Id, string message);
        void DeleteMessage(long MessageId);

        void MakeChangeInChannelStatus(bool status);
        bool CheckChannelStatus();
    }
}
