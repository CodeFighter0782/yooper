﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;

namespace Yooper.Service.Interface
{
    public interface ICampaignAuditService
    {
        void Add(SqliteCampaignAudits model);
        List<SqliteCampaignAudits> Get();
        List<SqliteCampaignAudits> Get(int Id);
        void Update(int Id, SqliteCampaignAudits model);
        void Delete(int Id);
    }
}
