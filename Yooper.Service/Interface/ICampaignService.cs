﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;

namespace Yooper.Service.Interface
{
    public interface ICampaignService
    {
        void Add(SqliteCampaigns model);
        List<SqliteCampaigns> Get();
        List<SqliteCampaigns> Get(int Id);
        void Update(int Id, SqliteCampaigns model);
        void Delete(int Id);
    }
}
