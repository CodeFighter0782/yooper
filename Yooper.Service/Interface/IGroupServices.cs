﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;

namespace Yooper.Service.Interface
{
    public interface IGroupServices
    {
        void DeleteFromGroupNumbers(int GroupId, List<string> numbers);
        void Add(SqliteGroups model);
        List<SqliteGroups> Get();
        List<SqliteGroups> Get(int Id);
        void Update(int Id, SqliteGroups model);
        void Delete(int Id);
    }
}
