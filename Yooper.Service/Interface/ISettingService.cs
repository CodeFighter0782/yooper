﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;

namespace Yooper.Service.Interface
{
    public interface ISettingService
    {
        void Add(SqliteSettings model);
        List<SqliteSettings> Get();
        List<SqliteSettings> Get(int Id);
        void Update(int Id, SqliteSettings model);
        void Delete(int Id);
        void Delete();
    }
}
