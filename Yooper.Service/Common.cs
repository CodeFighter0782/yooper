﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Yooper.Service
{
    internal class Common
    {
        public static string POST(string address, NameValueCollection value)
        {
            try
            {
                using (WebClient wc = new WebClient())
                {
                    byte[] res = wc.UploadValues(address, value);
                    return Encoding.UTF8.GetString(res);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public static string GET(string address)
        {
            using (WebClient wc=new WebClient())
            {
                byte[] res = wc.DownloadData(address);
                return Encoding.UTF8.GetString(res);
            }
        }

        public static string ADMIN_BASEURL()
        {
            return "http://yooper.blockteks.io/";
            //return "http://localhost:6561/";
        }
    }
}
