﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;
using Yopper.Common;

namespace Yooper.Service.Implementation
{
    public class AdminService : IAdminService
    {
        private AdminEntities _db;
        public AdminService(AdminEntities db)
        {
            _db = db;
        }
        public IQueryable<User> Login(string username, string password, string key)
        {
            var obj = _db.Users.Where(x => x.Enable == true && x.Username == username && x.Password == password);
            try
            {
                if (obj!=null && obj.Any())
                {
                    if (!obj.Where(x => x.SecretKey == key).Any())
                    {
                        throw new Exception(ErrorCode.Unable_to_verify_license_key.ToString().Replace('_',' '));
                    }
                    if (obj.Where(x => x.Block == true).Any())
                    {
                        throw new Exception(ErrorCode.Account_has_been_blocked_Please_contact_the_agent_for_more_information.ToString().Replace('_',' '));
                    }
                    if (obj.Select(x => x.Limit).FirstOrDefault().GetValueOrDefault() > 1)
                    {
                        this.DecreaseLoginLimit(obj.Select(x => x.Id).FirstOrDefault());
                    }
                    else
                    {
                        throw new Exception(ErrorCode.Login_Limit_exceed_Please_contact_admin.ToString().Replace('_', ' '));
                    }
                }
                else
                {
                    throw new Exception("Unable to verify username. Please re-try");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
            return obj;
        }

        public void DecreaseLoginLimit(long userId)
        {
            try
            {
                var obj= _db.Users.Where(x => x.Enable == true && x.Id == userId).FirstOrDefault();
                if (obj!=null)
                {
                    obj.Limit = obj.Limit.GetValueOrDefault() - 1;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IQueryable<UserMaster> Login(string username, string password)
        {
            try
            {
                return _db.UserMasters.Where(x => x.Enable == true && x.Email == username && x.Password == password);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<UserMaster> GetUserMaster()
        {
            return _db.UserMasters.Where(x => x.Enable == true);
        }
        public IQueryable<UserMaster> GetUserMaster(long Id)
        {
            return _db.UserMasters.Where(x => x.Enable == true && x.Id==Id);
        }

        public IQueryable<User> GetUsers()
        {
            return _db.Users.Where(x => x.Enable == true);
        }

        public IQueryable<User> GetUsers(long Id)
        {
            return _db.Users.Where(x => x.Enable == true && x.Id == Id);
        }


        public void SaveUserMaster(long Id, string email, string imageUrl)
        {
            try
            {
                var obj= _db.UserMasters.Where(x => x.Enable == true && x.Id == Id).FirstOrDefault();
                if (obj!=null)
                {
                    obj.Email = email;
                    obj.ImageUrl = imageUrl;
                    obj.UpdatedDate = DateTime.UtcNow;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void SaveUser(User u)
        {
            if (u.Id > 0)
            {
                var obj = _db.Users.Where(x => x.Enable == true && x.Id == u.Id).First();
                if (obj != null)
                {
                    obj.Name = u.Name;
                    obj.Username = u.Username;
                    obj.Password = u.Password;
                    obj.Block = u.Block;
                    obj.UpdatedDate = DateTime.UtcNow;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            else
            {
                if (_db.Users.Where(x => x.Enable == true && x.Username == u.Username).Any())
                {
                    YooperException.Throw(ErrorCode.USERNAME_ALREADY_EXIST.ToString());
                }
                _db.Entry(u).State = System.Data.Entity.EntityState.Added;
                _db.Configuration.ValidateOnSaveEnabled = false;
                _db.SaveChanges();
            }
        }

        public bool CheckEmailAlreadyExist(string Email)
        {
            if(_db.Users.Where(x=>x.Enable==true && x.Username== Email).Any())
            {
                return true;
            }
            return false;
        }
        public void GenerateLicenseKey(long userId, string secretKey, int numberOfLogins, DateTime licenseExpiryDate)
        {
            try
            {
                if (_db.Users.Where(x=>x.SecretKey == secretKey).Any())
                {
                    throw new Exception("Lincense key can not be same.");
                }
                var obj = _db.Users.Where(x => x.Enable == true && x.Id == userId).First();
                if (obj != null)
                {
                    obj.Limit = numberOfLogins;
                    obj.SecretKey = secretKey;
                    obj.LicenseIssueDate = DateTime.UtcNow;
                    obj.LicenseExpiryDate = licenseExpiryDate;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
                else
                {
                    YooperException.Throw(ErrorCode.USER_NOT_FOUND.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void RestartUserApplication(long UserId)
        {
            try
            {
                var obj = _db.Users.Where(x => x.Enable == true && x.Id == UserId).First();
                obj.Restart = true;
                _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception ex)
            {

                YooperException.Throw(ErrorCode.OTHER, ex);
            }
        }


        public void DeleteUser(long Id)
        {
            try
            {
                var obj = _db.Users.Where(x => x.Id == Id).First();
                if (obj != null)
                {
                    obj.Enable = false;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
                else
                {
                    YooperException.Throw(ErrorCode.OBJECT_NOT_FOUND.ToString());
                }
            }
            catch (YooperException ex)
            {
                throw ex;
            }
        }



        public bool CheckRestart(long userId)
        {
            try
            {
                bool result = false;
                var res = _db.Users.Where(x => x.Enable == true && x.Block==false && x.Id == userId).FirstOrDefault();
                if (res!=null)
                {
                    if (res.Restart.GetValueOrDefault())
                    {
                        result = true;
                    }
                }
                else
                {
                    result = true;
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _db.Users.Where(x => x.Enable == true && x.Id == userId).First().Restart = false;
                _db.SaveChanges();
            }
            
        }

        public bool VerifyAdminPassword(long userId, string password)
        {
            try
            {
                return _db.UserMasters.Where(x => x.Enable == true && x.Id == userId).Select(x => x.Password).FirstOrDefault() == password ? true : false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ChangeAdminPassword(long userId, string newPassword)
        {
            try
            {
                var obj = _db.UserMasters.Where(x => x.Enable == true && x.Id == userId).FirstOrDefault();
                if (obj!=null)
                {
                    obj.Password = newPassword;
                    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                    _db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AdminForgetPassword(string email)
        {
            try
            {
                var obj= _db.UserMasters.Where(x => x.Enable == true && x.Email == email).FirstOrDefault();
                if (obj!=null)
                {
                    string body = $"<h3>Please enter this link to renew your password</h3><p><a href='http://localhost:6561/Account/RecoverResult?Id={obj.Id}' target='_blank'>click here !!!</a></p>";

                    HelperFunction.sendEmail(obj.Email, "Yooper Password Recovery", body);
                }
                else
                {
                    throw new Exception("User is invalid.");
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
