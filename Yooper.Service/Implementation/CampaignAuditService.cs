﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;
using Yopper.Common;

namespace Yooper.Service.Implementation
{
    public class CampaignAuditService:ICampaignAuditService
    {
        public void Add(SqliteCampaignAudits model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("insert into CampaignAudits(Number,Message,MessageType,Enable,CreatedDate)values(@Number,@Message,@MessageType,@Enable,@CreatedDate)"
                    , new
                    {
                        Number = model.Number,
                        Message = model.Message,
                        MessageType = model.MessageType,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString()
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SqliteCampaignAudits> Get()
        {
            try
            {
                return SqliteDataAccess<SqliteCampaignAudits>.Load("select * from CampaignAudites where Enable =1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SqliteCampaignAudits> Get(int Id)
        {
            try
            {
                return SqliteDataAccess<SqliteCampaignAudits>.Load("select * from CampaignAudites where Id=@Id Enable =1",new {Id=Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(int Id, SqliteCampaignAudits model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update CampaignAudits set Number =@Number, Message = @Message, MessageType=@MessageType, UpdatedDate=@UpdatedDate where Id=@Id",
                    new
                    {
                        Number = model.Number,
                        Message = model.Message,
                        MessageType = model.MessageType,
                        UpdatedDate = DateTime.Now.ToString(),
                        Id = Id
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public void Delete(int Id)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update CampaignAudits set Enable=@Enable where Id=@Id", new
                {
                    Id = Id
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
