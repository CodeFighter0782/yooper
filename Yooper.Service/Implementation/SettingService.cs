﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;

namespace Yooper.Service.Implementation
{
    public class SettingService:ISettingService
    {
        public void Add(SqliteSettings model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("insert into Settings(Delay,Pause,Stop,Enable,CreatedDate)values(@Delay,@Pause,@Stop,@Enable,@CreatedDate)"
                    , new
                    {
                        Delay = model.Delay,
                        Pause = model.Pause,
                        Stop = model.Stop,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString(),
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SqliteSettings> Get()
        {
            try
            {
                return SqliteDataAccess<SqliteSettings>.Load("select * from Settings  where Enable =1 order by CreatedDate desc");
            }
            catch (Exception ex)
            {

                //throw ex;
                return new List<SqliteSettings>();
            }
        }

        public List<SqliteSettings> Get(int Id)
        {
            try
            {
                return SqliteDataAccess<SqliteSettings>.Load("select * from Settings where Id=@Id and Enable =1 order by CreatedDate desc", new { Id = Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(int Id, SqliteSettings model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Settings set Delay=@Delay, Pause= @Pause, Stop=@Stop, UpdatedDate=@UpdatedDate where Id=@Id",
                    new
                    {
                        Delay = model.Delay,
                        Pause = model.Pause,
                        Stop = model.Stop,
                        UpdatedDate = DateTime.Now.ToString(),
                        Id = Id
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete()
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update Settings set Enable=0");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(int Id)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update Settings set Enable=0 where Id=@Id", new
                {
                    Id = Id
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
