﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;
using Yopper.Common;
using Yopper.DTO;

namespace Yooper.Service.Implementation
{
    public class AccountService : IAccountService
    {
        private sbyooperEntities _db;
        public AccountService(sbyooperEntities db)
        {
            _db = db;
        }


        public LoginRes Login(string username, string password, string key)
        {
            var res = new APIResponse<LoginRes>();
            try
            {
                NameValueCollection values = new NameValueCollection();
                values.Add("Username", username);
                values.Add("Password", password);
                values.Add("Key", key);
                res = JsonConvert.DeserializeObject<APIResponse<LoginRes>>(Common.POST(Common.ADMIN_BASEURL() + "api/yooper/login", values));
                if (!res.Success)
                {
                    throw new Exception(res.Exception);
                }

            }
            catch (Exception ex)
            {
                YooperException.Throw(ex.Message);
            }
            return res.Data;
        }


        public void MakeUserLogin(bool status)
        {
            try
            {
                //var obj = _db.CustomUsers.Where(x => x.Enable == true).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.IsLogin = status;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}
                var obj = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers where Enable = 1").FirstOrDefault();
                if (obj != null)
                {
                    SqliteDataAccess<SqliteCustomUsers>.Execute("update CustomUsers set IsLogin= @IsLogin where Id=@Id", new { Id = obj.Id, IsLogin = (status) ? 1 : 0 });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GetIsLogin()
        {
            try
            {
                //return _db.CustomUsers.Where(x => x.Enable == true).OrderByDescending(x => x.CreatedDate).Select(x => x.IsLogin).FirstOrDefault();
                var obj = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers where Enable =1").FirstOrDefault();
                if (obj != null)
                {
                    return obj.IsLogin == 1 ? true : false;
                }
                return false;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool CheckIfApplicationRestart(long userId)
        {
            try
            {
                var res = JsonConvert.DeserializeObject<APIResponse<bool>>(Common.GET(Common.ADMIN_BASEURL() + "api/yooper/checkRestart?userId=" + userId));
                if (res.Success)
                {
                    return res.Data;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }

        public void AddUser(SqliteCustomUsers userObject)
        {
            try
            {
                //_db.CustomUsers.RemoveRange(_db.CustomUsers.ToList());
                //_db.SaveChanges();

                //_db.Entry(userObject).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteDataAccess<SqliteCustomUsers>.Execute("DELETE from CustomUsers");

                SqliteDataAccess<SqliteCustomUsers>.Execute("INSERT INTO CustomUsers(LicenseKey,IsLogin,Enable,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy,TwoWayAuthentication,Restart,Username,UserId) VALUES(@LicenseKey,@IsLogin,@Enable,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy,@TwoWayAuthentication,@Restart,@Username,@UserId)",
new
{
    LicenseKey = userObject.LicenseKey,
    IsLogin = userObject.IsLogin,
    Enable = userObject.Enable,
    CreatedDate = userObject.CreatedDate,
    CreatedBy = userObject.CreatedBy,
    UpdatedDate = userObject.UpdatedDate,
    UpdatedBy = userObject.UpdatedBy,
    TwoWayAuthentication = userObject.TwoWayAuthentication,
    Restart = userObject.Restart,
    Username = userObject.Username,
    UserId = userObject.UserId
});
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EnableRestart(long userId)
        {
            try
            {
                //var obj = _db.CustomUsers.Where(x => x.Enable == true).OrderByDescending(x => x.CreatedDate).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Restart = true;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}

                var obj = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers where Enable=1 ORDER BY CreatedDate DESC").FirstOrDefault();
                if (obj != null)
                {
                    SqliteDataAccess<SqliteCustomUsers>.Execute(@"UPDATE CustomUsers SET Restart= 1");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckRestartStartEnabled(long userId)
        {
            //var obj = _db.CustomUsers.Where(x => x.Enable == true).Select(x => x.Restart).FirstOrDefault().GetValueOrDefault();
            //if (obj)
            //{
            //    _db.CustomUsers.Where(x => x.Enable == true).OrderByDescending(x => x.CreatedDate).FirstOrDefault().Restart = false;
            //    _db.SaveChanges();
            //}

            bool restart = false;
            var obj = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers where Enable=1 ORDER BY CreatedDate DESC").FirstOrDefault();
            if (obj != null)
            {
                restart = obj.Restart == 1 ? true : false;
                if (restart)
                {
                    SqliteDataAccess<SqliteCustomUsers>.Execute("Update CustomUsers set Restart=1");

                }
            }
            return restart;
        }


        public List<SqliteNumbers> GetNumbers()
        {
            //return _db.Numbers.Where(x => x.Enable == true);
            return SqliteDataAccess<SqliteNumbers>.Load("SELECT * from Numbers where Enable=1");
        }


        public List<SqliteNumbers> GetNumbers(string searchItem)
        {
            try
            {
                //var query = _db.Numbers.Where(x => x.Enable == true);
                //if (query.Where(x => x.PhoneNumber.Contains(searchItem)).Any())
                //{
                //    query = query.Where(x => x.PhoneNumber.Contains(searchItem));
                //}
                //else if (query.Where(x => x.Name.Contains(searchItem)).Any())
                //{
                //    query = query.Where(x => x.Name.Contains(searchItem));
                //}

                return SqliteDataAccess<SqliteNumbers>.Load("select * from Numbers where Enable=1 and (Name LIKE '%" + searchItem + "%' or PhoneNumber LIKE '%" + searchItem + "%')");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void UpdateWhatsAppGroupId(int GroupId,string GroupWhatsAppId)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Groups set WhatsAppGroupId= @WhatsAppGroupId where Id= @Id",
                    new
                    {
                        Id = GroupId,
                        WhatsAppGroupId = GroupWhatsAppId
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string GetGroupWhatsAppId(int GroupId)
        {
            try
            {
                return SqliteDataAccess<string>.Load("select WhatsAppGroupId from Groups where Id = @Id", new { Id = GroupId }).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void AddNumber(string name, string number)
        {
            try
            {
                //var obj = new Number()
                //{
                //    Name = name,
                //    PhoneNumber = number,
                //    CreatedDate = DateTime.Now,
                //    Enable = true
                //};
                //_db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteNumbers obj = new SqliteNumbers()
                {
                    Name = name,
                    PhoneNumber = number,
                    Enable = 1,
                    CreatedDate = DateTime.Now.ToString(),
                    CreatedBy = string.Empty,
                    UpdateDate = string.Empty,
                    UpdatedBy = string.Empty
                };

                if (number != null)
                {
                    SqliteDataAccess<dynamic>.Execute(
                                "INSERT INTO Numbers(Name,PhoneNumber,Enable,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) VALUES(@Name,@PhoneNumber,@Enable,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy)",
                                new
                                {
                                    Name = obj.Name,
                                    PhoneNumber = obj.PhoneNumber,
                                    Enable = obj.Enable,
                                    CreatedDate = obj.CreatedDate,
                                    CreatedBy = obj.CreatedBy,
                                    UpdatedDate = obj.UpdateDate,
                                    UpdatedBy = obj.UpdatedBy
                                });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void AddNumber(long Id, string name, string number)
        {
            try
            {
                //var obj = _db.Numbers.Where(x => x.Enable == true && x.Id == Id).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Name = name;
                //    obj.PhoneNumber = number;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}
                //else
                //{
                //    throw new Exception("Number not found.");
                //}
                SqliteDataAccess<dynamic>.Execute($"UPDATE Numbers SET Name=@Name, PhoneNumber=@PhoneNumber, UpdatedDate=@UpdatedDate where Id=@Id",
                    new { Id = (int)Id, Name = name, PhoneNumber = number, UpdatedDate = DateTime.Now.ToString() });

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void DeleteNumber(long Id)
        {
            try
            {
                //var obj = _db.Numbers.Where(x => x.Enable == true && x.Id == Id).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Enable = false;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}
                //else
                //{
                //    throw new Exception("Number not found.");
                //}

                SqliteDataAccess<dynamic>.Execute("update Numbers set Enable = 0 where Id=@Id", new { Id = Id });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteAllNumbers()
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update numbers set Enable= 0");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<SqliteCustomUsers> GetCustomUser()
        {
            //return _db.CustomUsers.Where(x => x.Enable == true).OrderByDescending(x => x.CreatedDate);
            return SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers where Enable=1 order by CreatedDate desc");
        }



        public List<SqliteMessages> GetMessages()
        {
            //return _db.Messages.Where(x => x.Enable == true && x.IsText == true).OrderByDescending(x => x.CreatedDate);
            return SqliteDataAccess<SqliteMessages>.Load("select * from Messages where Enable = 1 and IsText= 1");

        }

        public List<SqliteMessages> GetMultimediaMessages()
        {
            //return _db.Messages.Where(x => x.Enable == true && x.IsText == false).OrderByDescending(x => x.CreatedDate);
            return SqliteDataAccess<SqliteMessages>.Load("select * from Messages where Enable =1 and IsText= 0");
        }


        public void AddMessage(string message)
        {
            try
            {
                //var obj = new Message()
                //{
                //    Text = message,
                //    IsText = true,
                //    IsLocation = false,
                //    IsMultimedia = false,
                //    Enable = true,
                //    CreatedDate = DateTime.Now
                //};
                //_db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteDataAccess<dynamic>.Execute(@"insert into 
Messages(Text,IsText,IsMultimedia,IsLocation,Enable,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy)
values(@Text,@IsText,@IsMultimedia,@IsLocation,@Enable,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy)",
new SqliteMessages()
{
    Text = message,
    IsText = 1,
    IsLocation = 0,
    IsMultimedia = 0,
    Enable = 1,
    CreatedDate = DateTime.Now.ToString(),
    UpdatedBy = string.Empty,
    UpdatedDate = string.Empty,
    CreatedBy = string.Empty
});

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void UpdateMessage(int MessageId, string NewText)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Messages set Text=@Text where Id=@Id", new {Id=MessageId, Text=NewText });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddFile(string url)
        {
            try
            {
                //var obj = new Message()
                //{
                //    Text = message,
                //    IsText = true,
                //    IsLocation = false,
                //    IsMultimedia = false,
                //    Enable = true,
                //    CreatedDate = DateTime.Now
                //};
                //_db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteDataAccess<dynamic>.Execute(@"insert into 
Messages(Text,IsText,IsMultimedia,IsLocation,Enable,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy)
values(@Text,@IsText,@IsMultimedia,@IsLocation,@Enable,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy)",
new SqliteMessages()
{
    Text = url,
    IsText = 0,
    IsLocation = 0,
    IsMultimedia = 1,
    Enable = 1,
    CreatedDate = DateTime.Now.ToString(),
    UpdatedBy = string.Empty,
    UpdatedDate = string.Empty,
    CreatedBy = string.Empty
});

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddMessage(long Id, string message)
        {
            try
            {
                //var obj = _db.Messages.Where(x => x.Enable == true && x.Id == Id).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Text = message;
                //    obj.UpdatedDate = DateTime.Now;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //    _db.SaveChanges();
                //}

                SqliteDataAccess<dynamic>.Execute(@"update Messages set Text=@Text, UpdatedDate=@UpdatedDate where Id=@Id",
                    new SqliteMessages()
                    {
                        Id = (int)Id,
                        Text = message,
                        UpdatedDate = DateTime.Now.ToString()
                    });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void DeleteMessage(long MessageId)
        {
            try
            {
                //var obj = _db.Messages.Where(x => x.Enable == true && x.Id == MessageId).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Enable = false;
                //    obj.UpdatedDate = DateTime.Now;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}

                SqliteDataAccess<dynamic>.Execute(@"update Messages set Enable = 0 where Id=@Id", new { Id = (int)MessageId });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public List<SqliteMessages> GetCurrentMessage()
        {
            try
            {
                return SqliteDataAccess<SqliteMessages>.Load("select * from Messages where Enable= 1 order by CreatedDate desc");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SqliteGroups> GetGroups()
        {
            //return _db.Groups.Where(w => w.Enable == true);
            return SqliteDataAccess<SqliteGroups>.Load("select * from Groups where Enable =1");
        }

        public List<SqliteGroups> GetGroups(string searchItem)
        {
            try
            {
                // it will query on GroupName only
                //IQueryable<Group> query = _db.Groups.Where(x => x.Enable == true);
                //if (query.Where(x => x.GroupName.Contains(searchItem)).Any())
                //{
                //    query = query.Where(x => x.GroupName.Contains(searchItem));
                //}
                //return query;
                return SqliteDataAccess<SqliteGroups>.Load("select * from Groups where Enable=1 and (GroupName LIKE '%" + searchItem + "%')");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsGroupAvailable(string searchItem)
        {
            try
            {
                //return _db.Groups.Where(w => w.Enable == true && w.GroupName == searchItem).Any();

                var res = SqliteDataAccess<SqliteGroups>.Load("select * from Groups where Enable=1 and GroupName=@GroupName", new { GroupName = searchItem });
                return (res.Count > 0) ? true : false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void CreateGroup(string groupName)
        {
            try
            {
                //var obj = new Group()
                //{
                //    GroupName = groupName,
                //    CreatedDate = DateTime.Now,
                //    Enable = true
                //};
                //_db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteDataAccess<dynamic>.Execute(
                    "insert into Groups(GroupName,Enable,CreatedDate,CreatedBy,UpdatedDate,UpdatedBy) values(@GroupName,@Enable,@CreatedDate,@CreatedBy,@UpdatedDate,@UpdatedBy)",
                    new
                    {
                        GroupName = groupName,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString(),
                        CreatedBy = string.Empty,
                        UpdatedDate = string.Empty,
                        UpdatedBy = string.Empty
                    });

                // var createdGroup = _db.Groups.Where(w => w.GroupName == groupName && w.Enable == true).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteGroup(long id)
        {
            try
            {
                //var obj = _db.Groups.Where(w => w.Id == id && w.Enable == true).FirstOrDefault();

                //if (obj != null)
                //{
                //    obj.Enable = false;
                //    obj.UpdatedDate = DateTime.Now;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}

                SqliteDataAccess<dynamic>.Execute("Update Groups set Enable=0, UpdatedDate=@UpdatedDate", new { UpdatedDate = DateTime.Now.ToString() });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddNumberInGroup(int groupId, int numberId)
        {
            try
            {
                //var obj = new GroupNumber()
                //{
                //    GroupId = groupId,
                //    NumberId = numberId,
                //    CreatedDate = DateTime.Now,
                //    Enable = true
                //};

                //_db.Entry(obj).State = System.Data.Entity.EntityState.Added;
                //_db.SaveChanges();

                SqliteDataAccess<dynamic>.Execute("insert into GroupNumbers(GroupId,NumberId,Enable,CreatedDate) values(@GroupId,@NumberId,@Enable,@CreatedDate)",
                    new
                    {
                        GroupId = groupId,
                        NumberId = numberId,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString()
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveNumberFromGroup(long groupId, string name, string number)

        {
            try
            {
                //var obj = _db.GroupNumbers.Where(
                //    w => w.Enable == true && w.GroupId == groupId && w.Number.Name == name && w.Number.PhoneNumber == number).FirstOrDefault();
                //if (obj != null)
                //{
                //    obj.Enable = false;
                //    obj.UpdatedDate = DateTime.Now;
                //    _db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                //    _db.SaveChanges();
                //}

                //SqliteDataAccess<dynamic>.Execute(
                //    "update GN set GN.Enable=0, GN.UpdatedDate=@UpdatedDate FROM GroupNumbers GN left outer join Numbers N on N.Id= GN.NumberId where GN.Enable=1 and GN.GroupId=@GroupId and N.Name= @Name and N.PhoneNumber=@PhoneNumber",
                //    new
                //    {
                //        UpdatedDate = DateTime.Now.ToString(),
                //        GroupId = (int)groupId,
                //        Name = name,
                //        PhoneNumber = number
                //    });

                SqliteDataAccess<dynamic>.Execute("Update GroupNumbers set Enable=0 where Id=@Id", new { Id = groupId });

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public List<NumberVM> GetGroupNumbers(long id)
        {
            //return _db.GroupNumbers.Where(w => w.GroupId == id && w.Number.Enable == true && w.Enable == true);

            return SqliteDataAccess<NumberVM>.Load("select GN.Id, N.Name, N.PhoneNumber as Number, N.CreatedDate from GroupNumbers GN left outer join Numbers N on N.Id= GN.NumberId left outer join Groups G on G.Id= GN.GroupId where GN.Enable=1 and GN.GroupId=@GroupId and N.Enable=1",
                new
                {
                    GroupId = (int)id
                });



        }


        public GroupVM GetGroupByName(string groupName)
        {
            //var createdGroup = _db.Groups.Where(w => w.GroupName == groupName && w.Enable == true).FirstOrDefault();

            //return new GroupVM()
            //{
            //    ID = createdGroup.Id,
            //    Name = createdGroup.GroupName,
            //    CreateDate = createdGroup.CreatedDate.ToString()
            //};

            return SqliteDataAccess<GroupVM>.Load("select Id as ID,WhatsAppGroupId, GroupName as Name, CreatedDate from Groups where Enable=1 and GroupName=@GroupName",
                new { GroupName = groupName }).FirstOrDefault();

        }

        public GroupVM GetGroupById(int GroupId)
        {
            //var createdGroup = _db.Groups.Where(w => w.GroupName == groupName && w.Enable == true).FirstOrDefault();

            //return new GroupVM()
            //{
            //    ID = createdGroup.Id,
            //    Name = createdGroup.GroupName,
            //    CreateDate = createdGroup.CreatedDate.ToString()
            //};

            return SqliteDataAccess<GroupVM>.Load("select Id as ID, GroupName as Name, CreatedDate from Groups where Enable=1 and Id=@Id",
                new { Id = GroupId }).FirstOrDefault();

        }




        public void MakeChangeInChannelStatus(bool status)
        {
            try
            {
                var customUsers = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers order by CreatedDate desc").FirstOrDefault();
                if (customUsers != null)
                {
                    SqliteDataAccess<dynamic>.Execute("update CustomUsers set IsChannelConnected=@IsChannelConnected where Id=@Id", new { Id = customUsers.Id, IsChannelConnected = (status) ? 1 : 0 });
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool CheckChannelStatus()
        {
            var obj = SqliteDataAccess<SqliteCustomUsers>.Load("select * from CustomUsers order by CreatedDate desc").FirstOrDefault();
            if (obj != null)
            {
                return obj.IsChannelConnected == 1 ? true : false;
            }
            return false;
        }


        public void AddSentAudits(SqliteSendAudits obj)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute(
                    "insert into SendAudits(NumberId,IsCompleted,Enable,CreatedDate) values(@NumberId,@IsCompleted,@Enable,@CreatedDate)",
                    new {
                        NumberId= obj.NumberId,
                        IsCompleted= obj.IsCompleted,
                        Enable= obj.Enable,
                        CreatedDate= obj.CreatedDate
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SqliteSendAudits> GetSendAudits()
        {
            try
            {
                return SqliteDataAccess<SqliteSendAudits>.Load("select * from SendAudits where Enable=1");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetNumberValidity(int numberId, bool isValid)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Numbers set IsValidNumber=@IsValidNumber where Id=@Id",new {Id=numberId, IsValidNumber = isValid?1:0 });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void SaveAutoReply(string AutoReplyMessage, bool MakeAsDefault =true)
        {
            try
            {
                if (MakeAsDefault)
                {
                    SqliteDataAccess<dynamic>.Execute("Update AutoReplyMessages set SetAsReply=@SetAsReply where Enable=1", new {SetAsReply= 0 });
                    this.SaveAutoReplyToDatabase(AutoReplyMessage, MakeAsDefault);
                }
                else
                {
                    this.SaveAutoReplyToDatabase(AutoReplyMessage, MakeAsDefault);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveAutoReply(int Id,string AutoReplyMessage, bool MakeAsDefault = true)
        {
            try
            {

                SqliteDataAccess<dynamic>.Execute("Update AutoReplyMessages set SetAsReply=@SetAsReply where Enable=1", new { SetAsReply = 0 });
                SqliteDataAccess<dynamic>.Execute(
                    "Update AutoReplyMessages set SetAsReply=@SetAsReply,Message=@Message, UpdatedDate=@UpdatedDate where Enable=1 and Id=@Id",
                    new { SetAsReply = MakeAsDefault ? 1 : 0, Message = AutoReplyMessage, Id = Id, UpdatedDate = DateTime.Now.ToString()  });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteAutoReply(int Id)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update AutoReplyMessages set Enable=0 where Enable=1 and Id=@Id", new { Id = Id });
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SaveAutoReplyToDatabase(string AutoReplyMessage, bool MakeAsDefault)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute(
                    "insert into AutoReplyMessages(Message,SetAsReply,Enable,CreatedDate) values(@Message,@SetAsReply,@Enable,@CreatedDate)",
                    new { Message = AutoReplyMessage, SetAsReply = MakeAsDefault?1:0, Enable = 1, CreatedDate = DateTime.Now.ToString() });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetAsAutoReplyMessage(int AutoReplyMessageId)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update AutoReplyMessages set SetAsReply=@SetAsReply where Enable=1", new { SetAsReply = 0 });
                SqliteDataAccess<dynamic>.Execute("Update AutoReplyMessages set SetAsReply=@SetAsReply where Enable=1 and Id=@Id", new { SetAsReply = 1, Id=AutoReplyMessageId });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SqliteAutoReplyMessages> GetAutoReplyMessages()
        {
            return SqliteDataAccess<SqliteAutoReplyMessages>.Load("select * from AutoReplyMessages where Enable=1");
        }

        public void UnableAllReplyMessages()
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update AutoReplyMessages set SetAsReply=0 where Enable=1");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public void UpdateGroupName(int GroupId, string GroupName)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Groups set GroupName=@GroupName where Id=@Id", new { Id = GroupId, GroupName = GroupName });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public DashboardDataVM GetDashboardData()
        {
            try
            {
                int Message = SqliteDataAccess<int>.Load("select COUNT(Id) from SendAudits where Enable = 1").FirstOrDefault();
                int Numbers = SqliteDataAccess<int>.Load("select COUNT(Id) from Numbers where Enable = 1").FirstOrDefault();
                int Groups = SqliteDataAccess<int>.Load("select COUNT(Id) from Groups where Enable = 1").FirstOrDefault();
                int Channel = 0;

                return new DashboardDataVM() {Numbers = Numbers, Messages = Message, Channels = Channel, Groups = Groups };
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        

    }
}
