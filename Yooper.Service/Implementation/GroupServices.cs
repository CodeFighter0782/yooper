﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;

namespace Yooper.Service.Implementation
{
    public class GroupServices:IGroupServices
    {
        public void Add(SqliteGroups model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("insert into Groups(GroupName,WhatsAppGroupId,Enable,CreatedDate)values(@GroupName,@WhatsAppGroupId,@Enable,@CreatedDate)"
                    , new
                    {
                        GroupName = model.GroupName,
                        WhatsAppGroupId = model.WhatsAppGroupId,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString()
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<SqliteGroups> Get()
        {
            try
            {
                return SqliteDataAccess<SqliteGroups>.Load("select * from Groups where Enable =1");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<SqliteGroups> Get(int Id)
        {
            try
            {
                return SqliteDataAccess<SqliteGroups>.Load("select * from Groups where Id=@Id and Enable =1", new { Id = Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(int Id, SqliteGroups model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Groups set GroupName=@GroupName, WhatsAppGroupId=@WhatsAppGroupId, UpdatedDate=@UpdatedDate where Id=@Id",
                    new
                    {
                        GroupName = model.GroupName,
                        WhatsAppGroupId = model.WhatsAppGroupId,
                        UpdatedDate = DateTime.Now.ToString(),
                        Id = Id
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(int Id)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update Groups set Enable=@Enable where Id=@Id", new
                {
                    Id = Id,
                    Enable = 0
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public void DeleteFromGroupNumbers(int GroupId, List<string> numbers)
        {
            try
            {
                List<int> numberIds = new List<int>();
                foreach (var item in numbers)
                {
                    int val = SqliteDataAccess<int>.Load("select Id from Numbers where Enable =1 and PhoneNumber=@PhoneNumber",
                    new { PhoneNumber = item }).FirstOrDefault();
                    numberIds.Add(val);
                }

                foreach (var item in numberIds)
                {
                    SqliteDataAccess<dynamic>.Execute("delete from GroupNumbers where GroupId=@GroupId and NumberId=@NumberId",
                        new {GroupId = GroupId, NumberId=item });
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
