﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.DTO;
using Yooper.EF;
using Yooper.Service.Interface;

namespace Yooper.Service.Implementation
{
    public class CampaignService : ICampaignService
    {
        public void Add(SqliteCampaigns model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("insert into Campaigns(Name,Numbers,Messages,Urls,Enable,CreatedDate,Immediate,CampaignTimer)values(@Name,@Numbers,@Messages,@Urls,@Enable,@CreatedDate,@Immediate,@CampaignTimer)"
                    , new
                    {
                        Numbers = model.Numbers,
                        Messages = model.Messages,
                        Urls = model.Urls,
                        Name = model.Name,
                        Enable = 1,
                        CreatedDate = DateTime.Now.ToString(),
                        Immediate= model.Immediate,
                        CampaignTimer =model.CampaignTimer
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<SqliteCampaigns> Get()
        {
            try
            {
                return SqliteDataAccess<SqliteCampaigns>.Load("select * from Campaigns where Enable =1");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<SqliteCampaigns> Get(int Id)
        {
            try
            {
                return SqliteDataAccess<SqliteCampaigns>.Load("select * from Campaigns where Id=@Id and Enable =1", new { Id = Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(int Id, SqliteCampaigns model)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("update Campaigns set Name=@Name, Numbers=@Numbers,Urls=@Urls, Messages=@Messages, Immediate=@Immediate, CampaignTimer=@CampaignTimer ,UpdatedDate=@UpdatedDate where Id=@Id",
                    new
                    {
                        Numbers = model.Numbers,
                        Messages = model.Messages,
                        Urls = model.Urls,
                        Name = model.Name,
                        UpdatedDate = DateTime.Now.ToString(),
                        Id = Id,
                        Immediate = model.Immediate,
                        CampaignTimer = model.CampaignTimer
                    });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Delete(int Id)
        {
            try
            {
                SqliteDataAccess<dynamic>.Execute("Update Campaigns set Enable=@Enable where Id=@Id", new
                {
                    Id = Id,
                    Enable = 0
                });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
