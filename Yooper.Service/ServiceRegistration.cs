﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.EF;
using Yooper.Service.Implementation;
using Yooper.Service.Interface;

namespace Yooper.Service
{
    public static  class ServiceRegistration
    {
        public static IKernel GlobalKernel { get; private set; }
        public static void BindAll(IKernel kernel)
        {
            kernel.Bind<IAccountService>().To<AccountService>();
            kernel.Bind<IAdminService>().To<AdminService>();
            kernel.Bind<ICommonService>().To<CommonService>();
            kernel.Bind<ICampaignAuditService>().To<CampaignAuditService>();
            kernel.Bind<ISettingService>().To<SettingService>();
            kernel.Bind<ICampaignService>().To<CampaignService>();
            kernel.Bind<IGroupServices>().To<GroupServices>();
            kernel.Bind<AdminEntities>().ToSelf();
            GlobalKernel = kernel;
            
        }
    }
}
