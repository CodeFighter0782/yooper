﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopper.Common
{
    public class APIResponse<T>
    {
        public string Exception { get; set; }
        public bool Success { get; set; }
        public T Data { get; set; }

    }
}
