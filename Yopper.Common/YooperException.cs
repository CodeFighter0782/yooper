﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopper.Common
{
    public class YooperException: Exception
    {
        public ErrorCode ErrorCode { get; private set; }
        public string ErrorMessage { get;private set; }
        public YooperException(ErrorCode code, Exception innerException)
            : base(code.ToString(), innerException)
        {
            ErrorCode = code;
            ErrorMessage = code.ToString().Replace("_", " ");
        }
        public YooperException(string message)
        {
            ErrorMessage = message;
        }

        public static void Throw(ErrorCode code, Exception innerException)
        {
            throw new YooperException(code, innerException);
        }
        public static void Throw(string errorMessage)
        {
            throw new YooperException(errorMessage);
        }
    }
}
