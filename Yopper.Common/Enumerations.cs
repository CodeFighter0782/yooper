﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopper.Common
{
    public enum ErrorCode
    {
        USERNAME_PASSWORD_INCORRECT=101,
        Unable_to_verify_license_key=102,
        Account_has_been_blocked_Please_contact_the_agent_for_more_information=103,
        OTHER=104,
        OBJECT_NOT_FOUND=105,
        USER_NOT_FOUND=106,
        USERNAME_ALREADY_EXIST=107,
        Login_Limit_exceed_Please_contact_admin=108
    }

    public enum ERole
    {
        ADMIN=1
    }

    public enum EMessageType
    {
        INDIVIDUAL,
        GROUP
    }
}
