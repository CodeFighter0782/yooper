USE [sd_Yooper]
GO
/****** Object:  Table [dbo].[LoginAudits]    Script Date: 2/12/2019 12:01:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginAudits](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[User_Id] [bigint] NOT NULL,
	[MacAddress] [nvarchar](max) NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoginAudits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 2/12/2019 12:01:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](max) NOT NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserMasters]    Script Date: 2/12/2019 12:01:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserMasters](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Role_Id] [bigint] NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2/12/2019 12:01:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Username] [nvarchar](max) NOT NULL,
	[PasswordHash] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[SecretKey] [nvarchar](max) NULL,
	[Limit] [int] NULL,
	[Restart] [bit] NULL,
	[Block] [bit] NOT NULL,
	[LicenseIssueDate] [datetime] NULL,
	[LicenseExpiryDate] [datetime] NULL,
	[Enable] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [RoleName], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Admin', 1, CAST(N'2019-01-31T14:44:28.273' AS DateTime), N'SYSTEM', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[UserMasters] ON 

INSERT [dbo].[UserMasters] ([Id], [Role_Id], [Email], [PasswordHash], [Password], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 1, N'admin@yooper.com', N'admin', N'admin', 1, CAST(N'2019-02-01T12:42:42.220' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserMasters] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Hassam', N'hassam207@hotmail.com', N'abc123456', N'12345', N'fbe9e274-6671-45c7-99f7-200af48cc8e8', 15, 1, 0, CAST(N'2019-02-06T12:57:34.817' AS DateTime), CAST(N'2019-01-29T10:33:05.000' AS DateTime), 1, CAST(N'2019-01-29T10:33:05.053' AS DateTime), NULL, CAST(N'2019-02-01T12:19:58.213' AS DateTime), NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'zohaib', N'zohaib@yooper.com', N'', N'12345', N'c81f1663-fc80-430d-ab98-cacb369c3320', 8, 1, 0, CAST(N'2019-02-06T12:45:03.897' AS DateTime), CAST(N'2020-01-01T00:00:00.000' AS DateTime), 1, CAST(N'2019-02-01T11:23:45.100' AS DateTime), N'SYSTEM', CAST(N'2019-02-01T11:44:43.730' AS DateTime), NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, N'ihtehsam', N'ihtasham@yooper.com', N'', N'12345', N'46732', NULL, 0, 0, CAST(N'2019-02-01T16:29:49.330' AS DateTime), CAST(N'2019-02-01T00:00:00.000' AS DateTime), 0, CAST(N'2019-02-01T11:29:49.330' AS DateTime), N'SYSTEM', CAST(N'2019-02-01T11:33:32.070' AS DateTime), NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, N'Shavaiz', N'shavaiz@yooper.com', N'', N'abc123', N'8dee63df-7e74-474f-b94a-d0ec57c710e6', 2, 0, 0, CAST(N'2019-02-06T12:50:48.627' AS DateTime), CAST(N'2020-06-01T00:00:00.000' AS DateTime), 0, CAST(N'2019-02-04T06:43:52.950' AS DateTime), N'admin@yooper.com', NULL, NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, N'Ahmad', N'ahmad@yooper.com', N'', N'abc123456', N'287778f3-e57e-4065-b526-b7cf562c0851', 10, 0, 0, CAST(N'2019-02-06T12:00:45.673' AS DateTime), CAST(N'2021-01-01T00:00:00.000' AS DateTime), 0, CAST(N'2019-02-04T06:44:48.927' AS DateTime), N'admin@yooper.com', CAST(N'2019-02-06T07:28:04.757' AS DateTime), NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10006, N'Bilal', N'bilal@yooper.com', N'', N'12345', N'd3ea7d5d-5ad7-4ba6-bbc1-e3e23974e6a4', 5, 1, 0, CAST(N'2019-02-06T11:47:13.847' AS DateTime), CAST(N'2025-09-16T00:00:00.000' AS DateTime), 1, CAST(N'2019-02-06T11:29:03.967' AS DateTime), N'admin@yooper.com', NULL, NULL)
INSERT [dbo].[Users] ([Id], [Name], [Username], [PasswordHash], [Password], [SecretKey], [Limit], [Restart], [Block], [LicenseIssueDate], [LicenseExpiryDate], [Enable], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10007, N'ahmad', N'ahmad@yooper.com', N'', N'abc123456', N'61a5dcab-590b-423e-b127-771fe2e7168e', 15, 1, 0, CAST(N'2019-02-08T13:57:28.497' AS DateTime), CAST(N'2020-03-01T00:00:00.000' AS DateTime), 1, CAST(N'2019-02-06T14:38:39.847' AS DateTime), N'admin@yooper.com', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[LoginAudits] ADD  CONSTRAINT [DF_LoginAudits_Enable]  DEFAULT ((1)) FOR [Enable]
GO
ALTER TABLE [dbo].[LoginAudits] ADD  CONSTRAINT [DF_LoginAudits_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_Enable]  DEFAULT ((1)) FOR [Enable]
GO
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[UserMasters] ADD  CONSTRAINT [DF_UserMaster_Enable]  DEFAULT ((1)) FOR [Enable]
GO
ALTER TABLE [dbo].[UserMasters] ADD  CONSTRAINT [DF_UserMaster_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Restart]  DEFAULT ((0)) FOR [Restart]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Block]  DEFAULT ((0)) FOR [Block]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_LicenseIssueDate]  DEFAULT (getutcdate()) FOR [LicenseIssueDate]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_LicenseExpiryDate]  DEFAULT (getutcdate()) FOR [LicenseExpiryDate]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_Enable]  DEFAULT ((1)) FOR [Enable]
GO
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreatedDate]  DEFAULT (getutcdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[LoginAudits]  WITH CHECK ADD  CONSTRAINT [FK_LoginAudits_Users] FOREIGN KEY([Id])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[LoginAudits] CHECK CONSTRAINT [FK_LoginAudits_Users]
GO
ALTER TABLE [dbo].[UserMasters]  WITH CHECK ADD  CONSTRAINT [FK_UserMasters_Roles] FOREIGN KEY([Role_Id])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserMasters] CHECK CONSTRAINT [FK_UserMasters_Roles]
GO
