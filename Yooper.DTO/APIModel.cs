﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yopper.DTO
{
    public class LoginReq
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Key { get; set; }
    }
    public class LoginRes
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string SecretKey { get; set; }
        public bool Block { get; set; }
        public Nullable<System.DateTime> LicenseIssueDate { get; set; }
        public Nullable<System.DateTime> LicenseExpiryDate { get; set; }
    }

    

}
