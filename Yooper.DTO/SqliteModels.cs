﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yooper.DTO
{
    public class SqliteChannels
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
    public class SqliteCustomUsers
    {
        public int Id { get; set; }
        public string LicenseKey { get; set; }
        public int IsLogin { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int TwoWayAuthentication { get; set; }
        public int Restart { get; set; }
        public string Username { get; set; }
        public int UserId { get; set; }
        public int IsChannelConnected { get; set; }
    }

    public class SqliteGroups
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string WhatsAppGroupId { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class SqliteNumbers
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdateDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class SqliteGroupNumbers
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int NumberId { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class SqliteMessages
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int IsText { get; set; }
        public int IsMultimedia { get; set; }
        public int IsLocation { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }


    public class SqliteSendAudits
    {
        public int Id { get; set; }
        public int NumberId { get; set; }
        public int IsCompleted { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }


    public class SqliteAutoReplyMessages
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public int SetAsReply { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }


    public class SqliteCampaignAudits
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class SqliteCampaigns
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Messages { get; set; }
        public string Urls { get; set; }
        public string Numbers { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int Immediate { get; set; }
        public string CampaignTimer { get; set; }
    }

    public class SqliteSettings
    {
        public int Id { get; set; }
        public int Delay { get; set; }
        public int Pause { get; set; }
        public int Stop { get; set; }
        public int Enable { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }

    public class SqliteYooperSettings
    {
        public int Id { get; set; }
        public int DelayEveryMessageMin { get; set; }
        public int DelayEveryMessageMax { get; set; }
        public int LongDelayAfterMessageSentCountMin { get; set; }
        public int LongDelayAfterMessageSentCountMax { get; set; }
        public int LongDelayAfterMessageSentWaitMin { get; set; }
        public int LongDelayAfterMessageSentWaitMax { get; set; }
        public int FriendlyMessageSentCountMin { get; set; }
        public int FriendlyMessageSentCountMax { get; set; }
        public int FriendlyMessageSentAfterMin { get; set; }
        public int FriendlyMessageSentAfterMax { get; set; }
        public int MemberOfGroupMin { get; set; }
        public int MemberOfGroupMax { get; set; }
        public int IsGroupExist { get; set; }
        public int ForwardToMin { get; set; }
        public int ForwardToMax { get; set; }
        public int SendTextFirst { get; set; }
        public int AddDateTime { get; set; }
        public int AddRandomCharacter { get; set; }
        public int CheckTimeNet { get; set; }
        public int WaitForMessageSent { get; set; }
        public int UploadImageOrVideoCount { get; set; }

    }
}
