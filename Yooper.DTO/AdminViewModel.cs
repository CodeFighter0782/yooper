﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Yooper.DTO
{
    public class LoginPageVM
    {
        public LoginVM Login { get; set; }
        public ForgetPasswordVM ForgetPassword { get; set; }
    }

    public class ForgetPasswordVM
    {
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Email is not in a correct format.")]
        public string Email { get; set; }
    }
    public class LoginVM
    {
        [Required(ErrorMessage = "Email address is required.")]
        [EmailAddress(ErrorMessage = "Email is not in a correct format.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required.")]
        [MinLength(5, ErrorMessage = "Password must be 5 digit long.")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }

    public class ActionUserVM
    {
        public long Id { get; set; }
        [Required(ErrorMessage ="Name is required.")]
        public string Name { get; set; }
        [Required(ErrorMessage ="Username is required.")]
        public string Username { get; set; }
        [Required(ErrorMessage ="Password is requried.")]
        [MinLength(5,ErrorMessage ="Password must be atleast 5 digit long.")]
        public string Password { get; set; }
        [Required(ErrorMessage ="Confirm password is required.")]
        [Compare("Password",ErrorMessage ="Confirm password did not match password. Please try again.")]
        public string ConfirmPassword { get; set; }
        public string SecretKey { get; set; }
        public bool Block { get; set; }
        public Nullable<System.DateTime> LicenseIssueDate { get; set; }
        public Nullable<System.DateTime> LicenseExpiryDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }


    public class GenerateLicenseVM
    {
        [Required(ErrorMessage ="Please select user to generate license.")]
        public long UserId { get; set; }
        [Required(ErrorMessage ="Secret key is required.")]
        public string SecretKey { get; set; }
        [Required(ErrorMessage ="Give count for how many time a user can login.")]
        public int? NumberOfLogin { get; set; }
        [Required(ErrorMessage ="Please mention the license expiry date.")]
        public DateTime? LicenseExpiryDate { get; set; }
    }

    public class NumberStatusVM
    {
        public string Number { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
    }

    public class ProfileVM
    {
        public long Id { get; set; }
        [Required]
        public string Email { get; set; }
        public string ImageUrl { get; set; } = "";
        public HttpPostedFileBase Photo { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }


    public class ChangePasswordVM
    {
        public long Id { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }
    }

    public class ForgetPasswordClick
    {
        public string Email { get; set; }

    }

    

}
