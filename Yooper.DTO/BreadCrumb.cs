﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yooper.DTO
{
    public class BreadCrumb
    {
        public string Link { get; set; }
        public string Title { get; set; }
    }
}
