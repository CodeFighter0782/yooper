﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yooper.DTO
{
    public class NumberVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public string CreatedDate { get; set; }
    }

    public class MessagesVM
    {
        public long Id { get; set; }
        public string Text { get; set; }
        public string CreatedDate { get; set; } 
    }

    public class GroupVM
    {
        public int ID { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string WhatsAppGroupId { get; set; }
        public string CreateDate { get; set; }

    }

    public class FileVM
    {
        public string Url { get; set; }
        public string CreatedDate { get; set; }
    }

    public class AutoReplyVM
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public string CreatedDate { get; set; }
        public string SetAsDefault { get; set; }
    }

    public class DashboardDataVM
    {
        public int Channels { get; set; }
        public int Numbers { get; set; }
        public int Messages { get; set; }
        public int Groups { get; set; }
    }
}
