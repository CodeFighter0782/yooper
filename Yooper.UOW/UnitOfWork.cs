﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.Service.Interface;
using static Yooper.UOW.UOWRegistration;

namespace Yooper.UOW
{
    public class UnitOfWork: IUnitofWork
    {
        public IAccountService Account => Globalkernel.Get<IAccountService>();

        public IAdminService Admin => Globalkernel.Get<IAdminService>();
        public ICommonService Common => Globalkernel.Get<ICommonService>();
        public ICampaignAuditService CampaignAudit => Globalkernel.Get<ICampaignAuditService>();
        public ISettingService Setting => Globalkernel.Get<ISettingService>();
        public ICampaignService Campaign => Globalkernel.Get<ICampaignService>();
        public IGroupServices Group => Globalkernel.Get<IGroupServices>();
    }
}
