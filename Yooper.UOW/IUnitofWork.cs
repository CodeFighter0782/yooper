﻿using Yooper.Service.Interface;

namespace Yooper.UOW
{
    public interface IUnitofWork
    {
        IAccountService Account { get; }
        IAdminService Admin { get; }
        ICommonService Common { get; }
        ICampaignAuditService CampaignAudit { get; }
        ISettingService Setting { get; }
        ICampaignService Campaign{ get; }
        IGroupServices Group{ get; }
    }
}