﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yooper.Service;

namespace Yooper.UOW
{
    public static class UOWRegistration
    {
        internal static IKernel Globalkernel { get; private set; }
        public static void BindAll(IKernel kernel)
        {
            kernel.Bind<IUnitofWork>().To<UnitOfWork>();
            ServiceRegistration.BindAll(kernel);
            Globalkernel = kernel;
        }
    }
}
