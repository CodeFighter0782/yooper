﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yooper.EF
{
    public class SqliteDataAccess<T>
    {
        public static List<T> Load(string query)
        {
            using (IDbConnection db = new SQLiteConnection(LoadConnectionString()))
            {
                var output = db.Query<T>(query, new DynamicParameters());
                return output.ToList();
            }
        }

        public static List<T> Load(string query, object obj)
        {
            using (IDbConnection db = new SQLiteConnection(LoadConnectionString()))
            {
                var output = db.Query<T>(query, obj);
                return output.ToList();
            }
        }


        public static void Execute(string query, object obj)
        {
            using (IDbConnection db= new SQLiteConnection(LoadConnectionString()))
            {
                db.Execute(query, obj);
            }
        }

        public static void Execute(string query)
        {
            using (IDbConnection db = new SQLiteConnection(LoadConnectionString()))
            {
                db.Execute(query, new DynamicParameters());
            }
        }


        private static string LoadConnectionString(string id = "Default")
        {
            string connectionString = ConfigurationManager.ConnectionStrings[id].ConnectionString;
            return connectionString;
        }
    }
}
