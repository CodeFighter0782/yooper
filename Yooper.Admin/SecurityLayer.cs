﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Yooper.Admin
{
    public class SecurityLayer : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            ViewResult view = new ViewResult();

            if (HttpContext.Current.Session["user"] == null)
            {
                view.ViewName = "~/Views/Account/Login.cshtml";
                filterContext.Result = view;
            }
        }
    }
}