[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Yooper.Admin.MVCGridConfig), "RegisterGrids")]

namespace Yooper.Admin
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Linq;
    using System.Collections.Generic;

    using MVCGrid.Models;
    using MVCGrid.Web;
    using Yooper.DTO;
    using Yooper.UOW;
    using Yooper.Admin.App_Start;
    using Ninject;

    public static class MVCGridConfig 
    {
        public static void RegisterGrids()
        {

           //START USER GRID
           // MVCGridDefinitionTable.Add("UserGrid", new MVCGridBuilder<User>()
           //    .WithAuthorizationType(AuthorizationType.AllowAnonymous)
           //    .AddColumns(cols =>
           //    {
           //        // Add your columns here
           //        cols.Add()
           //           .WithHeaderText("Name")
           //           .WithValueExpression(i => i.Name); // use the Value Expression to return the cell text for this column
           //        cols.Add()
           //           .WithHeaderText("Username")
           //           .WithValueExpression(i => i.Username);
           //        cols.Add()
           //        .WithHeaderText("Password")
           //        .WithValueExpression(i => i.Password);
           //        cols.Add().WithHtmlEncoding(true)
           //        .WithSorting(false)
           //        .WithHeaderText("Block")
           //        .WithValueExpression((p, c) => !p.Block ? "label label-primary" : "label label-danger")
           //        .WithValueTemplate("<label class='{Value}>{Model.Block}</label>'");
           //        cols.Add().WithHtmlEncoding(true)
           //        .WithHeaderText(" ")
           //        .WithValueExpression((p, c) => c.UrlHelper.Action("ActionUser", "User", new { Id = p.Id }))
           //        .WithValueTemplate(@"<a href='Value' class='btn btn-icon-only red'>
           //                             < i class='fa fa-edit'></i>
           //                         </a>");
           //        cols.Add().WithHtmlEncoding(true)
           //        .WithHeaderText(" ")
           //        .WithValueTemplate(@"<a onclick='Delete('Model.Id')'  class='btn btn-icon-only red'>
           //                             < i class='fa fa-times'></i>
           //                         </a>");
           //    })
           //    .WithSorting(true, "Username")
           //    .WithPaging(true, 10)
           //    .WithRetrieveDataMethod((context) =>
           //    {
           //        // Query your data here. Obey Ordering, paging and filtering parameters given in the context.QueryOptions.
           //        // Use Entity Framework, a module from your IoC Container, or any other method.
           //        // Return QueryResult object containing IEnumerable<YouModelItem>
           //        IUnitofWork _uow = NinjectWebCommon.GlobalKernel.Get<IUnitofWork>();
           //        var option = context.QueryOptions;
           //        var query = _uow.Admin.GetUsers();
           //        var result = new QueryResult<User>();
           //        result.TotalRecords = query.Count();
           //        if (!string.IsNullOrWhiteSpace(option.SortColumnName))
           //        {
           //            switch (option.SortColumnName.ToLower())
           //            {
           //                case "name":
           //                    query = query.OrderBy(p => p.Name);
           //                    break;
           //                case "username":
           //                    query = query.OrderBy(p => p.Username);
           //                    break;
           //                case "password":
           //                    query = query.OrderBy(p => p.Password);
           //                    break;
           //                case "block":
           //                    query = query.OrderBy(p => p.Block);
           //                    break;
           //                default:
           //                    break;

           //            }
           //        }
           //        if (option.GetLimitOffset().HasValue)
           //        {
           //            query = query.Skip(option.GetLimitOffset().Value).Take(option.GetLimitRowcount().Value);
           //        }
           //        result.Items = query;
           //        return result;

           //    })
           //);
            //END USER GRID



            /*
           
            */
        }
    }
}