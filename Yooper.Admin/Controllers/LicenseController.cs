﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yooper.DTO;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.Admin.Controllers
{
    [SecurityLayer]
    public class LicenseController : Controller
    {
        IUnitofWork _uow;
        public LicenseController(IUnitofWork uow)
        {
            _uow = uow;
        }
        // GET: License
        public ActionResult Index()
        {
            var model = _uow.Admin.GetUsers().Where(x=> !string.IsNullOrEmpty(x.SecretKey));
            return View(model);
        }

        public ActionResult GenerateLicense(long? Id)
        {
            try
            {
                var __model = new GenerateLicenseVM();
                var obj = _uow.Admin.GetUsers().Where(x => x.Id == Id).Select(x=>new GenerateLicenseVM() {
                    UserId= x.Id,
                    LicenseExpiryDate= x.LicenseExpiryDate,
                    NumberOfLogin= x.Limit,
                    SecretKey= x.SecretKey
                }).FirstOrDefault();
                if (obj!=null)
                {
                    __model = obj;
                    ViewBag.users = _uow.Admin.GetUsers().Where(x => x.Id== __model.UserId && !x.Block);
                }
                else
                {
                    ViewBag.users = _uow.Admin.GetUsers().Where(x => string.IsNullOrEmpty(x.SecretKey) && !x.Block);
                }
                
                return View(__model);
            }
            catch (YooperException ex)
            {
                throw ex;
            }
        }
        [HttpPost]
        public ActionResult GenerateLicense(GenerateLicenseVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _uow.Admin.GenerateLicenseKey(model.UserId, model.SecretKey, model.NumberOfLogin.GetValueOrDefault(), model.LicenseExpiryDate.GetValueOrDefault());
                }
                return RedirectToAction("Index");
            }
            catch (YooperException ex)
            {
                throw ex;
            }
        }



        public ActionResult CreateLicense()
        {
            return Json(new { Data = Guid.NewGuid().ToString() }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Restart(long Id)
        {
            _uow.Admin.RestartUserApplication(Id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}