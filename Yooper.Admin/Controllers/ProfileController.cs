﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yooper.DTO;
using Yooper.UOW;

namespace Yooper.Admin.Controllers
{
    [SecurityLayer]
    public class ProfileController : Controller
    {
        IUnitofWork _uow;
        public ProfileController(IUnitofWork uow)
        {
            _uow = uow;
        }
        // GET: Profile
        public ActionResult Index(long Id)
        {
            var model= _uow.Admin.GetUserMaster(Id).Select(x => new ProfileVM()
            {
                Id = x.Id,
                Email = x.Email,
                ImageUrl = x.ImageUrl,
                CreatedDate = x.CreatedDate,
                UpdatedDate = x.UpdatedDate
            }).FirstOrDefault();
            return View(model);
        }


        public ActionResult SaveProfile(ProfileVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string fileName = string.Empty;
                    if (model.Photo!=null && model.Photo.ContentLength>0)
                    {
                        
                        fileName = Guid.NewGuid() + model.Photo.FileName.Substring(model.Photo.FileName.IndexOf('.'));
                        string fullPath = Server.MapPath("~/Images/" + fileName);
                        model.Photo.SaveAs(fullPath);
                    }
                    _uow.Admin.SaveUserMaster(model.Id, model.Email, string.IsNullOrEmpty(fileName)?model.ImageUrl:fileName);
                }
                return RedirectToAction("Index", new { Id = model.Id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult ChangePassword()
        {
            var user = Session["user"] as UserMaster;
            var model = new ChangePasswordVM() { Id = user.Id };
            return View(model);
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _uow.Admin.ChangeAdminPassword(model.Id, model.NewPassword); 
                }
                return RedirectToAction("Index",new { Id = model.Id });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public JsonResult CheckPassword(long userId, string password)
        {
            try
            {
                bool result=_uow.Admin.VerifyAdminPassword(userId, password);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


    }
}