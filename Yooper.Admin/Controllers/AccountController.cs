﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yooper.DTO;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.Admin.Controllers
{
    public class AccountController : Controller
    {
        private IUnitofWork _uow;
        public AccountController(IUnitofWork uow)
        {
            _uow = uow;
        }
        // GET: Account
        public ActionResult Login()
        {
            if (TempData["error"]!=null)
            {
                ViewBag.error = TempData["error"].ToString();
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginPageVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userMaster= _uow.Admin.Login(model.Login.Username, model.Login.Password);
                    int RoleAdmin = Convert.ToInt32(ERole.ADMIN);
                    if (userMaster.Any() && userMaster.Where(x=>x.Role_Id== RoleAdmin).Any())
                    {
                        Session.Add("user", userMaster.FirstOrDefault());
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        TempData.Add("error", "username/password is incorrect, or you are not authorized.");
                        return RedirectToAction("Login");
                    }
                }
                else
                {   
                    TempData.Add("error", "Authorization exception occurs in server.");
                    return RedirectToAction("Login");
                }
            }
            catch (YooperException ex)
            {
                TempData.Add("error", ex.Message);
                return RedirectToAction("Login");
            }
        }


        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }


        [HttpPost]
        public ActionResult ForgetPasswordClick(LoginPageVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _uow.Admin.AdminForgetPassword(model.ForgetPassword.Email);
                }
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        public ActionResult RecoverResult(long Id)
        {
            try
            {
                return View(new ChangePasswordVM()
                {
                    Id= Id
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        [HttpPost]
        public ActionResult RecoverResult(ChangePasswordVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _uow.Admin.ChangeAdminPassword(model.Id, model.NewPassword);
                }

                return RedirectToAction("Login","Account");
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }

}