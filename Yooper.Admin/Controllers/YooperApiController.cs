﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Yooper.UOW;
using Yopper.Common;
using Yopper.DTO;

namespace Yooper.Admin.Controllers
{
    [EnableCors("*","*","*")]
    [RoutePrefix("api/yooper")]
    public class YooperApiController : System.Web.Http.ApiController
    {
        private IUnitofWork _uow;

        public YooperApiController(IUnitofWork uow)
        {
            _uow = uow;
        }

        [Route("login")]
        [HttpPost]
        public APIResponse<LoginRes> Login(LoginReq data)
        {
            try
            {
                var res = _uow.Admin.Login(data.Username, data.Password, data.Key).Select(x => new LoginRes()
                {
                    Id = x.Id,
                    Username = x.Username,
                    Block = x.Block,
                    LicenseExpiryDate = x.LicenseExpiryDate,
                    LicenseIssueDate = x.LicenseIssueDate,
                    Name = x.Name,
                    PasswordHash = x.Password,
                    SecretKey = x.SecretKey
                }).FirstOrDefault();
                if (res.Block)
                {
                    return new APIResponse<LoginRes>()
                    {
                        Success = false,
                        Exception = "User is Blocked by admin.",
                        Data = null
                    };
                }

                return new APIResponse<LoginRes>() {
                    Success = true,
                    Exception = string.Empty,
                    Data =res };
            }
            catch (Exception ex)
            {
                return new APIResponse<LoginRes>() { Success = false, Exception = ex.Message, Data = null };
            }
        }

        [Route("checkRestart")]
        [HttpGet]
        public APIResponse<bool> CheckRestart([FromUri] long userId)
        {
            try
            {
                var res= _uow.Admin.CheckRestart(userId);
                return new APIResponse<bool>() { Success = true, Data = res, Exception = string.Empty };
            }
            catch (Exception ex)
            {
                return new APIResponse<bool>() { Success = false, Data = false, Exception = ex.Message };
            }
        }
    }
}
