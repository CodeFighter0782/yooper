﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Yooper.DTO;
using Yooper.UOW;
using Yopper.Common;

namespace Yooper.Admin.Controllers
{
    [SecurityLayer]
    public class UserController : Controller
    {
        private IUnitofWork _uow;
        public UserController(IUnitofWork uow)
        {
            _uow = uow;
        }
        // GET: User
        public ActionResult Index()
        {
            var model = _uow.Admin.GetUsers();
            return View(model);
        }

        public ActionResult ActionUser(long? Id)
        {
            try
            {
                if (TempData["error"]!=null)
                {
                    ViewBag.error = TempData["error"].ToString();
                }
                var model = new ActionUserVM();
                if (Id!=null)
                {
                    model = _uow.Admin.GetUsers(Id.GetValueOrDefault()).Select(x => new ActionUserVM()
                    {
                        Id = x.Id,
                        Block = x.Block,
                        Password = x.Password,
                        CreatedDate = x.CreatedDate,
                        CreatedBy = x.CreatedBy,
                        LicenseExpiryDate = x.LicenseExpiryDate,
                        LicenseIssueDate = x.LicenseIssueDate,
                        Name = x.Name,
                        UpdatedBy = x.UpdatedBy,
                        UpdatedDate = x.UpdatedDate,
                        Username = x.Username
                    }).First();
                }
                return View(model);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        [HttpPost]
        public ActionResult ActionUser(ActionUserVM model)
        {
            try
            {
                var user = Session["user"] as UserMaster;
                if (ModelState.IsValid)
                {
                    _uow.Admin.SaveUser(new DTO.User()
                    {
                        Id = model.Id,
                        Block = model.Block,
                        CreatedBy = user != null ? user.Email : "SYSTEM",
                        CreatedDate = DateTime.UtcNow,
                        Enable = true,
                        Name = model.Name,
                        Password = model.Password,
                        Username = model.Username,
                        PasswordHash = ""
                    });
                }
                return RedirectToAction("Index");
            }
            catch (YooperException ex)
            {
                TempData["error"] = ex.ErrorMessage;
                return RedirectToAction("ActionUser");
            }
        }


        public ActionResult DeleteUser(long Id)
        {
            try
            {   
                _uow.Admin.DeleteUser(Id);
                return RedirectToAction("Index");
            }
            catch (YooperException ex)
            {
                throw ex;
            }
        }


        public ActionResult CheckEmail(string Email)
        {
            return Json(_uow.Admin.CheckEmailAlreadyExist(Email), JsonRequestBehavior.AllowGet);
        }
    }
}